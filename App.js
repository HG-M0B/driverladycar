import { View, Text, SafeAreaView } from 'react-native'
import React, { useEffect } from 'react'
import RootNavigator from './src/navigator/RootNavigator'
import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';
import { SafeAreaProvider } from 'react-native-safe-area-context';

const App = () => {
  useEffect(()=>{
    setTimeout(
      function () {
        SplashScreen.hide()
      }
        .bind(this),
      3000
    );
  },[])
  return (
    <SafeAreaProvider >
      <NavigationContainer>
        <RootNavigator />
      </NavigationContainer>
    </SafeAreaProvider>
   
  )
}

export default App