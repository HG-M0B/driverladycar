import Geolocation from '@react-native-community/geolocation';


export const getCurrentPosition = () => {
    return new Promise((resolve, reject) => {
        Geolocation.getCurrentPosition(
            position => {
                resolve({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                });
            },
            error => reject(error),
            { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 },
        );
    });
};