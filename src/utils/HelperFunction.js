import AsyncStorage from '@react-native-async-storage/async-storage';

export const storeData = async (key, value) => {
    try {
      await AsyncStorage.setItem(key,JSON.stringify(value),setDataFunction);
    } catch (e) {
      console.log("error async", e);
    }
  };
  
  export const getData = async key => {
    try {
      let value = await AsyncStorage.getItem(key);
      console.log(value);
      return JSON.parse(value);
    } catch (e) {
      console.log("value fetched error", e);
    }
  };

  export const getAllAsyncData = async ()=>{
    try{
      const keys = await AsyncStorage.getAllKeys(); 
      console.log("All keys",keys); 
      const result = await AsyncStorage.multiGet(keys);  
      console.log("All async store",JSON.parse(result));
      // return JSON.parse(result)
    }
    catch(e)
    {
      console.log("e",e)
    }
  }
  
export const checkPasswordFormat = (password) => {
  let passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
 return passwordRegex.test(password);
}