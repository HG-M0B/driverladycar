import axios from "axios";
import { getData } from "./HelperFunction";

const instance = axios.create({
    baseURL: 'http://13.59.90.211:7030/api/'
  });


  instance.interceptors.request.use(
    async (config) => {
      console.log("instance called")
      // const jwtToken = getData("")
      // const token = store.getState().localStates && store.getState().localStates.loginuserToken
      // if (token) {
      //   config.headers.Authorization = token
      // }
      return config
    },
    (error) => {
      console.log("request error",error)
      Promise.reject(error)
    }
  )
  
  
  instance.interceptors.response.use(async (response) => {
    // debugger
      return response
  }, error => {
    // debugger
    console.log({ error })
    return error
  })
  

  export default instance;