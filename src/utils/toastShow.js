import { Platform } from 'react-native';
import Toast from 'react-native-root-toast';
// import { colorConstant } from './constant';
// import { heightPercentageToDP } from './responsiveFIle';

const toastShow=(message,type,color)=>{
    Toast.show(message, {
        position:Platform.OS =='ios'?  10 :15,
        shadow: true,
        animation: true,
        hideOnPress: true,
        textColor:color == "" ? "#FFFFFF" : color ,
        delay: 0,
        backgroundColor:type,
        onShow: () => {
            // calls on toast\`s appear animation start
        },
        onShown: () => {
            // calls on toast\`s appear animation end.
        },
        onHide: () => {
            // calls on toast\`s hide animation start.
        },
        onHidden: () => {
            // calls on toast\`s hide animation end.
        }
    });
}

export default toastShow