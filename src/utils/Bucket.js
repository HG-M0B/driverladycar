import {
    S3Client,
    CreateBucketCommand,
    DeleteBucketCommand,
  } from "@aws-sdk/client-s3";
  import { CognitoIdentityClient } from "@aws-sdk/client-cognito-identity";
  import { fromCognitoIdentityPool } from "@aws-sdk/credential-provider-cognito-identity";

  const region = "Regions.US_EAST_1";
  const client = new S3Client({
    region,
    credentials: fromCognitoIdentityPool({
      client: new CognitoIdentityClient({ region }),
      // Replace IDENTITY_POOL_ID with an appropriate Amazon Cognito Identity Pool ID for, such as 'us-east-1:xxxxxx-xxx-4103-9936-b52exxxxfd6'.
      identityPoolId: "us-east-1:65c832c5-2f05-4ea7-ab8c-761f9f9cf1a8",
    }),
  });

  export const createBucket = async () => {
    try {
      await client.send(new CreateBucketCommand({ Bucket: "ladycar" }));
      setSuccessMsg(`Bucket "${bucketName}" created.`);
    } catch (e) {
      setErrorMsg(e);
    }
  };

  const deleteBucket = async () => {
    setSuccessMsg("");
    setErrorMsg("");

    try {
      await client.send(new DeleteBucketCommand({ Bucket: bucketName }));
      setSuccessMsg(`Bucket "${bucketName}" deleted.`);
    } catch (e) {
      setErrorMsg(e);
    }
  };