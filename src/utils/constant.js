import start from '../assets/images/start.png'
import carlogo from '../assets/images/carlogo.png'
import boarding1 from '../assets/images/boarding1.png'
import boarding2 from '../assets/images/boarding2.png'
import back from '../assets/images/back.png'
import info from '../assets/images/info.png'
import license from '../assets/images/license.png'
import eye from '../assets/images/eye.png'
import drop from '../assets/images/drop.png'
import cancel from '../assets/images/cancel.png'
import face from '../assets/images/face.png'
import fog from '../assets/images/fog.png'
import wish from '../assets/images/wish.png'
import option from '../assets/images/option.png'
import dog from '../assets/images/dog.png'
import profile from '../assets/images/profile.png'
import slide from  '../assets/images/slide.png'
import drawer from '../assets/images/drawer.png'
import off from '../assets/images/off.png'
import on from '../assets/images/on.png'
import profile1 from '../assets/images/profile1.png';
import profile2 from '../assets/images/profile2.png';
import profile3 from '../assets/images/profile3.png';
import zoom from '../assets/images/zoom.png';
import ginfo from '../assets/images/ginfo.png';
import camera from '../assets/images/camera.png';
import star from '../assets/images/star.png';
import speech from '../assets/images/speech.png';
import ok from '../assets/images/ok.png';
import sand from '../assets/images/sand.png';
import pencil from '../assets/images/pencil.png';
import angle from '../assets/images/angle.png';
import header from '../assets/images/header.png';
import header2 from '../assets/images/header2.png';
import bank from '../assets/images/bank.png';
import plus from '../assets/images/plus.png';
import phone from '../assets/images/phone.png';
import watch from '../assets/images/watch.png';
import map from '../assets/images/map.png';
import locationred from '../assets/images/locationred.png';
import locationgreen from '../assets/images/locationgreen.png';
import question from '../assets/images/question.png'
import drawerbg from '../assets/images/drawerbg.png'
import logout from '../assets/images/logout.png'
import search from '../assets/images/search.png'
import calender from '../assets/images/calender.png'
import star5 from '../assets/images/star5.png'
import story from '../assets/images/story.png'
import pp from '../assets/images/pp.png'
import jpeg from '../assets/images/jpeg.png'
import sos from '../assets/images/sos.png'
import car from '../assets/images/car.png'
import arrow from '../assets/images/arrow.png'
import tick from '../assets/images/tick.png'
import voice from '../assets/images/voice.png'
import send from '../assets/images/send.png'
import done from '../assets/images/done.png'
import profile4 from   '../assets/images/profile4.png'
import carroof from '../assets/images/carroof.png';
import timer from '../assets/images/timer.png'
import location from '../assets/images/location.png'
import direction from '../assets/images/direction.png'
import img from  "../assets/images/img.png"
import carr from '../assets/images/carr.png'
import forgetPassword from "../assets/images/forgetPassword.png"
import resetPassword from '../assets/images/resetPassword.png'
import edit from '../assets/images/Edit.png'
import trash from '../assets/images/trash.png'
import profile5 from '../assets/images/profile5.png'
import red from '../assets/images/red.png'
import right from '../assets/images/right.png'
import line from    '../assets/images/line.png'
import trans from   '../assets/images/trans.png'
import upload from '../assets/images/upload.png'
import smoke from '../assets/images/smoke.png'
import music from '../assets/images/music.png'
import taxi2 from '../assets/images/taxi2.png'
import taxi3 from '../assets/images/taxi3.png'
import faceid from '../assets/images/faceid.png'
import facebook from '../assets/images/facebook.png'
import google from '../assets/images/google.png'
import apple from '../assets/images/apple.png'
export const imageConstant = {
    apple,
    facebook,google,
    faceid,
    taxi2,
    taxi3,
    music,
    smoke,
    upload,
    trans,
    line,
    red,
    right,
    profile5,
    trash,
    edit,
    resetPassword,
    carr,
    forgetPassword,
    img,
    direction,
    location,
    timer,
    carroof,
    profile4,
    done,
    send,
    voice,
    tick,
    arrow,
    car,
    sos,
    jpeg,
    pp,
    story,
    star5,
    calender,
    search,
    logout,
    drawerbg,
    question,
    locationgreen,
    locationred,
    map,
    phone,
    watch,
    profile2,
    profile3,
    plus,
    bank,
    header2,
    header,
    angle,
    pencil,
    sand,
    ok,
    speech,
    star,
    camera,
    ginfo,
    zoom,
    profile1,
    on,
    off,
    drawer,
    start,
    carlogo,
    boarding1,
    boarding2,
    back,
    info,
    license,
    eye,
    drop,
    cancel,
    face,
    fog,
    wish,
    option,
    dog,
    profile,
    slide
}



export const fontConstant = {
    thin:"SfProText-Thin",
    regular:"SfProText-Regular",
    semi:"SfProText-Semibold",
    ultralight:"SfProText-Ultralight",
    bold:"SfProText-Bold",
    reg:"SfProDisplay-Regular",
    
}
export const colorConstant = {
    white:"#FFFFFF",
    black:"#000000",
    purple:"#6C00C0",
    textGary:"#7A7A7A",
    placeholderColor:"#8D8D8DDE",
    darkGray:"#5C5C5C",
    grayBg : "#E4E4E4",
    grayButton:"#7C7C7C",
    grayText:"#555555",
    green :"#0EAC00",
    pink:"#EA758F",
    errorRed:"#FF0000",
    successGreen:"#07bc0c"

}

