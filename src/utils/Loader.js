import React, { useState, useEffect } from 'react'
import { ActivityIndicator } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { colorConstant, fontConstant } from './constant'
const Loader = (props) => {
    return (
        <Spinner
            visible={props.data}
            animation={'slide'}
            color={colorConstant.purple}
            overlayColor={'rgba(0, 0, 0, 0.50)'}
           
        />


    )
}
export default Loader