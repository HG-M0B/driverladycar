import * as types from "./actionsType";
import defaultState from "./globalStates";

/**
 * Reducer
 */
// console.log("localState--->",defaultState.initialState)
export default function parentFlowReducer(
    state = defaultState.initialState,
    action
) 
{
    switch (action.type) {
        case types.INTRO_STATUS:
            return {
                ...state,
                introStatus: action.payload.introStatus
            }
            case types.DRIVER_LOCATION:
            return {
                ...state,
                driverLocation: action.payload.driverLocation
            }
            case types.STATE_LIST:
            return {
                ...state,
                stateList: action.payload.stateList
            }
        default:
            return state;
    }

}

/**
 * Actions
 */
export const actions = {
    setIntroStatus: introStatus => {
        return {
            type: types.INTRO_STATUS,
            payload: { introStatus: introStatus }
        };
    },
    setDriverLocation :  driverLocation => {
        return {
            type: types.DRIVER_LOCATION,
            payload: { driverLocation: driverLocation }
        };
    },
    setStateList :  stateList => {
        return {
            type: types.STATE_LIST,
            payload: { stateList: stateList }
        };
    },
} 