import { View, Text ,StyleSheet, TouchableOpacity,Image, SafeAreaView,Platform,Dimensions,StatusBar} from 'react-native';
import React ,{useEffect, useState,useContext}from 'react';
import {Height, Width} from '../dimension/dimension';
import { colorConstant, fontConstant, imageConstant } from '../utils/constant';



const CustomHeader = (props) => {
  return (
  <View style={[
        {...styles.MainView},
        {
            marginTop: props.marginTop ? props.marginTop :  Platform.OS === 'ios' ? 25: Height/40,
            marginBottom:Platform.OS === 'ios' ? 20 : Height/60
        }]}>
        <TouchableOpacity
          activeOpacity={0.8}
              onPress={props.onPressBack ? props.onPressBack : ()=>props.navigation.goBack()}
              style={styles.BackTouch}>
            <Image source={props.img ? props.img : imageConstant.back} resizeMode='contain' 
            style={[{...styles.Img},{tintColor:props.tintColor?props.tintColor:colorConstant.purple}]}
            />
        </TouchableOpacity>
      {
          props.headerText ? 
          <Text style= {[{...styles.HeaderText},{
            color : props.color ? props.color :'#141416',
          }]}>{props.headerText}</Text>
          :<></>
      }
    
       
  </View>
  );
};

export default CustomHeader;

const styles  = StyleSheet.create({
    MainView: {
        width:"90%",
        alignSelf:"center",
        alignItems:"center",
        flexDirection:"row",
       
    },
    HeaderText:{
        fontSize:20,
        fontFamily:fontConstant.bold,
        width:"90%",
        textAlign:"center",
    },
    HeaderTextWithoutImage:{
        fontSize:18,
        fontFamily:fontConstant.bold,
        width:"75%",
        textAlign:"center",
        position:"absolute",
        marginLeft:Width/10,

    },
    BackTouch:{
      padding:5,
      width:20,
      height:20,
      justifyContent:"center",
      alignItems:"center",
    },
    Img:{
      width:20,
      height:10
      
    },

})