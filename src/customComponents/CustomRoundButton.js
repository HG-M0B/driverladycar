import { View, Text, TouchableOpacity ,StyleSheet,Platform} from 'react-native';
import React from 'react';
import {Width,Height} from '../dimension/dimension';
import {colorConstant, fontConstant } from '../utils/constant';
import { heightPercentageToDP, widthPercentageToDP } from '../utils/responsiveFIle';



const CustomRoundButton = (props) => {
  return (
      <TouchableOpacity
        onPress={props.OnButtonPress}
        activeOpacity={0.9}
        style={[
          {...styles.ButtonTouch},
          {
              marginTop:props.marginTop,
              marginBottom:props.marginBottom,
              width: props.width ? props.width : "90%",
              position:props.position,
              bottom:props.bottom,
              top:props.top,
              backgroundColor:props.backgroundColor? props.backgroundColor: "#6C00C0"
          }]}
        >
        <Text style={[{...styles.text},{color: props.textColor ? props.textColor : colorConstant.white}]}>{props.buttonText}</Text>
      </TouchableOpacity>
  );
};

export default CustomRoundButton;

const styles = StyleSheet.create({
  ButtonTouch:{
    height:50,
    alignSelf:"center",
    justifyContent:"center",
    alignItems:"center",
    borderRadius:30
  },
  text:{
    fontSize:14,
    fontFamily:fontConstant.semi,
    textAlign:"center"
  }    
})