import { View, Text ,StyleSheet, TextInput,Image, Platform} from 'react-native';
import React, { useState,useContext } from 'react';
import { Height, Width } from '../dimension/dimension';
import { imageConstant, colorConstant, fontConstant } from '../utils/constant';
import { Dropdown } from 'react-native-element-dropdown';
const CustomDropDown = (props) => {
 
    const data1 = [
        { label: 'Item 1', value: '1' },
        { label: 'Item 2', value: '2' },
        { label: 'Item 3', value: '3' }
      ];
  return (

          <Dropdown
              data={props?.dropDownData ? props.dropDownData : data1 }
              labelField={props?.labelField ? props.labelField : "label"}
              valueField={props?.valueField ? props.valueField : "value"}
              maxHeight={300}
              onChange={props?.onChange}
              disable={props?.disable}
              selectedTextStyle={props.selectedTextStyle}
              value={props.value}
              containerStyle={{
              }}
              style={{
                width:"80%",
                alignSelf:"center",
                height:50,
                borderWidth:props.borderWidth?props.borderWidth:1.5,
                borderColor: props.borderColor ? props.borderColor:colorConstant.purple,
                borderRadius:10,
                paddingHorizontal:25,
                marginTop:props.marginTop ? props.marginTop : 25,
                marginBottom:props.marginBottom,
                marginVertical:props.marginVertical,
                backgroundColor:props.backgroundColor ? props.backgroundColor:colorConstant.white,
                color:"black"
              }}
              iconStyle={{
                  tintColor: colorConstant.black,
              }}
              activeColor={{
                  color: "blue"
              }}
              placeholder={props.placeholder ?props.placeholder :"Selec item"}
              placeholderStyle={{ 
                  fontSize: props.placeholderFont ? props.placeholderFont :14, 
                  color: props.placeholderColor ? props.placeholderColor: colorConstant.placeholderColor,
                   fontFamily:props.fontFamily 
                }}
          />

  );
};

export default CustomDropDown;

const styles = StyleSheet.create({
    img:{
        width:25,
        height:25,
    },
    placeholderStyle:{

    }
})