import { StyleSheet, Text, View ,TextInput, Platform} from 'react-native'
import React from 'react';
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'

const CustomInput = (props) => {

  return (
   
  <TextInput
  name={props.name}
   placeholder = { props.placeholder? props.placeholder: 'Type here'}
   placeholderTextColor={ props.placeholderTextColor ? props.placeholderTextColor:colorConstant.placeholderColor}
   keyboardType={props.keyboardType}
   value={props.value}
   onChange={props.onChange}
   editable={props.editable}
   onChangeText={props.onChangeText}
   style={[{...styles.inputStyle},
    {
        ...Platform.select({
            ios: {
                height: 50
            }
        }),
        marginTop: props.marginTop,
        marginVertical: props.marginVertical,
        marginBottom: props.marginBottom,
        backgroundColor: props.backgroundColor ? props.backgroundColor : colorConstant.white,
        borderWidth: props.borderWidth ? props.borderWidth :1.5,
        borderRadius: props.borderRadius ? props.borderRadius : 10,
        paddingHorizontal: 25,
        borderColor: props.borderColor ? props.borderColor : colorConstant.purple
       
   }]}
   />

  )
}

export default CustomInput

const styles = StyleSheet.create({
    inputStyle:{
        width:"80%",
        alignSelf:"center",
        paddingVertical:8,
        color:'#000'
     
    }
})