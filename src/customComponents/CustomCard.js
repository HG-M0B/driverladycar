import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { useState } from 'react'
import { useEffect } from 'react'

const CustomCard = (props) => {
    const {route,navigation} =  props;
    const [speechFlag,setSpeechFlag] =  useState(false);
    useEffect(()=>{

    },[])
  return (
      <View style={[{ ...styles.row },
      {
        position:props.position,
        top:props.top,
        bottom:props.bottom,
        marginTop:props.marginTop
      }
      ]}>
          <Text style={styles.text}>Passenger</Text>

        <View style={{
            flexDirection:"row",
            width:"35%"
        }}>
        <Image
              source={imageConstant.profile4}
              resizeMode='contain'
              style={styles.img} />
          <Text numberOfLines={1} style={styles.text2}>Lucy</Text>
        </View>

          <View style={styles.starView}>

              {
                  props.speech &&
                  <TouchableOpacity
                  style={{
                      padding:2,
                      marginRight:3
                  }}
                  onPress={props.onSpeechPress}
                  >
                      <Image
                          source={props.speech}
                          resizeMode='contain'
                          style={styles.img1} />
                  </TouchableOpacity>
              }

              <Text style={styles.text3}>4.3 </Text>
              <Image
                  source={imageConstant.star}
                  resizeMode='contain'
                  style={styles.img1} />

          </View>
    </View>
  )
}

export default CustomCard

const styles = StyleSheet.create({
    row:{
        flexDirection:"row",
        justifyContent:"space-between",
        width:"90%",
        alignSelf:"center",
        alignItems:"center",
        height:55,
        backgroundColor:colorConstant.white,
        paddingHorizontal:10,
        borderRadius:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },
    text:{
        fontSize:20,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple
    },
    img:{
        width:30,
        height:30
    },
    img1:{
        width:20,
        height:20
    },
    text2:{
        fontSize:18,
        fontFamily:fontConstant.bold,
        color:colorConstant.black,
        marginLeft:5

    },
    text3:{
        fontSize:14,
        fontFamily:fontConstant.semi,
        color:colorConstant.black
    },
    starView:{
        flexDirection:"row",
    }
})