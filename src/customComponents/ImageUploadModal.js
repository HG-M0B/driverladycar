import {
  Modal,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import AntDesign from "react-native-vector-icons/AntDesign";
import { moderateScale, scale } from "react-native-size-matters";
import { colorConstant, imageConstant, fontConstant } from "../utils/constant";
import CustomButton from "./CustomButton";
const ImageUploadModal = ({showUploadModal, setShowUploadModal,modalShowFor,setModalShowFor}) => {
  return (
    <>
      <StatusBar backgroundColor={"white"} />
      <Modal visible={true} statusBarTranslucent>
        <SafeAreaView style={styles.mainContainer}>
          <View style={styles.container}>
            <View>
              <TouchableOpacity
              onPress={()=>setShowUploadModal(false)}
               style={styles.backButton}>
                <Image
                  source={require("../assets/images/purpleArrow.png")}
                  style={styles.img}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>

            <View style={styles.headingText}>
              <Text style={styles.infotext}>Take a picture of</Text>
              <Text style={styles.infotext}>
                your ID{" "}
                <Image
                  source={imageConstant.info}
                  resizeMode="contain"
                  style={styles.infoImg}
                />
              </Text>
            </View>

            <View style={[styles.alc, styles.avtarImg]}>
              <Image
                resizeMode="contain"
                source={require("../assets/images/license.png")}
                style={styles.img2}
              />
            </View>

            <View style={{marginTop:moderateScale(24)}}>
              <CustomButton
                buttonText={`${'Licence'} front`}
                marginTop={25}
                width={"85%"}
                //   rImage={licenseBack !== "" && licenseFront !== "" ? imageConstant.right : null}
                //   OnButtonPress={() => getImageUrl("License")}
              />

              <CustomButton
                buttonText={`Licence back`}
                marginTop={25}
                width={"85%"}
                //   rImage={licenseBack !== "" && licenseFront !== "" ? imageConstant.right : null}
                //   OnButtonPress={() => getImageUrl("License")}
              />
            </View>
          </View>
        </SafeAreaView>
      </Modal>
    </>
  );
};

export default ImageUploadModal;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: "#ffff",
    paddingHorizontal: moderateScale(20),
  },
  img: {
    height: moderateScale(35),
    width: moderateScale(35),
  },
  infotext: {
    fontSize: 29,
    color: colorConstant.purple,
    fontFamily: fontConstant.semi,
  },
  alc: {
    alignItems: "center",
  },
  avtarImg: {
    marginTop: moderateScale(35),
  },
  img2: {
    height: moderateScale(300),
  },
  backButton: { width: scale(50), marginTop: moderateScale(20) },
  headingText: { alignItems: "center", marginTop: moderateScale(20) },
  infoImg: { height: 30, width: 30 },
});
