import { View, Text, TouchableOpacity ,StyleSheet,Platform, Image} from 'react-native';
import React from 'react';
import {Width,Height} from '../dimension/dimension';
import {colorConstant, fontConstant, imageConstant } from '../utils/constant';
import { heightPercentageToDP, widthPercentageToDP } from '../utils/responsiveFIle';



const CustomButton = (props) => {
  return (
      <TouchableOpacity
        disabled={props.disabled}
        onPress={props.OnButtonPress}
        // activeOpacity={0.9}
        style={[
          {...styles.ButtonTouch},
          {
              marginTop:props.marginTop,
              marginBottom:props.marginBottom,
              width: props.width ? props.width : "90%",
              position:props.position,
              bottom:props.bottom,
              borderWidth:props.borderWidth,
              borderColor:props.borderColor,
              top:props.top,
              marginVertical:props.marginVertical,
              backgroundColor:props.backgroundColor? props.backgroundColor: colorConstant.purple
          }]}
        >
        <Text style={[{...styles.text},{
           color:props.color? props.color: colorConstant.white
        }]}>{props.buttonText}</Text>

        {
        props.rImage &&  <Image
          source={props.rImage}
          style={{
            width:20,
            height:20,
            position:"absolute",
            right:props.right?props.right:20
          }}
          />
        }
      </TouchableOpacity>
  );
};

export default CustomButton;

const styles = StyleSheet.create({
  ButtonTouch:{
    height :45,
    alignSelf:"center",
    justifyContent:"center",
    alignItems:"center",
    borderRadius:Width/35,
    

  },
  text:{
    fontSize:Platform.OS =='ios' ? widthPercentageToDP('4.5%'):  16,
    fontFamily:fontConstant.semi,
   
  }    
})