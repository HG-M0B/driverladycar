import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete'

const CustomAutoComplete = (props) => {
  return (
      <GooglePlacesAutocomplete
          placeholder="Type a place"
          onPress={(data, details = null) => console.log(data, details)}
          query={{ key: "AIzaSyB6YcSKTLafmolKS-F3NQ2dqOXFFY0YljA" }}
          fetchDetails={true}
          onFail={error => console.log(error)}
          onNotFound={() => console.log('no results')}
          listEmptyComponent={() => (
              <View style={{ flex: 1 }}>
                  <Text>No results were found</Text>
              </View>
          )}
      />
  )
}

export default CustomAutoComplete

const styles = StyleSheet.create({})