import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Platform,
} from "react-native";
import React from "react";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";

const CustomPasswordInput = (props) => {
  return (
    <View
      style={[
        { ...styles.main },
        {
          marginTop: props.marginTop,
          marginBottom: props.marginBottom,
          ...Platform.select({
            ios: {
              height: 50,
            },
          }),
        },
      ]}
    >
      <TextInput
        placeholder={props.placeholder ? props.placeholder : "Type here"}
        placeholderTextColor={colorConstant.placeholderColor}
        keyboardType={props.keyboardType}
        value={props.value}
        onChangeText={props.onChangeText}
        onChange={props.onChange}
        style={styles.inputStyle}
        secureTextEntry={props.secureTextEntry}
      />
      <TouchableOpacity
        onPress={props?.onPress}
        style={{
          padding: 5,
        }}
      >
        <Image
          source={imageConstant.eye}
          resizeMode="contain"
          style={{
            width: 15,
            height: 15,
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

export default CustomPasswordInput;

const styles = StyleSheet.create({
  main: {
    width: "80%",
    alignSelf: "center",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: colorConstant.white,
    borderWidth: 1.5,
    borderRadius: 10,
    justifyContent: "space-between",
    borderColor: colorConstant.purple,
    paddingHorizontal: 25,
  },
  inputStyle: {
    width: "90%",
    alignSelf: "center",
    paddingVertical: 8,
  },
});
