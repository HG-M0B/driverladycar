import { StyleSheet, Text, View,Image, Platform} from 'react-native'
import React ,{useMemo}from 'react';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Height, Width } from '../dimension/dimension';
import { imageConstant } from '../utils/constant';
import Geolocation from "@react-native-community/geolocation";
import Geocoder from 'react-native-geocoding';
import { useState } from 'react';
import { useFocusEffect } from '@react-navigation/native';

const CustomMapView = (props) => {

  const [lat,setLat] = useState(null);
  const [lng,setLng] = useState(null);
  const [address,setAddress] = useState("")

  // useFocusEffect(
  //   React.useCallback(() => {
  //     getPosition()
  //   }, [])
  // );

  // getPosition = () => {
  //   Geolocation.getCurrentPosition(
  //     position => {
  //       console.log("position",position.coords)
  //       Geocoder.init("AIzaSyAU6KwRMvEmhQbruXnj60cwiORuKQ5BohQ");
  //       setLat(position.coords.latitude);
  //       setLng(position.coords.longitude);
  //       Geocoder.from({
  //         lat: position.coords.latitude,
  //         lng: position.coords.longitude
  //       })
  //         .then(json => {
  //           // console.log("Address full response==",json.results[0].formatted_address);
            
  //           setAddress(json.results[0].formatted_address);
  //         })
  //         .catch(error =>{
  //           console.log(error)
  //         });
  //     },
  //     error => {
  //       if (error.code == 2) {
  //       alert("Please on your location ")
  //       }
  //     },
  //     {enableHighAccuracy:false, timeout: 1000, maximumAge: 3000},
     
  //   );
    
  // };

  // console.log(":Current-->",address,lat,lng)
  return (
    <View style={[{ ...styles.container }, { height: props.height ? props.height : Height * 0.6, marginTop: props.marginTop }]}>
      {/* {

        lat !== null && lng !== null &&(
          <MapView

        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
        style={styles.map}
        region={{
          latitude:28.5355,
          longitude:77.3910,
          latitudeDelta:0.534,
          longitudeDelta:0.654,
        }}
      >

      </MapView>
        )
      } */}
      <MapView

        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
        style={styles.map}
        region={{
          latitude: 28.5355,
          longitude: 77.3910,
          latitudeDelta: 0.534,
          longitudeDelta: 0.654,
        }}
      >

      </MapView>
      <Image
        resizeMode='contain'
        source={imageConstant.zoom}
        style={styles.img}
      />
    </View>
  )
}

export default CustomMapView


const styles = StyleSheet.create({
  container: {
   width:"100%",


 },
 map: {
   ...StyleSheet.absoluteFillObject,
 },
 img:{
   height:60,
   width:60,
   position:"absolute",
   top:Height*0.10,
   right:0
 }
})