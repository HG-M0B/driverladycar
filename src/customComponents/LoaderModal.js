import React from "react";
import { Modal, SafeAreaView, Text, View } from "react-native";
import LottieView from 'lottie-react-native';
import { scale } from "react-native-size-matters";

const LoaderModal = ({ isLoading}) => {
  return (
      <Modal statusBarTranslucent transparent visible={isLoading}>
        <SafeAreaView style={{flex:1}} >
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1,
            backgroundColor:'rgba(126,126,126,0.54)'

          }}
        >
          <View>
            <LottieView source={require('../assets/images/loader.json')} style={{ height: scale(80) }} autoPlay />
          </View>
        </View>
        {/* <Text>ISkjsdkj</Text> */}
        </SafeAreaView>
      </Modal>
  );
};

export default LoaderModal;