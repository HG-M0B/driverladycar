import { Image, Platform, StyleSheet, Text, View } from "react-native";
import React from "react";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import CustomButton from "../customComponents/CustomButton";

const Wishes = (props) => {
  return (
    <View style={styles.main}>
      <Text style={styles.text}>
        LadyCar wishes {"\n"} you have a pleasant trip !
      </Text>
      <Image
        resizeMode="contain"
        source={imageConstant.fog}
        style={{
          width: 27,
          height: 27,
          alignSelf: "center",
          marginTop: 35,
        }}
      />

      <Text style={styles.text1}>
        {" "}
        The time to confirm your profile{"\n"}and background check will take:
      </Text>
      <Text
        style={[
          { ...styles.text, fontSize: 28, fontFamily: fontConstant.bold },
          { marginTop: 20 },
        ]}
      >
        24 hours
      </Text>

      <Image
        resizeMode="contain"
        source={imageConstant.wish}
        style={{
          width: 300,
          height: null,
          aspectRatio: 1 / 0.6,
          alignSelf: "center",
          marginTop: 45,
        }}
      />
      <CustomButton
        buttonText={"START"}
        backgroundColor={colorConstant.grayButton}
        position={"absolute"}
        bottom={Platform.OS == "ios" ? 30 : 30}
        OnButtonPress={() => props.navigation.navigate("Login1")}
      />
    </View>
  );
};

export default Wishes;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
  },
  text: {
    fontSize: 24,
    fontFamily: fontConstant.semi,
    color: colorConstant.purple,
    textAlign: "center",
    marginTop: Platform.OS == "ios" ? 80 : 60,
  },
  text1: {
    fontSize: 18,
    fontFamily: fontConstant.semi,
    color: colorConstant.black,
    textAlign: "center",
    marginTop: 10,
  },
});
