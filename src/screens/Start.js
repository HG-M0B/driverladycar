import { View, Text, ImageBackground, Image ,StatusBar, Platform} from 'react-native'
import React from 'react'
import {colorConstant, fontConstant, imageConstant} from '../utils/constant';
import CustomRoundButton from '../customComponents/CustomRoundButton';
import {Height, Width} from '../dimension/dimension';
import { useDispatch } from 'react-redux';
import { actions } from '../redux/reducers';
const Start = (props) => {
    const {navigation} = props;
    let dispatch =  useDispatch();

  return (
      <>
          <StatusBar backgroundColor="transparent" translucent={true} />
          <ImageBackground
              source={imageConstant.start}
              resizeMode='cover'
              style={{
                  flex: 1,
                  height: null,
                  width: null,
                  alignItems: "center"
              }}
          >
              <Image
                  source={imageConstant.carlogo}
                  resizeMode='contain'
                  style={{
                      ...Platform.select({
                          ios: {
                              marginTop: StatusBar.currentHeight + 30,
                          },
                          android: {
                              marginTop: 20
                          }
                      }),
                      width: 220,
                      height: 100,
                  }} />

              <View style={{
                  position: "absolute",
                  bottom: 30,
              }}>
                  <CustomRoundButton
                      OnButtonPress={
                          () => dispatch(actions.setIntroStatus("auth"))
                          //   () => navigation.navigate("OnBoarding2")
                      }
                      width={"80%"}
                      backgroundColor={colorConstant.white}
                      buttonText={'Get Started'}
                      textColor={colorConstant.black}
                  />
                  <Text style={{
                      fontSize: 12,
                      fontFamily: fontConstant.regular,
                      color: colorConstant.white,
                      marginTop: 10
                  }}>Need a ride?<Text
                      style={{
                          textDecorationLine: "underline",
                      }} > Open the LadyCar Passenger application</Text>
                  </Text>

              </View>
          </ImageBackground>
      </>
  )
}

export default Start