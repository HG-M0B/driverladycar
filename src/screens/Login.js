import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import CustomInput from '../customComponents/CustomInput'
import CustomPasswordInput from '../customComponents/CustomPasswordInput'
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'
import CustomButton from '../customComponents/CustomButton'
import AboutDriver from './AboutDriver'
import { Height, Width } from '../dimension/dimension'
import { useEffect } from 'react'
import { useState } from 'react'

const Login = (props) => {
    const [showpass,setShowpass]=useState(false)

    // useEffect(()=>{
    //     if(props.route.params.data == "login")
    //     {}
    //     else
    //     {}
    // },[])
  return (
      <View style={styles.main}>
          <KeyboardAwareScrollView 
          extraHeight={300}
          showsVerticalScrollIndicator={false}
          >
              <Text style={styles.text}>Your Account Is Ready!</Text>
              <Image
                  resizeMode='contain'
                  source={imageConstant.wish}
                  style={{
                      width:340,
                      height:null,
                      aspectRatio:1/0.6,
                      alignSelf: "center",
                      marginTop: 30
                  }}
              />
              <Text style={[{ ...styles.text }, { fontSize:20,marginTop:30}]}>Log In To Sart The Adventure</Text>
              <CustomInput
                  placeholder={'Phone Number'}
                  marginTop={30}
              />
              <CustomPasswordInput
              secureTextEntry={showpass}
                onPress={()=>setShowpass(!showpass)}
                  placeholder={'Password'}
                  marginTop={25}
              />


              <TouchableOpacity
              onPress={()=>props.navigation.navigate("Forget",{
                  flow:"login"
              })}
              style={{
                  marginTop:10,
                  alignSelf:"flex-start",
                  marginLeft:Width*0.10,
              }}>
                  <Text style={{
                   
                      
                      fontSize:12,
                      fontFamily:fontConstant.regular,
                      color:colorConstant.purple,
                      textDecorationLine:"underline"
                  }}>I forgot my password</Text>
              </TouchableOpacity>
                <CustomButton
                  buttonText={'CONFIRM'}
                  width={'80%'}
                  marginVertical={25}
                //   marginTop={120}
                //   position={'absolute'}
                //   bottom={Platform.OS == 'ios' ? 30 : 30}
                  OnButtonPress={() => props.navigation.navigate("AboutDriver")}
              />
             
          </KeyboardAwareScrollView>
        
       
          
      </View>

  )
}

export default Login

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    text:{
        fontSize:22,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        textAlign:"center",
        marginTop:Platform.OS == 'ios' ? 80 : 50
    }
})