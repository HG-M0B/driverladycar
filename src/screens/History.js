import { Image, ScrollView, StyleSheet, Text, TouchableOpacity,FlatList, View, SafeAreaView } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { Height } from '../dimension/dimension'
import CustomHeader from '../customComponents/CustomHeader';
import {  } from 'react-native-gesture-handler';


const History = (props) => {

    
    const data=[
        {
            id:1,
            title:"2720 Old Lebanon Rd",
            money:"13",
            date:"Sep 19  12:25 PM",
            flag:true
        },
        {
            id:2,
            title:"2720 Old Lebanon Rd",
            money:"13",
            date:"Sep 19  12:25 PM",
            flag:false
        },
        {
            id:3,
            title:"2720 Old Lebanon Rd",
            money:"13",
            date:"Sep 19  12:25 PM",
            flag:false
        },
        {
            id:4,
            title:"2720 Old Lebanon Rd",
            money:"13",
            date:"Sep 19  12:25 PM",
            flag:true
        },
        {
            id:5,
            title:"2720 Old Lebanon Rd",
            money:"13",
            date:"Sep 19  12:25 PM",
            flag:true
        },
        {
            id:6,
            title:"2720 Old Lebanon Rd",
            money:"13",
            date:"Sep 19  12:25 PM",
            flag:false
        },
      
    ]
    const renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity 
            onPress={()=>props.navigation.navigate("TripDetails",{
                data:item.flag
            })}
            style={{
                alignSelf: "center",
                width: "90%",
                marginTop: 10,
                alignItems: "center",
                height: 70,
                flexDirection: 'row',
            }}>
                <Image
                    resizeMode='contain'
                    source={imageConstant.profile4}
                    style={{
                        width: 46,
                        height: 46
                    }} />

                <View style={{
                    marginLeft: 10,
                }}>
                    <Text style={{
                        fontSize: 15,
                        fontFamily: fontConstant.semi,
                        color: colorConstant.black

                    }}>2720 Old Lebanon Rd</Text>
                    <View style={{
                        flexDirection: "row",
                        alignItems: "center"
                    }}>
                        <Text style={{
                            fontSize: 14,
                            fontFamily: fontConstant.semi,
                            color: colorConstant.purple,
                        }}>$34</Text>
                        <Text style={{
                            fontSize: 12,
                            fontFamily: fontConstant.regular,
                            color: colorConstant.black,
                            marginLeft: 5
                        }}>Sep 19  12:25 PM</Text>

                        <View style={{
                            flexDirection: "row",
                            alignItems: "center",
                            marginLeft: 7
                        }}>
                            <Image
                                resizeMode='contain'
                                style={{
                                    width: 10,
                                    height: 10,
                                    marginRight: 4
                                }}
                                source={item.flag ? imageConstant.right : imageConstant.red} />

                            <Text style={{
                                fontSize: 10,
                                fontFamily: fontConstant.regular,
                                color: item.flag ? "#83D28A" : "#D90909"
                            }}>{item.flag ? "Completed" : "Cancelled"}</Text>
                        </View>

                    </View>
                </View>

                <Image
                    resizeMode='contain'
                    source={imageConstant.angle}
                    style={{
                        width: 8,
                        height: 14,
                        position: "absolute",
                        right: 0,
                        top: 25
                    }} />
            </TouchableOpacity>
        )
    }
    const seperator=()=>{
        return(
            <View style={{
                width:"90%",
                borderWidth:0.5,
                alignSelf:"center",
                borderColor:colorConstant.black,
                opacity:0.5,
                marginTop:5
            }}></View>
        )
    }
  return (
      <SafeAreaView style={styles.main}>

          <CustomHeader
              color={colorConstant.purple}
              headerText={'History'}
              navigation={props.navigation}
          />
          <Text style={{
              fontSize: 16,
              fontFamily: fontConstant.bold,
              color: colorConstant.black,
              width: "90%",
              alignSelf: "center", marginTop: 20

          }}>Last Trips</Text>
          <FlatList
              bounces={false}
              data={data}
              renderItem={renderItem}
              contentContainerStyle={styles.container}
              data={data}
              renderItem={renderItem}
              ItemSeparatorComponent={seperator}

          />
      </SafeAreaView>
  )
}

export default History

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    container:{
        marginTop:5,

    }
    
})