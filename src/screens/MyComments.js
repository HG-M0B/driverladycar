import { View, Text } from 'react-native'
import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import PendingTrip from './PendingTrip';
import ScheduleTrip from './ScheduleTrip';
import { colorConstant, fontConstant } from '../utils/constant';
import { Width } from '../dimension/dimension';
import DriverComments from './DriverComments';
import OwnComments from './OwnComments';
const Tab = createMaterialTopTabNavigator();
const MyComments=(props)=> {
const {navigation,route}   = props;
 
 
  return (
    <Tab.Navigator

    screenOptions={{
      tabBarInactiveTintColor:colorConstant.grayButton,
      tabBarActiveTintColor:colorConstant.black,
      tabBarPressColor:colorConstant.grayBg,
      tabBarItemStyle: { width:Width/2},
      tabBarIndicatorStyle:{backgroundColor:colorConstant.purple},
      tabBarStyle:{
        elevation:1,
        borderBottomColor:colorConstant.grayBg,
        borderBottomWidth:1,
        activeTintColor:colorConstant.purple,
      },
      
    
    }}>
      <Tab.Screen name="Received"
        options={{
          tabBarLabel: (props) => {
            console.log("focused",props)
            return (
              <View style={{
                width: Width / 2,
                flexDirection: "row",
                paddingVertical: 12,
                alignItems: "center",
                justifyContent: "center",
              }}>
                <Text style={{
                  color : props.color,
                  fontSize: 14,
                  fontFamily: props.color == "#000000" ?  fontConstant.bold : fontConstant.regular,
                }}>Received Comments</Text>
                

              </View>
            )
          }
        }}
        component={DriverComments}
         />


      <Tab.Screen name="Own"
        options={{
          tabBarLabel: (props) => {
            return (
              <View style={{
                width: Width / 2,
                flexDirection: "row",
                paddingVertical: 12,
                alignItems: "center",
                justifyContent: "center"
              }}>
                <Text style={{
                   color : props.color,
                  fontSize: 14,
                  fontFamily: props.color == "#000000" ?  fontConstant.bold : fontConstant.regular,
                }}>My Comments</Text>
              
              </View>
            )
          }
        }}
        component={OwnComments} />
    </Tab.Navigator>
  )
}

export default MyComments;

  