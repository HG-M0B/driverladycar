import { FlatList, Image, Platform, StyleSheet, Text, TextInput, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import CustomHeader from '../customComponents/CustomHeader'

const DriverComments = (props) => {
    const flatData = [
        {id:1,name:"Kelsey",comment:"We had a great time. 😃",input:true,reply:false,thanks:false},
        {id:2,name:"Kelvin",comment:"We had a great time. 😃",input:false,reply:false,thanks:true},
        {id:3,name:"Kelsey",comment:"We had a great time. 😃",input:false,reply:true,thanks:false},

    ];
    const renderItem=({item,index})=>{
        return(
            <View style={styles.renderView}>
                <Image
                source={imageConstant.profile4}
                style={{
                    width:35,
                    height:35
                }}/>
                <View style={{
                    marginLeft:10,
                    width:"85%",
                }}>
                    <Text style={{
                    fontSize:16,
                    fontFamily:fontConstant.bold,
                    color:colorConstant.black
                }}>{item.name}</Text>
                    <Text style={{
                    fontSize:12,
                    fontFamily:fontConstant.regular,
                    color:colorConstant.black
                }}>{item.comment}</Text>
                    {
                        item.input && (
                            <TextInput
                                placeholder='Add a comment...'
                                placeholderTextColor={colorConstant.placeholderColor}
                                style={{
                                    borderBottomWidth:0.7,
                                    borderBottomColor: colorConstant.grayButton,
                                    ...Platform.select({
                                        ios:{
                                            paddingVertical:10,
                                            marginTop:5
                                            // backgroundColor:"green"
                                        }
                                    })
                                    
                                }} />
                        )
                    }


       

                    {item.thanks && (
                        <View style={{
                            flexDirection: "row",
                            marginTop:10
                        }}>
                            <Image
                                style={{
                                    width: 25,
                                    height: 25
                                }}
                                source={imageConstant.profile4} />

                            <View style={{
                                marginLeft: 10
                            }}>
                                <Text style={{
                                    fontSize: 12,
                                    fontFamily: fontConstant.regular,
                                    color: colorConstant.black
                                }}>Thanks...!! 😃</Text>
                                <Text style={{
                                    fontSize: 10,
                                    fontFamily: fontConstant.regular,
                                    color: "#929292",
                                    fontStyle: "italic"
                                }}>22/10/2022, 6:32 PM</Text>
                            </View>
                        </View>

                    )}







                {
                    item.reply && (
                        <Text style={{
                            fontSize:14,
                            fontFamily:fontConstant.semi,
                            color:colorConstant.purple,
                            fontStyle:"italic",
                            marginTop:10
                        }}>Reply</Text>
                    )
                }
                </View>
                <Text style={{
                    fontSize:10,
                    fontFamily:fontConstant.regular,
                    color:"#929292",
                    fontStyle:"italic",
                    position:"absolute",
                    right:0
                }}>22/10/2022, 6:32 PM</Text>
               
            </View>
        )
    }
    const seperateComponent=()=>{
        return(
            <View style={[{...styles.lineView},{marginVertical:20}]}></View>
        )
    }
  return (
      <View style={styles.main}>
          <FlatList
                
             bounces={false}
              showsVerticalScrollIndicator={false}
              data={flatData}
              contentContainerStyle={styles.contain}
              ItemSeparatorComponent={seperateComponent}
              renderItem={renderItem}
          />
      </View>
  )
}

export default DriverComments

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white,
    },
    Heading:{
        fontSize:20,
        fontFamily:fontConstant.bold,
        color:colorConstant.black,
        alignSelf:"center",
        
    },
    lineView:{
        width:"85%",
        borderWidth:0.3,
        borderColor:"#000000",
        alignSelf:"center",
        opacity:0.2
        
    },
    text1:{
        width:"90%",
        fontSize:16,
        fontFamily:fontConstant.semi,
        color:colorConstant.purple,
        alignSelf:"center",
    },
    text2:{
        fontSize:11,
        fontFamily:fontConstant.bold,
        color:colorConstant.black
    },
    contain:{
        paddingVertical:25

    },
    text3:{
        fontSize:14,
        fontFamily:fontConstant.semi,
        color:colorConstant.white,
        paddingHorizontal:4
        // width:"50%"
    },
    card:{
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-between",
        paddingVertical:8,
        borderRadius:10,
        paddingHorizontal:10,
        marginTop:10,
        backgroundColor:colorConstant.purple,
        // width:"90%"
    },
    renderView:{
        flexDirection:"row",
        width:"90%",
        alignSelf:"center"

    }
})