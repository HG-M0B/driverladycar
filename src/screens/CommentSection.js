import { SafeAreaView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import {colorConstant} from '../utils/constant';
import CustomHeader from '../customComponents/CustomHeader';
import MyComments from './MyComments';

const CommentSection = (props) => {
  return (
    <SafeAreaView style={styles.main}>
          <CustomHeader
              color={colorConstant.purple}
              headerText={'Comments'}
              navigation={props.navigation}
          />
          <MyComments/>
    </SafeAreaView>
  )
}


export default CommentSection

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    }
})