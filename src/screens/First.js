import { View, Text, StyleSheet,Image, ScrollView, StatusBar,Platform,PermissionsAndroid, SafeAreaView} from 'react-native'
import React ,{ useEffect }from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import CustomButton from '../../src/customComponents/CustomButton'
import { Width } from '../dimension/dimension'
import { getCurrentPosition } from '../utils/GoogleHelperFunction'
import { actions } from '../redux/reducers';
import {useDispatch} from 'react-redux'
import toastShow from '../utils/toastShow'

const First = (props) => {
  console.log('====================================');
  console.log('hello');
  console.log('====================================');
  const dispatch =  useDispatch()

  useEffect(()=>{
    requestLocationPermission();
  },[]);


  const requestLocationPermission = async () => {
    try {
      if (Platform.OS === 'android' && Platform.Version >= 23) {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location Permission',
            message:
              'This app requires access to your location ' ,
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          locationCall();
        } else {
          alert("Location permission denied")
        }
      } else {
        locationCall();
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const locationCall=()=>{
    getCurrentPosition()
        .then(location =>{
          console.log("get location--->",location)
          dispatch(actions.setDriverLocation(location))
        })
        .catch(error =>{
          toastShow(error.message,colorConstant.errorRed)
        });
  }

  return (
      <SafeAreaView style={styles.main}>

        <Text style={styles.welcomText}>Welcome to LadyCar!</Text>
        <Image
          resizeMode='contain'
          source={imageConstant.carr}
          style={styles.img}
        />
        <CustomButton
          buttonText={'Sign Up'}
          width={'80%'}
          marginTop={'20%'}
          borderRadius={10}
          height={40}
          OnButtonPress={() => props.navigation.navigate("SignUp")}
        />
        <CustomButton
          buttonText={'Log In'}
          width={'80%'}
          marginTop={'8%'}
          OnButtonPress={() => props.navigation.navigate('Login1', {
            data: "login"
          })}
        />
      </SafeAreaView>  
  )
}

export default First
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white,
        alignItems:'center'
    },
    welcomText:{
        color:'#6C00C0',
        fontFamily:fontConstant.semi,
        fontSize:30,
        width:246,
        lineHeight:37,
        textAlign:'center',
        marginTop:'17%'
    },
    img:{
      width:Width*0.70,
      height:null,
      aspectRatio:1/0.70,
      marginTop:'7%'
    }
})