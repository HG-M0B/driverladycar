import {
  Image,
  Modal,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import React from "react";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import CustomHeader from "../customComponents/CustomHeader";
import { Width } from "../dimension/dimension";
import { useEffect } from "react";
import CustomButton from "../customComponents/CustomButton";
import ImageUploadModal from "../customComponents/ImageUploadModal";
import { moderateScale, scale } from "react-native-size-matters";
import { GetImageUrlServices } from "../services/CommonServices";
import { uploadImageServices } from "../services/uploadImageServices";
import toastShow from "../utils/toastShow";
import LoaderModal from "../customComponents/LoaderModal";
import { useState } from "react";
import {
  DeleteVechicleServices,
  GetVehiclesListServices,
  SetDefaultVechicleServices,
} from "../services/DriverServices";
import { useIsFocused } from "@react-navigation/native";

const AddVehicles = (props) => {
  const data = [
    {
      id: 1,
      flag: false,
      name: "Cilo 5",
      color: "Red",
      plate: "GJZ-0754",
    },
    {
      id: 2,
      flag: true,
      name: "Cilo 6",
      color: "Blue",
      plate: "GJZ-0755",
    },
  ];

  const [isLoading, setIsLoading] = useState(false);
  const [registration, setRegistration] = useState("");
  const [insurence, setInsurense] = useState("");
  const [showImageModal, setShowImageModal] = useState(false);
  const [vechicleList, setVechicleList] = useState([]);
  const isFocoused = useIsFocused();

  const getVehiclesListFunction = async () => {
    setIsLoading(true);
    try {
      const response = await GetVehiclesListServices();
      console.log(
        response.data.askedData,
        "this is liskahdkjashdkjahsdj099080809890890"
      );
      setVechicleList(response.data.askedData);
      setIsLoading(false);
    } catch (error) {
      toastShow("Something went wrong", colorConstant.errorRed);
      setIsLoading(false);
      console.log(error);
    }
  };

  const setDefaultVechicleFunction = async (id) => {
    let objToSend = {
      vehicleId: id,
    };
    try {
      const response = await SetDefaultVechicleServices(objToSend);
      console.log(response.data);

      toastShow("Set Default successfuly", colorConstant.successGreen);
      getVehiclesListFunction();
    } catch (error) {
      toastShow("Something went wrong", colorConstant.errorRed);
    }
  };

  const deleteVechicleFunction = async (id) => {
    let objToSend = {
      vehicleId: id,
    };
    try {
      const response = await DeleteVechicleServices(objToSend);
      getVehiclesListFunction();
      console.log("====================================");
      console.log(response.data);
      console.log("====================================");
    } catch (error) {
      console.log(error);
    }
  };

  //use Effect Area

  useEffect(() => {
    let mount = true;
    if (mount) {
      getVehiclesListFunction();
    }
    return () => {
      mount = false;
    };
  }, [isFocoused]);

  return (
    <>
      <SafeAreaView style={styles.main}>
        <CustomHeader navigation={props.navigation} />

        <Text
          style={{
            fontSize: 22,
            alignSelf: "center",
            fontFamily: fontConstant.bold,
            color: colorConstant.purple,
            marginBottom: 30,
          }}
        >
          Vehicles
        </Text>

        <ScrollView style={{ marginBottom: moderateScale(70) }}>
          {vechicleList.map((item, index) => {
            return (
              <View
                style={[
                  styles.card,
                  { opacity: item.approvedStatus == "PENDING" ? 0.6 : 1 },
                ]}
                key={index}
              >
                {/* <LoaderModal isLoading={isLoading} /> */}
                <View
                  style={{
                    width: "100%",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                    }}
                  >
                    <Image
                      source={imageConstant.car}
                      style={{
                        width: 25,
                        height: 25,
                        tintColor: colorConstant.purple,
                        marginRight: 10,
                      }}
                    />
                    <Text style={styles.text1}>{item.model}</Text>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        // if (item.approvedStatus !== "PENDING"&&vechicleList.length>2) {
                          props.navigation.navigate("UploadDriverDocument", {
                            data: "edit",
                            vechicleDetail: item,
                          });
                        // } 
                        // else {
                        //   toastShow(
                        //     `${item.approvedStatus !== "PENDING"&&vechicleList.length>2?'This Vehicle is not verifed':'Restricted vechicle not be delete'}`,
                        //     colorConstant.purple
                        //   );
                        // }
                      }}
                      style={{
                        padding: 7,
                      }}
                    >
                      <Image
                        resizeMode="contain"
                        source={imageConstant.edit}
                        style={{
                          width: 23,
                          height: 23,
                          tintColor: colorConstant.purple,
                        }}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        padding: 7,
                      }}
                      onPress={() => {
                        if (
                          !item.defaultVehicle &&
                          item.approvedStatus !== "PENDING"
                        ) {
                          deleteVechicleFunction(item._id);
                        } else {
                          toastShow(
                            "Restricted vechicle not be delete",
                            colorConstant.purple
                          );
                        }
                      }}
                    >
                      <Image
                        resizeMode="contain"
                        source={imageConstant.trash}
                        style={{
                          width: 20,
                          height: 20,
                          tintColor: "red",
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: "row",
                    width: "60%",
                    justifyContent: "space-between",
                    alignItems: "center",
                    marginTop: 10,
                  }}
                >
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: fontConstant.semi,
                      color: colorConstant.darkGray,
                    }}
                  >
                    License Plate
                  </Text>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: fontConstant.semi,
                      color: colorConstant.darkGray,
                    }}
                  >
                    Color
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: "row",
                    width: "60%",
                    justifyContent: "space-between",
                    alignItems: "center",
                    marginTop: 3,
                  }}
                >
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: fontConstant.semi,
                      color: colorConstant.black,
                    }}
                  >
                    {item.plate}
                  </Text>
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: fontConstant.semi,
                      color: colorConstant.black,
                    }}
                  >
                    {item.color}
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  {item.defaultVehicle ? (
                    <TouchableOpacity
                      activeOpacity={0.8}
                      style={{
                        padding: 5,
                        alignSelf: "flex-start",
                        marginVertical: 15,
                      }}
                    >
                      <Text
                        style={{
                          alignSelf: "flex-start",
                          fontSize: 16,
                          fontFamily: fontConstant.regular,
                          color: colorConstant.purple,
                        }}
                      >
                        Primary Car
                      </Text>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      activeOpacity={0.8}
                      style={{
                        marginVertical: 15,
                        backgroundColor: colorConstant.purple,
                        alignSelf: "flex-start",
                        paddingVertical: 10,
                        paddingHorizontal: 25,
                        borderRadius: 10,
                      }}
                      onPress={() => {
                        if (item.approvedStatus !== "PENDING") {
                          setDefaultVechicleFunction(item._id);
                        } else {
                            toastShow(
                                "This Vehicle is not verifed",
                                colorConstant.purple
                              );
                        }
                      }}
                    >
                      <Text
                        style={{
                          alignSelf: "flex-start",
                          fontSize: 12,
                          fontFamily: fontConstant.bold,
                          color: colorConstant.white,
                        }}
                      >
                        SET TO DEFAULT
                      </Text>
                    </TouchableOpacity>
                  )}
                  {item.approvedStatus == "PENDING" && (
                    <View>
                      <Text
                        style={{
                          color: colorConstant.errorRed,
                          fontFamily: fontConstant.semi,
                        }}
                      >
                        Not verifed
                      </Text>
                    </View>
                  )}
                </View>
              </View>
            );
          })}
        </ScrollView>
        <View
          style={{
            position: "absolute",
            width: "100%",
            bottom: moderateScale(20),
          }}
        >
          <CustomButton
            marginTop={10}
            OnButtonPress={
              () =>
                props.navigation.navigate("UploadDriverDocument", {
                  data: "Add",
                })
              // props.navigation.navigate("AdditionalInfo2")
            }
            buttonText={"Add New Vehicle"}
          />
        </View>
      </SafeAreaView>
      <LoaderModal isLoading={isLoading} />
    </>
  );
};

export default AddVehicles;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
  },
  input: {
    width: "90%",
    alignSelf: "center",
    backgroundColor: colorConstant.grayBg,
    borderRadius: 10,
    paddingHorizontal: 20,
  },
  card: {
    paddingVertical: 10,
    justifyContent: "space-between",
    width: "90%",
    marginVertical: 15,
    backgroundColor: colorConstant.white,
    alignSelf: "center",
    borderRadius: 10,
    paddingHorizontal: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 6,
  },
  text1: {
    fontSize: 18,
    fontFamily: fontConstant.bold,
    color: colorConstant.grayText,
  },
  delete: {
    marginLeft: Width * 0.06,
    marginVertical: 20,
  },
  text2: {
    fontSize: 18,
    fontFamily: fontConstant.regular,
    color: colorConstant.black,
  },

  mainContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: "#ffff",
    paddingHorizontal: moderateScale(20),
  },
  img: {
    height: moderateScale(35),
    width: moderateScale(35),
  },
  infotext: {
    fontSize: 29,
    color: colorConstant.purple,
    fontFamily: fontConstant.semi,
  },
  alc: {
    alignItems: "center",
  },
  avtarImg: {
    marginTop: moderateScale(35),
  },
  img2: {
    height: moderateScale(300),
  },
  backButton: { width: scale(50), marginTop: moderateScale(20) },
  headingText: { alignItems: "center", marginTop: moderateScale(20) },
  infoImg: { height: 30, width: 30 },
});
