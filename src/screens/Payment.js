import { Image, ImageBackground, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { Height, Width } from '../dimension/dimension'
import { ScrollView } from 'react-native-gesture-handler'

const Payment = (props) => {
  return (
    <View style={styles.main}>


          <ImageBackground
              resizeMode='contain'
              source={imageConstant.header2}
              style={styles.headerImage}
          >
              <View style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                  width: "90%",
                  alignSelf: "center",
                  marginTop: Platform.OS == 'ios' ? 50:  20
              }}>
                  <TouchableOpacity 
                  style={{
                    position: "absolute",
                    left: 5,
                    zIndex:1,
                  }}
                   onPress={()=>props.navigation.goBack()}>
                  <Image
                      resizeMode='contain'
                      source={imageConstant.back}
                      
                      style={{
                          width: 20,
                          height: 20,
                          tintColor:colorConstant.white
                          
                      }} />
                  </TouchableOpacity>

                  <Text style={{
                      fontSize: 16,
                      fontFamily: fontConstant.semi,
                      color: colorConstant.white,
                      textAlign: "center"
                  }}>~ Liza Garcia! ksjahdksj~</Text>
              </View>

             

              <Text style={{
                  fontSize: 22,
                  fontFamily: fontConstant.semi,
                  color: colorConstant.white,
                  marginTop:20,
                  alignSelf:"center",
              }}>Payments</Text>

                 
             
                
                   <Text style={{
                      fontSize: 29,
                      fontFamily: fontConstant.semi,
                      color: colorConstant.white,
                      alignSelf:"center",
                      marginTop:15


                  }}>$168.89</Text>


                  <TouchableOpacity 
                  activeOpacity={0.9}
                  onPress={()=>{props.navigation.navigate("Withdraw")}}
                  style={{
                    fontSize: 29,
                    fontFamily: fontConstant.semi,
                    backgroundColor:colorConstant.white,
                    width:"25%",
                    height:30,
                    justifyContent:"center",
                    alignItems:"center",
                    borderRadius:10,
                    alignSelf:"center",
                    marginTop:10
                }}>
                      <Text 
                      style={{
                        fontSize:12,
                        fontFamily: fontConstant.regular,
                        color: colorConstant.purple,
                      }}>Withdraw</Text>
                  </TouchableOpacity>
                    


            <Image
            resizeMode='contain'
            source={imageConstant.profile5}
            style={{
                width:100,
                height:100,
                position:"absolute",
                left:Width/2 - 50,
                bottom:-55
            }}/>
          </ImageBackground>


          <ScrollView 
          bounces={false}
          contentContainerStyle={styles.container}>
            
              <TouchableOpacity 
              style={styles.row} activeOpacity={0.9}
                      onPress={() => props.navigation.navigate("AddBankInfo", {
                          data: "pre"
                      })}>
                  <View style={{
                      flexDirection:"row",
                      alignItems:"center"
                  }}>
                  <Image
                      resizeMode='contain'
                      source={imageConstant.bank}
                      style={{
                          width:30,
                          height:30,
                          marginRight:10
                      }} />
                  <Text style={styles.text}>CHASE BANK</Text>
                  </View>
                 
                      <Image
                          resizeMode='contain'
                          source={imageConstant.angle}
                          style={styles.img} />



              </TouchableOpacity>

              <TouchableOpacity 
              style={styles.row} activeOpacity={0.9}
                      onPress={() => props.navigation.navigate("AddBankInfo")}>
                  <View style={{
                      flexDirection:"row",
                      alignItems:"center"
                  }}>
                  <Image
                      resizeMode='contain'
                      source={imageConstant.plus}
                      style={{
                          width:30,
                          height:30,
                          marginRight:10
                      }} />
                  <Text style={styles.text}>Add Bank Info</Text>
                  </View>
                 
                      <Image
                          resizeMode='contain'
                          source={imageConstant.angle}
                          style={styles.img} />



              </TouchableOpacity>
             
    
          </ScrollView>
    </View>
  )
}

export default Payment

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white,
    },
    headerImage:{
        width:Width,
        height:null,
        aspectRatio: Platform.OS == 'ios' ? 1/0.682 :1/0.562,

    },
    container:{
        paddingVertical :70

    },
    img:{
        width:10,
        height:16
    },
    text:{
        fontSize:16,
        fontFamily:fontConstant.semi,
        color:colorConstant.black
    },
    row:{
        flexDirection: "row",
        width: "90%",
        alignSelf: "center",
        justifyContent: "space-between",
        marginTop: 30,
        alignItems: "center",

    }
})