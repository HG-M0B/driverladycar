import { SafeAreaView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colorConstant } from '../utils/constant'
import CustomHeader from '../customComponents/CustomHeader'
import TabStack from './TabStack'

const TripsList = (props) => {
  return (
    <SafeAreaView style={styles.main}>
        <CustomHeader
        color={colorConstant.purple}
        headerText={'Trip Request'}
        navigation ={props.navigation}
        />
     <TabStack/>
    </SafeAreaView>
  )
}

export default TripsList

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    }
})