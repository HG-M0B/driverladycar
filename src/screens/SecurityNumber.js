import { Platform, StyleSheet, Text, TextInput, View ,TouchableOpacity, Keyboard} from 'react-native'
import React, { useState} from 'react'
import CustomHeader from '../customComponents/CustomHeader'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import CustomRoundButton from '../customComponents/CustomRoundButton'
import { useRef } from 'react'
import toastShow from '../utils/toastShow'

const SecurityNumber = (props) => {
const [current ,setCurrent] =  useState(1)
const [first,setFirst] = useState("")
const [second,setSecond] = useState("")
const [third,setThird] = useState("")

const documentsData=props?.route?.params?.documentsData

console.log(documentsData)

const ref2 = useRef(null)  
const ref3 = useRef(null) 
    const onFocus=(value,name)=>{
        if(name === 1)
        {
            setCurrent(1);
            setFirst(value);
            if(value.length > 2)
            {
                
                setCurrent(2);
            }
        }
        if(name === 2)
        {
            setCurrent(2);
            setSecond(value)
            if(value.length > 1)
            {
               
                setCurrent(3);
            }
        }
        if(name === 3)
        {
            setCurrent(3);
            setThird(value)
            if(value.length > 3)
            {
                 Keyboard.dismiss();
                setCurrent(null)
            }
        }
    }

    const onSubmitEditing=()=>{

    }

    const isValidSSN=()=>{
        let str =  first.length > 0 && second.length > 0 && third.length > 0 ? first+"-"+second+"-"+third : "" ;
        console.log("SSN==>",str)
        let regex = /^(?!666|000|9\d{2})\d{3}-(?!00)\d{2}-(?!0{4})\d{4}$/;
        if (str == "") {
            console.log("blank")
            // alert('sjakhdsakj')
            toastShow("Please enter your SSN",colorConstant.errorRed)
        }


        // props.navigation.navigate("AdditionalInfo",{
        //     documentsData:documentsData,
        //     ssnNumber:str
        // })
    
        else if (regex.test(str) == true) 
        {
            props.navigation.navigate("AdditionalInfo",{
                documentsData:documentsData,
                ssnNumber:str
            })

        }
        else {
            toastShow("SSN number is not Valid",colorConstant.errorRed);
        }
    }
  return (
      <View style={styles.main}>
          <CustomHeader
                marginTop={35}
              img={imageConstant.cancel}
              tintColor={colorConstant.black}
              headerText={'Your information'}
              navigation = {props.navigation}
          />
          <TouchableOpacity activeOpacity={1} onPress={()=>
            {
                Keyboard.dismiss();
                setCurrent(null)
            }} >

              <Text style={styles.text1}>Social Security Number</Text>
              <Text style={styles.text2}>We'll need it to pay you and to run your background check  -  it won't be used for a credit check</Text>

              <Text style={styles.text3}>Social Security Number</Text>
              <View style={styles.row}>
                  <TextInput
                    value={first}
                      onChangeText={(text) => onFocus(text, 1)}
                      maxLength={3}
                      style={[{ ...styles.input },
                      {
                          borderWidth :  current === 1 ? 2 :1,
                          borderColor: current === 1 ? colorConstant.purple : colorConstant.black
                      }
                      ]}
                      secureTextEntry={true}
                      keyboardType='number-pad'
                      onFocus={() => setCurrent(1)}
                  />
                  <TextInput
                     ref={ref2}
                      maxLength={2}
                      value={second}
                      onChangeText={(text) => onFocus(text, 2)}
                      style={[{ ...styles.input },
                      {
                          width: "20%",
                          borderWidth :  current === 2 ?2 :1,
                          borderColor: current === 2 ? colorConstant.purple : colorConstant.black
                      }
                      ]}
                      secureTextEntry={true}
                      keyboardType='number-pad'
                      onFocus={() => setCurrent(2)}
                  />
                  <TextInput
                      maxLength={4}
                      value={third}
                      onChangeText={(text) => onFocus(text, 3)}
                      style={[{ ...styles.input },
                      {
                        borderWidth :  current === 3 ? 2 :1,
                          borderColor: current === 3 ? colorConstant.purple : colorConstant.black
                      }
                      ]}
                      secureTextEntry={true}
                      onFocus={() => setCurrent(3)}
                      keyboardType='number-pad'
                  />
              </View>
          </TouchableOpacity>
         
          <CustomRoundButton
              buttonText={'Save'}
              marginTop={35}
              OnButtonPress={isValidSSN}
          />
      </View>
  )
}

export default SecurityNumber

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    text1:{
        fontSize:18,
        fontFamily:fontConstant.bold,
        color:colorConstant.black,
        width:"90%",
        alignSelf:"center",
        marginTop:35
    },
    text2:{
        fontSize:16,
        fontFamily:fontConstant.regular,
        color:colorConstant.black,
        width:"90%",
        alignSelf:"center",
        marginTop:15
    },
    text3:{
        fontSize:12,
        fontFamily:fontConstant.semi,
        color:colorConstant.black,
        width:"90%",
        alignSelf:"center",
        marginTop:25
    },
    row:{
        width:"90%",
        alignSelf:"center",
        flexDirection:"row",
        justifyContent:"space-between",
        marginTop:15
    },
    input:{
        fontSize:24,
        width:"35%",
        borderWidth:1,
        borderRadius:10,
        paddingHorizontal:20,
        ...Platform.select({
            ios:{
                height:45
            }
        })
        
    }
})