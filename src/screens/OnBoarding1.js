import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colorConstant, imageConstant } from '../utils/constant'
import { Height } from '../dimension/dimension'
import CustomButton from '../customComponents/CustomButton'
import { heightPercentageToDP } from '../utils/responsiveFIle'

const OnBoarding1 = (props) => {
  const {navigation} = props;
  return (
    <View style={styles.main}>
      <Image 
        source={imageConstant.boarding1}
        style={styles.img}
        resizeMode='contain'
        />
        <CustomButton
            buttonText={'Continue'}
            position={'absolute'}
            bottom={20}
            OnButtonPress={()=>navigation.navigate('OnBoarding2')}
        />
    </View>
  )
}

export default OnBoarding1

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white,
        // justifyContent:"center",
        alignItems:"center"
    },
    img:{
        marginTop:Height*0.15,
        width:400,
        height:400,
    }

})