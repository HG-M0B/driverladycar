import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Modal,
  PermissionsAndroid,
  Platform,
  SafeAreaView,
  TouchableWithoutFeedback,
} from "react-native";
import React, { useEffect } from "react";
import { color } from "react-native-reanimated";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import CustomHeader from "../customComponents/CustomHeader";
import { Height, Width } from "../dimension/dimension";
import { useState } from "react";
import CustomRoundButton from "../customComponents/CustomRoundButton";
import { heightPercentageToDP } from "../utils/responsiveFIle";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { useDispatch } from "react-redux";
import { actions } from "../redux/reducers";
import CustomButton from "../customComponents/CustomButton";
import CheckBox from "@react-native-community/checkbox";
import { KeyboardAwareScrollView } from "@codler/react-native-keyboard-aware-scroll-view";
import axios from "../utils/api";
import { uploadImageServices } from "../services/uploadImageServices";
import AsyncStorage from "@react-native-async-storage/async-storage";
import LoaderModal from "../customComponents/LoaderModal";
import toastShow from "../utils/toastShow";
import { moderateScale } from "react-native-size-matters";

let initialState = {
  about: "",
  smoke: false,
  music: false,
  message: false,
  pets: false,
  opt: [],
};

const AboutDriver = (props) => {
  const [iState, updateState] = useState(initialState);
  const [modalVisible, setModalVisible] = useState(false);
  const [profileModal, setProfileVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [image, setImage] = useState("");
  const {
    about,
    opt: [],
    smoke,
    message,
    pets,
    music,
  } = iState;
  const disptach = useDispatch();

  // console.log(props.route.params, "ldjshfkjdghkdfjhgdfkj");

  let paramData = props?.route?.params;

  // ***********Additional Functions ****************

  const aboutHandler = (text) => {
    updateState({ ...iState, about: text });
  };
  const selectOption = (index) => {
    let tempOpt = [...opt, index];
    // console.log("tempOption",tempOpt)
    updateState({ ...iState, opt: tempOpt });
  };

  const onContinue = () => {
    // alert('djdjddj')
    setModalVisible(true);
  };

  const onProfileClick = () => {
    // alert('profile')
    setModalVisible(false);
    // setTimeout(() => {
      handleOpenCamera();
    // }, 400);
  };
  const onLogin = () => {
    handleData();
    setProfileVisible(false);
    setTimeout(() => {
      disptach(actions.setIntroStatus("main"));
    }, 200);
  };

  // **********Open camera***********
  const requestCameraPermission = async () => {
    if (Platform.OS === "android") {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: "Camera Permission",
            message: "App needs camera permission",
          }
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };
  const requestExternalWritePermission = async () => {
    if (Platform.OS === "android") {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: "External Storage Write Permission",
            message: "App needs write permission",
          }
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert("Write permission err", err);
      }
      return false;
    } else return true;
  };
  const handleOpenCamera = async () => {
    if (Platform.OS === "android") {
      let isCameraPermitted = await requestCameraPermission();
      let isStoragePermitted = await requestExternalWritePermission();
      if (isCameraPermitted && isStoragePermitted) {
        let options = {
          mediaType: "photo",
          cameraType: "back",
          saveToPhotos: true,
          quality: 0.9,
          maxWidth: 500,
          maxHeight: 500,
          includeBase64: true,
          noData: true,
        };
        await launchCamera(options, (response) => {
          if (response.didCancel) {
            console.log("Cancelled");
          } else if (response.error) {
            console.log("Error", response.errorMessage);
          } else {
            // console.log("got image response", response);
            let formDataReq = new FormData();
            let data = {
              uri: response?.assets?.[0]?.uri,
              type: response?.assets?.[0]?.type,
              name: response?.assets?.[0]?.fileName,
            };
            formDataReq.append("file", data);

            getImageData(formDataReq._parts[0][1]);

            setProfileVisible(true);
          }
        });
      }
    } else {
      let options = {
        mediaType: "photo",
        cameraType: "back",
        saveToPhotos: true,
        quality: 0.9,
        maxWidth: 500,
        maxHeight: 500,
        //  includeBase64: true,
        noData: true,
      };
      setTimeout(async () => {
        await launchCamera(options, (response) => {
          if (response.didCancel) {
            console.log("Cancelled");
          } else if (response.error) {
            console.log("Error", response.errorMessage);
          } else {
            // console.log("got image response", response);
            let formDataReq = new FormData();
            let data = {
              uri: response?.assets?.[0]?.uri,
              type: response?.assets?.[0]?.type,
              name: response?.assets?.[0]?.fileName,
            };
            formDataReq.append("file", data);
              console.log(data,'data 2222')
            getImageData(formDataReq._parts[0][1]);

          }
        });
      }, 200);
    }
  };

  // #region for upload image in server
  const getImageData = async (imageName) => {
    setIsLoading(true);
    let finalFormData = new FormData();
    finalFormData.append("fileName", imageName);
    try {
      const response = await uploadImageServices(finalFormData);
      setProfileVisible(true);

      console.log('====================================');
      console.log(response.data);
      console.log('====================================');
      setImage(response.data.data);
      setIsLoading(false);
      console.log("====================================");
      console.log(image, "lksdjflsdfjkl");
      console.log("====================================");
      // console.log("009999999922727272727272",response.data.data)
    } catch (error) {
      setIsLoading(false);
      console.log(error);
    }
  };

  // ***********Api call**************

  // console.log('====================================');
  // console.log(image);
  // console.log('====================================');

  const handleData = async () => {
    console.log(image,'ksdljsklfjldskf')
    if (image == "") {
      toastShow("Please try again",colorConstant.errorRed);
      return;
    }

  
    const getToken = await AsyncStorage.getItem("token");
    // console.log(
    //   image,
    //   ""
    // );
    let objToSend = {
      about_you: about,
      profilePic: image,
      is_smoking: smoke,
      is_music: music,
      is_chat: message,
      is_pet: pets,
    };

    console.log(objToSend, "jhsdjfdlksjfkldsjflksdjflskdjflksdjfsldkfj");

    try {
      let response = await axios.post("user/driver-about-update", objToSend, {
        headers: {
          "Content-Type": "application/json",
          Authorization: getToken,
        },
      });

      // console.log(response.data,objToSend,'----00000123usd,fhdkjshfkjsdfdsfhkjsdhfjksdhfkjsdhfjksdhfkjsdhfksdjfh9d8fu9d8');

      if (response && response.status == 200) {
        console.log("resp", response);
        setProfileVisible(false);
        setTimeout(() => {
          disptach(actions.setIntroStatus("main"));
        }, 200);
      } else {
        alert("Else");
        console.log(objToSend);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleData2 = async () => {
    const getToken = await AsyncStorage.getItem("token");
    let objToSend = {
      profilePic: image,
      about_you: about,
      is_smoking: smoke,
      is_music: music,
      is_chat: message,
      is_pet: pets,
    };

    console.log(objToSend, "jhsdjfdlksjfkldsjflksdjflskdjflksdjfsldkfj");

    try {
      let response = await axios.post("user/driver-about-update", objToSend, {
        headers: {
          "Content-Type": "application/json",
          Authorization: getToken,
        },
      });

      // console.log(response.data,objToSend,'----00000123usd,fhdkjshfkjsdfdsfhkjsdhfjksdhfkjsdhfjksdhfkjsdhfksdjfh9d8fu9d8');

      if (response && response.status == 200) {
        console.log("resp", response);
        props.navigation.navigate('Profile');
      } else {
        alert("Else");
        console.log(objToSend);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    let mount = true;
    if (mount) {
      if (paramData !== undefined && paramData.flow == "mainStack") {
        // alert(paramData.driverProfile.is_chat)
        updateState({
          ...iState,
          about: paramData.driverProfile.about_you,
          smoke: paramData.driverProfile.is_smoking,
          music: paramData.driverProfile.is_music,
          message: paramData.driverProfile.is_chat,
          pets: paramData.driverProfile.is_pet,
        });
        setImage(paramData.driverProfile.profilePic);
      }
    }

    return () => {
      mount = false;
    };
  }, []);

  // console.log({ about, smoke,message});
  return (
    <SafeAreaView style={styles.main}>
      <LoaderModal isLoading={isLoading} />
      <CustomHeader navigation={props.navigation} />
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        bounces={false}
      >
        <Text style={styles.text}>Write about you</Text>

        <TextInput
          value={about}
          placeholder={`What do you like to do? Hobbies? ${"\n"}Where are you from? etc`}
          placeholderTextColor={colorConstant.grayText}
          style={styles.input}
          multiline={true}
          onChangeText={(val)=> updateState({ ...iState, about: val })}
          // onChangeText={(val)=>aboutHandler(val)}
        />
        <View style={styles.row}>
          <Image resizeMode="contain" source={imageConstant.option} />
          <Image
            source={imageConstant.dog}
            style={{
              width: 30,
              height: 30,
              marginLeft: 25,
            }}
          />
        </View>

        {/* ***Checkbox */}
        <View style={styles.row1}>
          <CheckBox
            tintColors={{ true: colorConstant.purple }}
            style={{
              marginHorizontal: 15,
              width: 30,
              height: 30,
            }}
            value={smoke}
            onValueChange={(value) => {
              updateState({ ...iState, smoke: value });
            }}
          />
          <CheckBox
            style={{
              marginHorizontal: 15,
              width: 30,
              height: 30,
            }}
            tintColors={{ true: colorConstant.purple }}
            value={music}
            onValueChange={(value) => {
              updateState({ ...iState, music: value });
            }}
          />
          <CheckBox
            style={{
              marginHorizontal: 15,
              width: 30,
              height: 30,
            }}
            tintColors={{ true: colorConstant.purple }}
            value={message}
            onValueChange={(value) => {
              updateState({ ...iState, message: value });
            }}
          />
          <CheckBox
            style={{
              marginHorizontal: 15,
              width: 30,
              height: 30,
            }}
            tintColors={{ true: colorConstant.purple }}
            value={pets}
            onValueChange={(value) => {
              updateState({ ...iState, pets: value });
            }}
          />
        </View>

        {paramData?.flow !== "mainStack" && (
          <CustomButton
            buttonText={"Continue"}
            width={"75%"}
            marginTop={heightPercentageToDP("18%")}
            OnButtonPress={onContinue}
          />
        )}

{paramData?.flow == "mainStack"&& <TouchableOpacity
          onPress={handleData2}
          activeOpacity={1}
          style={[
            styles.buttonView1,
            { width: "75%", paddingVertical: moderateScale(3) },
          ]}
        >
          <Text style={styles.text4}>Continue</Text>
        </TouchableOpacity>}
      </KeyboardAwareScrollView>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {}}
      >
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => setModalVisible(!modalVisible)}
          style={styles.centeredView}
        >
          <TouchableOpacity activeOpacity={1} style={styles.modalView}>
            <View style={styles.circle}>
              <Image
                source={imageConstant.profile}
                resizeMode="contain"
                style={{
                  width: 50,
                  height: 50,
                }}
              />
            </View>

            <Text style={styles.text1}>Hi Liza Garcia!</Text>
            <Text style={styles.text2}>
              It is required to have a profile photo on LadyCar
            </Text>
            <Text
              style={[
                { ...styles.text3 },
                {
                  width: "100%",
                  textAlign: "justify",
                  lineHeight: 20,
                },
              ]}
            >
              LadyCar’s goal is safety for passengers and drivers. To ensure
              safe trips, please upload or take a profile picture to your
              account.
            </Text>

            <TouchableOpacity
              onPress={onProfileClick}
              activeOpacity={1}
              style={styles.buttonView}
            >
              <Text style={styles.text4}>Profile photo</Text>
              <Image
                resizeMode="contain"
                source={imageConstant.slide}
                style={{
                  width: 17,
                  height: 17,
                  marginLeft: 10,
                  marginTop: 2,
                }}
              />
            </TouchableOpacity>
          </TouchableOpacity>
        </TouchableOpacity>
      </Modal>

      <Modal
        animationType="slide"
        transparent={true}
        visible={profileModal}
        onRequestClose={() => {
          setProfileVisible(false);
        }}
      >
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => setProfileVisible(false)}
          style={styles.centeredView}
        >
          <TouchableOpacity activeOpacity={1} style={styles.modalView}>
            <Image
              source={imageConstant.done}
              resizeMode="contain"
              style={{
                marginVertical: 20,
                width: 100,
                height: 100,
                alignSelf: "center",
              }}
            />

            <Text style={styles.text1}>Congratulations!</Text>
            <Text style={styles.text2}>Your Profile is all set</Text>
            <Text
              style={{
                fontSize: 16,
                fontFamily: fontConstant.regular,
                color: colorConstant.black,
                lineHeight: 23,
                textAlign: "left",
                width: "100%",
              }}
            >
              Safety is LadyCar's priority for passengers and drivers. You can
              now drive with LadyCar and enjoy your trips!
            </Text>

            <TouchableOpacity
              onPress={handleData}
              // activeOpacity={1}
              style={styles.buttonView1}
            >
              <Text style={styles.text4}>Continue</Text>
            </TouchableOpacity>
          </TouchableOpacity>
        </TouchableOpacity>
      </Modal>
    </SafeAreaView>
  );
};

export default AboutDriver;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
  },
  scrollView: {
    ...Platform.select({
      ios: {
        paddingBottom: 20,
      },
    }),
  },
  text: {
    fontSize: 28,
    fontFamily: fontConstant.bold,
    color: colorConstant.purple,
    textAlign: "center",
    marginTop: 30,
  },
  row: {
    flexDirection: "row",
    alignSelf: "center",
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
  },
  input: {
    width: "75%",
    backgroundColor: colorConstant.white,
    alignSelf: "center",
    borderWidth: 1.5,
    marginTop: 30,
    height: Height * 0.3,
    borderColor: colorConstant.purple,
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 9,
    },
    shadowOpacity: 0.48,
    shadowRadius: 11.95,
    elevation: 18,
    ...Platform.select({
      ios: {
        paddingTop: 15,
        paddingLeft: 18,
      },
      android: {
        textAlignVertical: "top",
        padding: 25,
      },
    }),
  },
  dot: {
    width: 15,
    height: 15,
    borderWidth: 2,
    borderRadius: 10,
    marginHorizontal: 24,
    marginTop: 20,
    borderColor: colorConstant.purple,
  },
  row1: {
    flexDirection: "row",
    width: "60%",
    alignSelf: "center",
    justifyContent: "space-between",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(1,1,1,0.5)",
  },
  modalView: {
    margin: 20,
    width: "80%",
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  circle: {
    marginTop: 20,
    width: 100,
    height: 100,
    borderWidth: 8,
    borderColor: colorConstant.purple,
    borderRadius: 50,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  text1: {
    fontSize: 18,
    fontFamily: fontConstant.semi,
    color: colorConstant.purple,
    marginTop: 20,
  },
  text2: {
    fontSize: 17,
    // fontFamily:fontConstant.semi,
    fontWeight: "600",
    color: colorConstant.black,
    marginVertical: 10,
  },
  text3: {
    fontSize: 13,
    fontFamily: fontConstant.regular,
    color: colorConstant.black,
  },
  buttonView: {
    width: "100%",
    backgroundColor: colorConstant.purple,
    alignSelf: "center",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 50,
    marginBottom: 20,
    borderRadius: 10,
  },
  buttonView1: {
    width: "100%",
    backgroundColor: colorConstant.purple,
    alignSelf: "center",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 50,
    marginBottom: 20,
    borderRadius: 10,
  },
  text4: {
    fontSize: 17,
    fontFamily: fontConstant.bold,
    color: colorConstant.white,
    ...Platform.select({
      ios: {
        marginVertical: 13,
      },
      android: {
        marginVertical: 7,
      },
    }),
  },
});
