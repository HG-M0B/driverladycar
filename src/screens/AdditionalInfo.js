import { Platform, StyleSheet, Text, TextInput, View } from 'react-native'
import React ,{useState}from 'react'
import { colorConstant, fontConstant } from '../utils/constant'
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'
import CustomInput from '../customComponents/CustomInput'
import CustomDropDown from '../customComponents/CustomDropDown'
import CustomButton from '../customComponents/CustomButton'
import { Height } from '../dimension/dimension'
import { useSelector } from 'react-redux'
import { heightPercentageToDP } from '../utils/responsiveFIle'
import { useEffect } from 'react'
import axios from '../utils/api';
import Validations from '../utils/Validations'
import toastShow from '../utils/toastShow'

let initialState = {
  plateNumber : "",
  year:"",
  make:"",
  selectedState:"",
  selectedStateId:null,
  model:"",
  vehicleType:"",
  color:"",
  numberDoor:"",
  numberSeat:"",
  vehicleTypeData:null,
  vehicleTypeID:""

}


const AdditionalInfo = (props) => {
  console.log(props?.route?.params?.ssnNumber,'kkkk')

const [iState,updateState] = useState(initialState);
const {
    plateNumber,
    year,
    make,
    selectedState,
    selectedStateId,
    model,
    vehicleType,
    color,
    numberDoor,
    numberSeat,
    vehicleTypeData,
    vehicleTypeID
  } = iState;
const {stateList} = useSelector((state) => state.reducers);
const docsData =  props?.route.params?.documentsData
const ssnNumber=props?.route?.params?.ssnNumber
let numberOfDoorsData = [
  {
    label:"4",
    value:"4"
  },
  {
    label:"5",
    value:"5"
  },
  {
    label:"6",
    value:"6"
  },
  {
    label:"7",
    value:"7"
  },
  {
    label:"8",
    value:"8"
  }
]

let numberOfSeatData = [
  {
    label:"1",
    value:"1"
  },
  {
    label:"2",
    value:"2"
  },
  {
    label:"3",
    value:"3"
  },
  {
    label:"4",
    value:"4"
  },
  {
    label:"5",
    value:"5"
  },
  {
    label:"6",
    value:"6"
  },
  {
    label:"7",
    value:"7"
  },
  {
    label:"8",
    value:"8"
  }
]

useEffect(()=>{
  let subs =  props.navigation.addListener('focus',()=>{
    vehicleTypeApiCall();
  })
  return ()=>subs;
},[])

const Validater=()=>{
  Validations.isEmpty(plateNumber)
  ? toastShow("Please enter plate number",colorConstant.errorRed)
  : Validations.isEmpty(year)
  ? toastShow("Please enter year",colorConstant.errorRed)
  : Validations.isEmpty(make)
  ? toastShow("Please enter Make",colorConstant.errorRed)
  : Validations.isEmpty(model)
  ? toastShow("Please enter model",colorConstant.errorRed)
  : Validations.isEmpty(vehicleTypeID)
  ? toastShow("Please enter select Vehicle Type",colorConstant.errorRed)
  : Validations.isEmpty(color)
  ? toastShow("Please enter color",colorConstant.errorRed)
  : Validations.isEmpty(numberDoor)
  ? toastShow("Please select number of door's",colorConstant.errorRed)
  : Validations.isEmpty(numberSeat)
  ? toastShow("Please select number of seat's",colorConstant.errorRed)
  : navigateTo();

}

  const inputHandler = (e, inputName) => {
    // console.log(e.nativeEvent.text)
    updateState({
      ...iState,
      [inputName]: e.nativeEvent.text
    })
  }
   const stateHandler=(item)=>{
    updateState({...iState,selectedState:item.name,selectedStateId:item._id})
   } 
   const doorHandler=(item)=>{
    updateState({
      ...iState,
      numberDoor:item.value
    })
   }
   const seatHandler=(item)=>{
    updateState({
      ...iState,
      numberSeat:item.value
    })
   }
  const onChangeCall=(item)=>{
    // console.log(item)
   updateState({...iState,vehicleTypeID:item._id})
  }
  const vehicleTypeApiCall = async ()=>{
    // console.log("vehicle type data ")
    try{
      let response = await axios.get(`auth/vehicle-type`);
      if(response && response.status == 200)
      {
        updateState({...iState,vehicleTypeData:response.data.result})
      }
    }
    catch(error){
      console.log("err",error.message)
    }
  }

  const navigateTo=()=>{
    let addInfoData = {
      plateNumber,year,make,model,vehicleTypeID,color,numberDoor,numberSeat
    }
    props.navigation.navigate("FaceRecog",{docsData,addInfoData,ssnNumber})
  }
  // console.log("plate",iState)
  return (
    <View style={styles.main}>
      
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
      >
         <View style={{
          //  height:Height*0.99,
         }}>
        <Text style={styles.text1}>Additional Information About {'\n'} Your Personal Vehicle</Text>
        <CustomInput
          borderWidth={0}
          placeholder={'License Plate'}
          marginTop={45}
          onChange={(e)=>inputHandler(e,"plateNumber")}
        />
         <CustomInput
            borderWidth={0}
            placeholder={'Year'}
            marginTop={20}
            onChange={(e)=>inputHandler(e,"year")}
            keyboardType="numeric"
          />
          {/* <TextInput keyboardType='numeric' */}
         <CustomInput
          // value={make}
            borderWidth={0}
            placeholder={'Make'}
            marginTop={20}
            onChange={(e)=>inputHandler(e,"make")}
          />
          <CustomInput
            borderWidth={0}
            placeholder={'Model'}
            marginTop={20}
            onChange={(e)=>inputHandler(e,"model")}
          />
   
          <CustomDropDown
            dropDownData={vehicleTypeData}
            labelField="vehicleType"
            valueField="_id"
            marginTop={20}
            placeholder={'Vehicle Type'}
            onChange={onChangeCall}
          />
          <CustomInput
            borderWidth={0}
            placeholder={'Color'}
            marginTop={20}
            onChange={(e)=>inputHandler(e,"color")}
          />
        <CustomDropDown
          value={numberDoor}
          labelField='label'
          valueField='value'
          marginTop={20}
          placeholder={"Number of door's"}
          dropDownData={numberOfDoorsData}
          onChange={doorHandler}
        />
        <CustomDropDown
          value={numberSeat}
          labelField='label'
          valueField='value'
          marginVertical={20}
          placeholder={"Number of seat belts"}
          marginBottom={70}
          dropDownData={numberOfSeatData}
          onChange={seatHandler}

        />
        <CustomButton
          width={"80%"}
          buttonText={'Continue'}
          // position={"absolut  e"}
          bottom={20}
          OnButtonPress={Validater}
          // OnButtonPress={() => props.navigation.navigate("FaceRecog")}
        />
        </View>
      </KeyboardAwareScrollView>
    
    </View>
  )
}

export default AdditionalInfo

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    text1:{
        fontSize:24,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        textAlign:"center",
        marginTop: Platform.OS == 'ios' ? 70: 40
    }
})