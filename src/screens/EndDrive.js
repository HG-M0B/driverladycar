import { Image, StyleSheet, Text, View,ScrollView, SafeAreaView,TouchableOpacity, PermissionsAndroid, BackHandler, Platform } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import CustomMapView from '../customComponents/CustomMapView'
import { Height, Width } from '../dimension/dimension'
import CustomButton from '../customComponents/CustomButton'
import CustomCard from '../customComponents/CustomCard'
import { useFocusEffect } from '@react-navigation/native'


const EndDrive = (props) => {
    useFocusEffect(
        React.useCallback(() => {
          const backAction = () => {
            console.log("called")
            props.navigation.navigate("SafeDrive")
            return true;
          };
      
          const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
          );
      
          return () => backHandler.remove();
        }, [])
      );
  return (
      <SafeAreaView style={styles.main}>
          <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
          <View style={{
              backgroundColor:"red",
              ...Platform.select({
                  ios:{
                    height:Height * 0.65
                  }
              })
          }}>
              <CustomMapView
                  height={Platform.OS == 'ios' ? Height * 0.65 : Height * 0.70}

              />

              <View style={styles.row}>
                  <TouchableOpacity
                      onPress={() => props.navigation.toggleDrawer()}
                  >
                      <Image
                          resizeMode='contain'
                          source={imageConstant.drawer}
                          style={styles.img} />
                  </TouchableOpacity>
                  <TouchableOpacity
                      activeOpacity={1}
                      onPress={() => props.navigation.navigate("Profile")} >
                      <Image
                          resizeMode='contain'
                          source={imageConstant.profile1}
                          style={{
                              width: 35,
                              height: 35
                          }} />
                  </TouchableOpacity>

              </View>


              <CustomCard
                  position={'absolute'}
                  bottom={10}
              />
          </View>
      


          <View style={{
            ...Platform.select({
                ios:{
                  height:Height * 0.25
                }
            })
          }}>
              <View style={{
                  flexDirection: "row",
                  width: "90%",
                  alignItems: "center",
                  alignSelf: "center",
                  marginTop: 30
              }}>
                  <Image
                      resizeMode='contain'
                      source={imageConstant.car}
                      style={{
                          width: 50,
                          height: 50,
                          tintColor: colorConstant.black,
                      }} />


                  <View style={{
                      alignItems: "center",
                      marginHorizontal: Width * 0.035
                  }}>
                      <Text
                          style={{
                              fontSize: 14,
                              fontFamily: fontConstant.regular,
                              color: colorConstant.black
                          }}>4 min</Text>
                      <Image
                          resizeMode='contain'
                          source={imageConstant.arrow}
                          style={{
                              width: Width * 0.15,
                              height: 10,
                          }} />
                      <Text
                          style={{
                              fontSize: 8,
                              fontWeight: "400",
                              fontFamily: fontConstant.regular,
                              color: colorConstant.black
                          }}
                      >Your Location</Text>
                  </View>



                  <Image
                      resizeMode='contain'
                      source={imageConstant.locationgreen}
                      style={{
                          width: 30,
                          height: 30
                      }} />

                  <View style={{
                      alignItems: "center",
                      marginHorizontal: Width * 0.035
                  }}>
                      <Text style={{
                          fontSize: 14,
                          fontFamily: fontConstant.regular,
                          color: colorConstant.black
                      }}>7 min</Text>
                      <Image
                          resizeMode='contain'
                          source={imageConstant.arrow}
                          style={{
                              width: Width * 0.15,
                              height: 10,
                          }} />
                      <Text
                          style={{
                              fontSize: 8,
                              fontWeight: "400",
                              fontFamily: fontConstant.regular,
                              color: colorConstant.black
                          }}
                      >Destination</Text>
                  </View>


                  <Image
                      resizeMode='contain'
                      source={imageConstant.locationred}
                      style={{
                          width: 30,
                          height: 30
                      }} />
                  <View>


                  </View>


                  <Text style={{
                      fontSize: 24,
                      fontFamily: fontConstant.bold,
                      color: colorConstant.black,
                      position: "absolute",
                      right: 0
                  }}>$9</Text>

              </View>
              <CustomButton
                  OnButtonPress={() => props.navigation.navigate("TripSummary")}
                  width={'80%'}
                  position={'absolute'}
                  bottom={Platform.OS == 'ios' ?20:30}
                  buttonText={'Confirm End Of The Ride'} />
          </View>
          </ScrollView>
      </SafeAreaView>
    
  )
}

export default EndDrive

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    row:{
        flexDirection:"row",
        width:"93%",
        alignSelf:"center",
        justifyContent:"space-between",
        top:20,
        position:"absolute",
    },
    row1:{
        flexDirection:"row",
        marginTop:20,
        width:"90%",
        alignSelf:"center",
        alignItems:"center",
    },
    img:{
        width:35,
        height:35
    },
  
    img1:{
        width:15,
        height:15
    },
    text:{
        fontSize:18,
        fontFamily:fontConstant.semi,
        color:colorConstant.black,
        marginLeft:20
    },
    text1:{
        fontSize:10,
        fontFamily:fontConstant.semi,
        color:colorConstant.black,
        position:"absolute",
        top:-10
    }
   
   
})