import { Image, Platform, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { useEffect } from 'react';
import CustomButton from '../customComponents/CustomButton';
import CustomInput from '../customComponents/CustomInput';
import CustomHeader from '../customComponents/CustomHeader';
import { Height } from '../dimension/dimension';


const AddBankInfo = (props) => {
    const {route,navigation} =  props;
    const [button,setButton] =  useState(false)

    useEffect(()=>{
        if(route?.params?.data == "pre")
        {
            setButton(true)
        }
    },[])
  return (

      <SafeAreaView style={{
          flex: 1,
          backgroundColor:colorConstant.white
      }}>

              <CustomHeader
              navigation={props.navigation}/>

          <ScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}
          >
              <Text style={styles.text}>Add Bank Details</Text>
              <CustomInput
                  borderWidth={1}
                  marginTop={Height*0.08}
                  placeholder={'Routing Number'}
              />
              <CustomInput
                  borderWidth={1}
                  marginVertical={Height*0.03}
                  placeholder={'Account Number'}
              />
              <CustomInput
                  borderWidth={1}
                  placeholder={'Name of the Account'}
              />
             
          </ScrollView>
       
          <CustomButton
              position={'absolute'}
              bottom={Platform.OS == 'ios' ? 30 : 0}
              OnButtonPress={() => props.navigation.goBack()}
              width={'80%'}
              buttonText={button ? "Update" : "Add"}
          />
      </SafeAreaView>
   
  )
}

export default AddBankInfo

const styles = StyleSheet.create({
    ViewHeader:{
        width:"90%",
        alignSelf:"center",
        marginTop:20
    },
    back:{
        width:20,
        height:20,
        transform:[{ rotate: '180deg'}],
       
    },
    text:{
        fontSize:32,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        textAlign:"center",
        marginTop:20
    },
    row:{
        flexDirection: "row",
        width: "90%",
        alignSelf: "center",
        justifyContent: "space-between",
        backgroundColor: colorConstant.white,
        marginTop: 40,
        alignItems: "center",
        padding: 15,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,

        elevation: 8,
    },
    img:{
        width:20,
        height:20
    },
    text1:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.black
    }
})