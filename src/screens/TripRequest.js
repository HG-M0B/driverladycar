import React, {useEffect, useCallback, useMemo, useRef,useState, useLayoutEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Platform, SafeAreaView, Modal,ScrollView, BackHandler, FlatList } from 'react-native';
import BottomSheet ,{ BottomSheetScrollView }from '@gorhom/bottom-sheet';
import {colorConstant, fontConstant, imageConstant} from '../utils/constant'
import CustomMapView from '../customComponents/CustomMapView'
import { Rating, AirbnbRating } from 'react-native-ratings';
import { useFocusEffect } from '@react-navigation/native';
import { Height, Width } from '../dimension/dimension';
import CustomButton from '../customComponents/CustomButton';
import CustomModal from '../customComponents/CustomModal';
import {Timer, Countdown} from 'react-native-element-timer';
import { DriverLooactionServices, getUserProfileServices, PassengerRequestList } from '../services/DriverServices';
import toastShow from '../utils/toastShow';
import { moderateScale } from 'react-native-size-matters';


let initialState = {
  counter:20
}

const TripRequest = (props) => {
  const [iState,updateState] =  useState(initialState)
  const [isLoading,setIsLoading]=useState(false)
  const [isActive, setIsActive] = useState(false);

    const sheetRef = useRef(null);
    const [open,setOpen] = useState(true);
    const [modalVisible, setModalVisible] = useState(false);
    const [i,setIndex] = useState(0);
    const countdownRef = useRef(null);
    const [driverProfile,setDriverProfile]=useState({})
    const {counter} = iState;
  // const snapPoints = useMemo(() => ['40%'], []);
  const snapPointsAndroid = useMemo(() => ['40%', "55%"], []);
  const snapPointsIos = useMemo(() => ['52%', "75%"], []);
  const handleSheetChanges = useCallback((index) => {
    sheetRef.current?.snapToIndex(index);
    setOpen(false)
    setIndex(index)
  }, []);

  const getDriverDetailsFunction=async()=>{
    setIsLoading(true)
    try {
      const response=await getUserProfileServices()
      console.log(response.data);
      setDriverProfile(response.data.userData)
      setIsActive(response.data.userData.is_online)
      setIsLoading(false)

    } catch (error) {
      console.log(error)
      setIsLoading(false)
    }
  }


  const onToggle = async () => {
    // setIsActive(!isActive)
    setIsLoading(true)
    let objToSend={
        is_online:false,
        lat:"0",
        long:"0"
    }
    try {
      const response = await DriverLooactionServices(objToSend);
      props.navigation.goBack()
      console.log(response.data)
      setIsLoading(false)
      // await AsyncStorage.setItem("token", response.data.userData.jwtToken);
      // driverProfileFunction()

    } catch (error) {
      console.log(objToSend);
      setIsLoading(false)
      toastShow("Something went wrong plase try again",)
    }
  };

  useEffect(() => {
    let mount=true
    if(mount){
      getDriverDetailsFunction()
    }
  
    return () => {
      mount=false
    }
  }, [])
  
 useEffect(()=>{
   let FocusListener =  props.navigation.addListener('focus',()=>{
    updateState({...iState,counter:20})
   })
   return FocusListener;
 },[])

  // useEffect(()=>{
  //   let ClearID =  setInterval(()=>{
  //     updateState({...iState,counter:counter-1})
  //   },1000)
  //   if(counter === 0)
  //   {
  //     console.log("clear interval")
  //     clearInterval(ClearID)
  //   }
  //   return ()=>{
  //     clearInterval(ClearID);
  //   }
  // },[counter])



  const getPassengerRequestList=async()=>{
    try {
      const response =await PassengerRequestList()
      console.log(response.data,'djhfdskjfhdkjsfhsjdkfh')
    } catch (error) {
        console.log(error)
    }
  }

  useEffect(() => {
    let mount =true

    if(mount){
      getPassengerRequestList()
    }
  
    return () => {
      mount=false
    }
  }, [])
  


 useFocusEffect(
    React.useCallback(() => {
      const backAction = () => {
        props.navigation.goBack();
        return true;
      };
  
      const backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        backAction
      );
  
      return () => backHandler.remove();
    }, [])
  );
console.log("counter ",counter)

  return (
    <SafeAreaView style={{
      flex:1,
      backgroundColor:colorConstant.white
  }}>
      <View style={styles.main}>

        <View style={{
           height:Height * 0.48,

        }}>
          <CustomMapView
            height={Platform.OS == "ios" ? Height * 0.48:  Height * 0.60}
          />
          <TouchableOpacity
            onPress={() => alert("Recenter")}
            activeOpacity={0.8}
            style={{
              position: "absolute",
              right: 0,
              bottom:10
            }}>
            <Image
              resizeMode='contain'
              source={imageConstant.location}
              style={{
                width: 55,
                height: 55
              }}
            />
          </TouchableOpacity>
        </View>


        <View style={styles.headerView}>
          <TouchableOpacity
          onPress={()=>props.navigation.toggleDrawer()}
          >
            <Image
              resizeMode='contain'
              source={imageConstant.drawer}
              style={styles.img} />
          </TouchableOpacity>

          <View style={{
            flexDirection:"row",
            width:"50%",
            justifyContent:"space-between",
            alignItems:"center",
          }}>
          <Text style={{
            fontSize:18,
            fontFamily:fontConstant.semi,
            color:colorConstant.purple
           
          }}>$160.98</Text>
          <View>
          <TouchableOpacity onPress={onToggle}>
          <Text style={styles.text}>{driverProfile.is_online?'Online':'Offline'}</Text>
          </TouchableOpacity>
          {/* <Text style={[styles.text,{marginTop:moderateScale(10)}]}>{'Offline'}</Text> */}
          </View>
          </View>
          <TouchableOpacity 
          activeOpacity={1}
          onPress={() => props.navigation.navigate("Profile")}
          style={{
          }}>
          <Image
            resizeMode='contain'
            source={{uri:driverProfile.profilePic}}
            style={styles.img1} />
          </TouchableOpacity>
          

        </View>

        

        <BottomSheet
          index={i}
          ref={sheetRef}
          snapPoints={Platform.OS == "ios" ? snapPointsIos: snapPointsAndroid}
          onChange={handleSheetChanges}
        // enableContentPanningGesture={true}

        >
          <View style={{
            backgroundColor: colorConstant.white,
            flex: 1,
            shadowColor: "#000",
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,
          }}>

            <BottomSheetScrollView
              contentContainerStyle={{
                paddingVertical: 10
              }}>
              <Text style={styles.text1}>Trip requests{'\n'}Select your passenger</Text>


              <View style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                width: "90%",
                alignSelf: "center",
                marginTop: 20
              }}>


                <View style={{
                  flexDirection: "row"
                }}>
                
                 <TouchableOpacity activeOpacity={0.8} onPress={()=>setModalVisible(!modalVisible)}>
                 <Image
                    resizeMode='contain'
                    source={imageConstant.profile4}
                    style={{
                      height: 45,
                      width: 45
                    }}
                  />
                 </TouchableOpacity>

                  <View
                    style={{
                      justifyContent:"center",
                      alignItems:"center",
                      marginLeft: 10
                    }}>
                    <Text style={{
                      fontSize: 15,
                      fontFamily: fontConstant.semi,
                      color: colorConstant.black,
                      opacity: 1
                    }}>KELSEY</Text>


                    {/* <View style={{
                      flexDirection: "row",
                      alignItems: "center"
                    }}>
                      {/* <Image

                        source={imageConstant.carroof}
                        style={{
                          width: 18,
                          height: 18
                        }} /> */}
                      {/* <Text style={{
                        fontSize: 12,
                        fontFamily: fontConstant.regular,
                        color: colorConstant.black,
                        opacity: 1
                      }}>CLio 5</Text> */}
                    {/* </View> */}


                  </View>
                </View>



                <View style={{
                  flexDirection: "row",
                  alignItems: "center"
                }}>
                  <Image
                    resizeMode='contain'
                    source={imageConstant.star}
                    style={{
                      width: 25,
                      height: 25
                    }}

                  />
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.regular,
                    color: colorConstant.black
                  }}> 4.3</Text>
                </View>



                <View style={{
                  flexDirection: "row",
                  alignItems: "center"
                }}>
                  <Image
                    resizeMode='contain'
                    source={imageConstant.watch}
                    style={{
                      width: 25,
                      height: 25
                    }} />
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.regular,
                    color: colorConstant.black
                  }}> 5 min</Text>
                </View>


              </View>





              <View style={{
                flexDirection: "row",
                width: "90%",
                alignItems: "center",
                alignSelf: "center",
                marginTop: 20
              }}>
                <Image
                  resizeMode='contain'
                  source={imageConstant.car}
                  style={{
                    width: 50,
                    height: 50,
                    tintColor: colorConstant.black,
                  }} />


                <View style={{
                  alignItems: "center",
                  marginHorizontal: Width * 0.035
                }}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontFamily: fontConstant.regular,
                      color: colorConstant.black
                    }}>4 min</Text>
                  <Image
                    resizeMode='contain'
                    source={imageConstant.arrow}
                    style={{
                      width: Width * 0.15,
                      height: 10,
                    }} />
                  <Text
                    style={{
                      fontSize: 8,
                      fontWeight: "400",
                      fontFamily: fontConstant.regular,
                      color: colorConstant.black
                    }}
                  >Your Location</Text>
                </View>



                <Image
                  resizeMode='contain'
                  source={imageConstant.locationgreen}
                  style={{
                    width: 30,
                    height: 30
                  }} />

                <View style={{
                  alignItems: "center",
                  marginHorizontal: Width * 0.035
                }}>
                  <Text style={{
                    fontSize: 14,
                    fontFamily: fontConstant.regular,
                    color: colorConstant.black
                  }}>7 min</Text>
                  <Image
                    resizeMode='contain'
                    source={imageConstant.arrow}
                    style={{
                      width: Width * 0.15,
                      height: 10,
                    }} />
                  <Text
                    style={{
                      fontSize: 8,
                      fontWeight: "400",
                      fontFamily: fontConstant.regular,
                      color: colorConstant.black
                    }}
                  >Destination</Text>
                </View>


                <Image
                  resizeMode='contain'
                  source={imageConstant.locationred}
                  style={{
                    width: 30,
                    height: 30
                  }} />
                <View>


                </View>


                <Text style={{
                  fontSize: 24,
                  fontFamily: fontConstant.bold,
                  color: colorConstant.black,
                  position: "absolute",
                  right: 0
                }}>$9</Text>

              </View>


              <View style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                width: "25%",
                alignSelf: "center",
                marginTop: 10
              }}>
                <Image
                  resizeMode='contain'
                  source={imageConstant.timer}
                  style={{
                    width: 55,
                    height: 55,
                    marginTop: 5
                  }}
                />

                <Text style={{
                  top: -3,
                  fontSize: 24,
                  fontFamily: fontConstant.bold,
                  color: colorConstant.purple,
                }}>{`${counter}s`}</Text>

              </View>



              <View style={{
                flexDirection: "row",
                width: "90%",
                alignSelf: "center",
                justifyContent: "space-between",
                alignItems: "center",
                marginTop: 10
              }}>

                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => props.navigation.navigate("TripsList")}>
                  <Image
                    source={imageConstant.calender}
                    style={{
                      width: Width * 0.08,
                      height: Width * 0.08
                    }}
                    resizeMode='contain'
                  />

                </TouchableOpacity>

                <CustomButton
                  marginTop={0}
                  OnButtonPress={() => props.navigation.navigate("OnWay")}
                  width={'85%'}
                  buttonText={'CONFIRM YOUR TRIP'}
                  backgroundColor={colorConstant.green}
                />
              </View>




            </BottomSheetScrollView>
          </View>

        </BottomSheet>
      </View>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>


            <TouchableOpacity 
            activeOpacity={0.8}
            onPress={()=>setModalVisible(!modalVisible)}
            style={{
              position: "absolute",
              right: 15,
              top: 10,
              padding: 9

            }}>
              <Image
                resizeMode='contain'
                source={imageConstant.cancel}
                style={{
                  width: 8,
                  height: 8,
                  tintColor: colorConstant.grayText

                }}
              />
            </TouchableOpacity>


           

            <View style={{
              flexDirection: "row",
              alignItems: "center",
              width: "100%",
              marginBottom:10,
              justifyContent: "space-between"
            }}>

              <View style={{
                flexDirection: "row",
                alignItems: "center"
              }}>
                <Image
                  resizeMode='contain'
                  source={imageConstant.profile4}
                  style={{
                    width:66,
                    height:66,
                    alignSelf: "center",
                  }}
                />
                <View style={{
                  marginLeft: 10
                }}>

                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.bold,
                    color: colorConstant.black
                  }}>LUCY</Text>

                  <View style={{
                    flexDirection: "row",
                  }}>

                    <Image
                      resizeMode='contain'
                      source={imageConstant.star}
                      style={{
                        width: 15,
                        height: 15,
                        alignSelf: "center"
                      }}
                    />
                    <Text style={{
                      fontSize:14,
                      fontFamily:fontConstant.regular,
                      color:colorConstant.black
                    }}>3.6</Text>
                  </View>
                </View>
              </View>

              <TouchableOpacity
                activeOpacity={0.8}
                style={{
                  width: "30%",
                  height: 30,
                  borderRadius: 10,
                  backgroundColor: colorConstant.purple,
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <Text style={{
                  fontSize: 14,
                  fontFamily: fontConstant.semi,
                  color: colorConstant.white
                }}>Follow</Text>
              </TouchableOpacity>
            </View>


          
           



            <Text style={{
              fontSize: 14,
              fontFamily: fontConstant.regular,
              color: colorConstant.grayText,
              textAlign: "justify",
              marginVertical:6,
              width:"100%",
              fontStyle:"italic"

            }}>
              I’m Sophia from Nevada. I love swimming. Riding with people is a passion for me and it allows me to meet new people and discover new places.
            </Text>

            <View style={[{...styles.row},{alignSelf:"flex-start",marginTop:10}]}>
              <Image
                resizeMode='contain'
                source={imageConstant.option}
              />
              <Image
                source={imageConstant.dog}
                style={{
                  width: 30,
                  height: 30,
                  marginLeft: 23

                }}
              />
            </View>




          
              <Text style={{
                width:"100%",
                fontSize:20,
                fontFamily: fontConstant.bold,
                color: colorConstant.black,
                marginTop:20
              }}>Comments</Text>


            <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                width: "100%",
                height: Height * 0.20,
                paddingVertical: 10,
              }}>
              {
                [
                  {
                    image: imageConstant.profile3,
                    name: "Sophia is a lovely person, we got in touch after our first trip and we ended up being best friends!"
                  },
                  {
                    image: imageConstant.profile4,
                    name: "The trip was super cool, Sophia was very nice and funny! I recommend her :)"
                  },
                  {
                    image: imageConstant.profile4,
                    name: "The trip was super cool, Sophia was very nice and funny! I recommend her :)"
                  }
                ].map((item, index) => {
                  return (
                    <View style={[{ ...styles.row }, { marginVertical: 15, alignItems: "flex-start", alignItems: "center", justifyContent: "center" }]} key={index}>
                      <Image
                        resizeMode='contain'
                        source={item.image}
                        style={{
                          width: 40,
                          height: 40,
                          marginRight: 10
                        }}
                      />
                      <View style={{
                        width: "70%"
                      }}>
                        <Text style={{
                          fontSize: 12,
                          fontFamily: fontConstant.regular,
                          color: colorConstant.black
                        }}>{item.name}</Text>
                        <Text style={{
                          fontSize: 12,
                          fontFamily: fontConstant.regular,
                          color: colorConstant.grayText,
                          fontStyle: "italic"
                        }}>14/02/2021 at 5.24 p.m</Text>
                      </View>

                      <View style={[{ ...styles.row }, { top: -10 }]}>
                        <Image
                          resizeMode='contain'
                          source={imageConstant.star}
                          style={{
                            width: 10,
                            height: 10
                          }}
                        />
                        <Text style={{
                          fontSize: 14,
                          fontFamily: fontConstant.bold,
                          color: colorConstant.black
                        }}>3.9</Text>
                      </View>
                    </View>
                  )
                })
              }
            </ScrollView>


              <FlatList
                renderItem={(item,index)=>{
                  return(
                    <View
                      key={index}>
                      <Image
                        resizeMode='contain'
                        style={{
                          height:70,
                          width:70,
                          margin:5
                        }}
                        source={imageConstant.img}
                      />
                    </View>
                  )
                }}
                data={[{},{},{},{},{},{},{},{}]}
                numColumns={4}
                contentContainerStyle={{
                  width:Width *0.95,
                  // alignSelf:'center',
                  // alignItems:'center',
                  // justifyContent:'center',
                  marginTop:'5%',
                }}
                />
          </View>
        </View>
      </Modal>

    </SafeAreaView>
   
  )
}

export default TripRequest

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:"rgba(255,255,255,0.1)",
        // ...Platform.select({
        //   ios:{
        //     marginTop:40
        //   }
        // })
    },
    contentContainer: {
      paddingVertical:40
      },
      headerView:{
        position:"absolute",
        top:15,
        width:"95%",
        alignSelf:"center",
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-between"
    },
    img:{
        width:35,
        height:35
    },
    text:{
        fontSize:14,
        fontFamily:fontConstant.semi,
        color:colorConstant.white,
        backgroundColor:colorConstant.purple,
        paddingVertical:3,
        paddingHorizontal:25,
        ...Platform.select({
          ios:{
            overflow: 'hidden'
          }
        }),
        borderRadius:5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 12.35,
        elevation: 19,

    },
    img1:{
        width:40,
        height:40,
        borderRadius:40
    },
    text1:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        width:"90%",
        alignSelf:"center"
    },
    card:{
      backgroundColor:colorConstant.white,
      width:"90%",
      alignSelf:"center",
      borderRadius:10,
      marginTop:20,
      paddingVertical:5,
      paddingHorizontal:15,
      flexDirection:"row",
      justifyContent:"space-between",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.34,
      shadowRadius: 6.27,
      elevation: 10,

    },
    row:{
      flexDirection:"row",
      marginTop:15,
      height:30,
      alignItems:"center",

    },
    centeredView: {
      flex: 1,
      backgroundColor:"rgba(1,1,1,0.4)"
    },
    modalView: {
      width:"90%",
      margin: 20,
      backgroundColor: "white",
      borderRadius: 20,
      padding:25,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    row2:{
      flexDirection:"row",
      marginTop:30,
      width:"70%",
      alignSelf:"center",
      alignItems:"center",
  }
})