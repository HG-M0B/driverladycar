import {
  Image,
  Keyboard,
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import React, { useState, useEffect } from "react";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import { Width } from "../dimension/dimension";
import CustomInput from "../customComponents/CustomInput";
import CustomPasswordInput from "../customComponents/CustomPasswordInput";
import CustomButton from "../customComponents/CustomButton";
import {
  LoginWithGoogleServices,
  LoginWithMobileServices,
} from "../services/AuthServices";
import {
  GoogleSignin,
  statusCodes,
} from "@react-native-google-signin/google-signin";
import { WEB_CLIENT_ID } from "../utils/HelperConstant";
import toastShow from "../utils/toastShow";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useDispatch } from "react-redux";
import { actions } from "../redux/reducers";
import Validations from "../utils/Validations";

const Login1 = (props) => {
  const disptach=useDispatch()
  const [padding, setPadding] = useState(0);
  const initialSate = {
    phoneNumber: "",
    password: "",
  };
  const [showpass,setShowpass]=useState(false)

  const [loginDetail, setLoginDetails] = useState(initialSate);
  const [err, setErr] = useState({
    phoneErr: "",
    passwordErr: "",
  });

  const checkValidation = () => {
    if (loginDetail.phoneNumber.trim().length == 0) {
      setErr({ ...err, phoneErr: "Enter phone number" });
    }
    if (loginDetail.password.trim().length == 0) {
      setErr({ ...err, passwordErr: "Enter password" });
    }
  };

  //login with mobile number
  const checkLoginHandler = async () => {
    console.log("sdkjashdkasjdhk", loginDetail.phoneNumber);
    // Validations.isEmpty(loginDetail.phoneNumber)
    if(loginDetail.phoneNumber==""){
      toastShow("Please enter phone number", colorConstant.errorRed)
      return
    }
    if(loginDetail.phoneNumber.length<8){
      toastShow("Please enter valid phone number", colorConstant.errorRed)
      return
    }
    if(loginDetail.password==""){
      toastShow("Please enter password", colorConstant.errorRed)
      return
    }

    const objToSend = {
      phoneNumber: loginDetail.phoneNumber,
      password: loginDetail.password,
    };
    try {
      const response = await LoginWithMobileServices(objToSend);
      console.log(response.data.jwtToken);
      await AsyncStorage.setItem("token", response?.data?.activeUser?.jwtToken);
      console.log(response?.data?.activeUser?.is_aboutInfo);
      if(response?.data?.activeUser?.is_aboutInfo){
        disptach(actions.setIntroStatus("main"));
      }
      else{
        props.navigation.navigate("AboutDriver", { userDatils: response.data });
      }
    } catch (error) {
      console.log(error.request.status,'dddd');
      if(error.request.status=="401"){
        toastShow("User does not exits", colorConstant.errorRed);
      }
      else if(error.request.status=="409"){
        toastShow("Please wait your account has not approve by admin", colorConstant.errorRed);
      }
      else{
        toastShow("Invalid credentials", colorConstant.errorRed);
      }
    }
  };

  // google login code

  const googleInit = () => {
    GoogleSignin.configure({
      scopes: [], // what API you want to access on behalf of the user, default is email and profile
      webClientId: WEB_CLIENT_ID,
      offlineAccess: true,
      // hostedDomain: '', // specifies a hosted domain restriction
      // forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
      // accountName: '', // [Android] specifies an account name on the device that should be used
      // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
      // googleServicePlistPath: '', // [iOS] if you renamed your GoogleService-Info file, new name here, e.g. GoogleService-Info-Staging
      // openIdRealm: '', // [iOS] The OpenID2 realm of the home web server. This allows Google to include the user's OpenID Identifier in the OpenID Connect ID token.
      // profileImageSize: 120, // [iOS] The desired height (and width) of the profile image. Defaults to 120px
    });
  };

  const SignUpViaGoogle = async () => {
    // await GoogleSignin.revokeAccess();
    // await GoogleSignin.signOut();
    googleInit();
    // props.navigation.navigate("Register", {
    //     data: "Google"
    // });
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log("====================================");
      console.log(userInfo);
      console.log("====================================");
      loginWithGoogle(userInfo);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
        console.log("User profile", error);
      }
    }
  };

  // region end

  const loginWithGoogle = async (userInfo) => {
    const objToSend = {
      signupType: "GOOGLE",
      socialId: userInfo.user.id,
    };
    try {
      const response = await LoginWithGoogleServices(objToSend);
      
      console.log(response.data.jwtToken);
      await AsyncStorage.setItem("token", response.data.activeUser.jwtToken)
      console.log(response.data.activeUser.is_aboutInfo,'kdsljflksdjfklsdjflkjfdl')
      
      if(response.data.activeUser.is_aboutInfo){
        disptach(actions.setIntroStatus("main"));
      }
      else{
        props.navigation.navigate("AboutDriver", { userDatils: response.data });
      }
      await GoogleSignin.signOut();

    } catch (error) {
      await GoogleSignin.signOut();
      toastShow("User are not exits please sign up", colorConstant.errorRed);
      console.log(error);
      console.log(objToSend, "hdskjdshfk");

    }
  };

  useEffect(() => {
    const showSubscription = Keyboard.addListener("keyboardDidShow", (e) => {
      if (Platform.OS === "ios") {
        console.log("e.endCoordinates.height", e.endCoordinates.height);
        setPadding(e.endCoordinates.height);
      }
    });
    const hideSubscription = Keyboard.addListener("keyboardDidHide", () => {
      if (Platform.OS === "ios") {
        setPadding(10);
      }
    });

    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);

  return (
    <SafeAreaView style={styles.main}>
      <Text style={styles.text1}>Your Account Is Ready!</Text>
      <ScrollView
        showsVerticalScrollIndicator={false}
        bounces={false}
        contentContainerStyle={{
          ...Platform.select({
            ios: {
              paddingVertical: 25,
              paddingBottom: padding,
            },
            android: {
              paddingVertical: 25,
            },
          }),
        }}
      >
        <Image
          resizeMode="contain"
          style={{
            width: Width * 0.8,
            height: null,
            aspectRatio: 1 / 0.7,
            alignSelf: "center",
          }}
          source={imageConstant.wish}
        />
        <Text style={styles.text2}>Log In To Sart The Adventure</Text>
        <CustomInput
          marginTop={15}
          placeholder={"Phone Number"}
          keyboardType={'numeric'}
          onChangeText={(val) => {
            setLoginDetails({ ...loginDetail, phoneNumber: val });
          }}
          value={loginDetail.phoneNumber}
        />
        {/* <TextInput keyboardType="numeric" */}
        <CustomPasswordInput
          marginTop={15}
          placeholder={"Password"}
          onChangeText={(val) => {
            setLoginDetails({ ...loginDetail, password: val });
          }}
          value={loginDetail.password}
          secureTextEntry={showpass}
          onPress={()=>setShowpass(!showpass)}
        />
        <TouchableOpacity
          onPress={() =>
            props.navigation.navigate("Forget", {
              flow: "login",
            })
          }
          style={{
            marginTop: 20,
            alignSelf: "flex-start",
            marginLeft: Width * 0.1,
          }}
        >
          <Text
            style={{
              fontSize: 12,
              fontFamily: fontConstant.regular,
              color: colorConstant.purple,
              textDecorationLine: "underline",
            }}
          >
            I forgot my password
          </Text>
        </TouchableOpacity>

        <CustomButton
          OnButtonPress={() => checkLoginHandler()}
          marginTop={Platform.OS == "ios" ? 15 : 25}
          width={"80%"}
          buttonText={"CONFIRM"}
        />
        <TouchableOpacity
          activeOpacity={0.9}
          style={[
            styles.faceStyle,
            {
              ...Platform.select({
                ios: {
                  marginTop: 15,
                },
                android: {
                  //   marginTop: 20, marginBottom: 45
                },
              }),
            },
          ]}
        >
          <Image
            source={imageConstant.faceid}
            style={{
              width: 25,
              height: 25,
            }}
          />
          <Text style={styles.text3}> Continue with Face ID</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => SignUpViaGoogle()}
          activeOpacity={0.9}
          style={[
            styles.googleStyle,
            {
              ...Platform.select({
                ios: {
                  marginTop: 10,
                },
                android: {
                  //   marginTop: 20, marginBottom: 45
                },
              }),
            },
          ]}
        >
          <Image
            source={imageConstant.google}
            style={{
              width: 25,
              height: 25,
            }}
          />
          <Text style={styles.text4}> Continue with Google</Text>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.9}
          style={[
            { ...styles.facebookStyle },
            {
              ...Platform.select({
                ios: {
                  marginTop: 10,
                },
                android: {
                  marginTop: 20,
                  marginBottom: 45,
                },
              }),
            },
          ]}
        >
          <Image
            source={imageConstant.facebook}
            style={{
              width: 25,
              height: 25,
            }}
          />
          <Text style={styles.text4}> Continue with Facebook</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Login1;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
    //  backgroundColor:"red"
  },
  text1: {
    fontSize: 28,
    fontFamily: fontConstant.bold,
    color: colorConstant.purple,
    alignSelf: "center",
    marginTop: Platform.OS == "ios" ? 40 : 20,
  },
  text2: {
    fontSize: 20,
    fontFamily: fontConstant.bold,
    color: colorConstant.purple,
    alignSelf: "center",
    marginTop: 20,
  },
  text3: {
    fontSize: 14,
    fontFamily: fontConstant.semi,
    color: colorConstant.black,
  },
  text4: {
    fontSize: 14,
    fontFamily: fontConstant.semi,
    color: colorConstant.white,
  },
  faceStyle: {
    flexDirection: "row",
    width: "80%",
    borderWidth: 2,
    borderColor: colorConstant.purple,
    justifyContent: "center",
    height: 45,
    borderRadius: 10,
    alignItems: "center",
    alignSelf: "center",
    marginTop: 30,
  },
  facebookStyle: {
    flexDirection: "row",
    width: "80%",
    backgroundColor: colorConstant.black,
    justifyContent: "center",
    height: 45,
    borderRadius: 10,
    alignItems: "center",
    alignSelf: "center",
  },

  googleStyle: {
    flexDirection: "row",
    width: "80%",
    backgroundColor: colorConstant.black,
    justifyContent: "center",
    height: 45,
    borderRadius: 10,
    alignItems: "center",
    alignSelf: "center",
    marginTop: 20,
  },
});
