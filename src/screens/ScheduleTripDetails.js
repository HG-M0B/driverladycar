import { View, Text, StyleSheet, Image, ScrollView ,TouchableOpacity, SafeAreaView} from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { Height, Width } from '../dimension/dimension'
import { useEffect } from 'react'
import CustomHeader from '../customComponents/CustomHeader'
import { Rating, AirbnbRating } from 'react-native-ratings';
import CustomButton from '../customComponents/CustomButton'
import { useState } from 'react'

const ScheduleTripDetails = (props) => {

    const [status,setStatus] = useState(false)
    useEffect(()=>{
        if(props.route?.params)
        {
            setStatus(props.route?.params?.data)
        }
    },[])
 
  return (
    <SafeAreaView style={styles.main}>
      <CustomHeader
      headerText={'Trip Details'}
      color={colorConstant.purple}
      navigation={props.navigation}
      />

      <Image
      source={imageConstant.map}
      resizeMode='contain'
      style={styles.map}
      />

          <ScrollView>
              <View style={{
                  marginTop:25,
                  flexDirection: "row",
                  width: "90%",
                  alignSelf: "center",
              }}>
                  <Image

                      source={imageConstant.profile4}
                      style={{
                          width: 40,
                          height: 40
                      }} />
                  <View style={{
                      marginLeft: 10
                  }}>
                      <View style={{
                          flexDirection: 'row',
                      }}>
                          <Text style={{
                              fontSize: 15,
                              color: colorConstant.black,
                              fontFamily: fontConstant.semi
                          }}>SONIA </Text>

                          <View style={{
                              flexDirection: "row",
                              alignItems: "center"
                          }}>
                              <View
                                 
                                  style={{
                                      width: 10,
                                      height: 10,
                                     borderRadius:5,
                                      backgroundColor:"#FF7F09"
                                  }} />
                              <Text style={{
                                  fontSize: 10,
                                  fontFamily: fontConstant.regular,
                                  color:"#FF7F09"
                              }}> Upcoming</Text>
                          </View>

                      </View>

                      <View style={{
                          flexDirection: "row"
                      }}>
                          <Image
                              resizeMode='contain'
                              style={{
                                  width: 16,
                                  height: 16
                              }}
                              source={imageConstant.star} />
                          <Text style={{
                              fontSize: 15,
                              fontFamily: fontConstant.regular,
                              color: colorConstant.black

                          }}>4.9</Text>
                      </View>
                  </View>


                  <View style={{
                      position: "absolute",
                      right: 0
                  }}>
                      <View style={{
                          flexDirection: "row"
                      }}>
                          <Text style={{
                              fontSize: 12,
                              fontFamily: fontConstant.regular,
                              color: colorConstant.black
                          }}>Fare:</Text>
                          <Text style={{
                              fontSize: 14,
                              fontFamily: fontConstant.bold,
                              color: colorConstant.purple
                          }}>$13</Text>
                      </View>
                     {
                         status && (
                            <View style={{
                                flexDirection: "row",
                                alignSelf: "flex-end"
                            }}>
                                <Text style={{
                                    fontSize: 12,
                                    fontFamily: fontConstant.regular,
                                    color: colorConstant.black
                                }}>Tip:</Text>
                                <Text style={{
                                    fontSize: 14,
                                    fontFamily: fontConstant.bold,
                                    color: colorConstant.purple
                                }}>$2</Text>
                                </View>
                         )
                     }
                      
                  </View>
              </View>


              <View style={{
                  marginTop: 20,
                  width: "90%",
                  alignSelf: "center"
              }}>
                  <Text style={{
                      fontSize: 12,
                      fontFamily: fontConstant.regular,
                      color: colorConstant.black
                  }}>Pickup Time:
                  </Text>
                  <Text style={{
                      fontSize: 12,
                      fontFamily: fontConstant.bold,
                      color: colorConstant.black,
                      marginTop:10
                  }}>Sep 19  12:25 PM .
                      <Text style={{
                          fontSize: 12,
                          fontFamily: fontConstant.regular,
                          color: colorConstant.black
                      }}>
                          15 Mins
                      </Text></Text>

<View style={{
    flexDirection:"row",
    position:"absolute",
    right:5
}}>
    <Image
    resizeMode='contain'
    style={{
        width:31,
        height:31,
        marginRight:10
    }}
    source={imageConstant.phone}
    />
        <Image
    resizeMode='contain'
    style={{
        width:31,
        height:31
    }}
    source={imageConstant.speech}
    />
</View>

              </View>

              <View style={{
                  width: "90%",
                  borderWidth: 0.3,
                  alignSelf: "center",
                  color: colorConstant.black,
                  opacity: 0.2,
                  marginTop: 25

              }}>
              </View>



              <View style={{
                  width: "90%",
                  alignSelf: "center",
                  flexDirection: "row",
                  marginTop: 20
              }}>

                  <View style={{
                      // backgroundColor:"red",
                      justifyContent: "center",
                      alignItems: "center"
                  }}>
                      <Image
                          resizeMode='contain'
                          style={{
                              width: 15,
                              height: 21

                          }}
                          source={imageConstant.locationred} />
                      <Image
                          resizeMode='contain'
                          style={{
                              width: 10,
                              height: 38

                          }}
                          source={imageConstant.line} />
                      <Image
                          resizeMode='contain'
                          style={{
                              marginVertical: 5,
                              width: 15,
                              height: 21

                          }}
                          source={imageConstant.locationgreen} />
                  </View>

                  <View style={{
                      marginLeft: 10,
                      justifyContent: "space-between",
                      height: 100,
                  }}>
                      <View>
                          <Text
                              numberOfLines={1}
                              style={{
                                  fontSize: 14,
                                  fontFamily: fontConstant.bold,
                                  color: colorConstant.black,
                                  width: "90%",
                              }}>Nashville, TN 37214, USA</Text>
                          <Text style={{
                              fontSize: 12,
                              fontFamily: fontConstant.regular,
                              color: colorConstant.black
                          }}>Sep 19  12:25 PM</Text>
                      </View>


                      <View>
                          <Text
                              numberOfLines={1}
                              style={{
                                  fontSize: 14,
                                  fontFamily: fontConstant.bold,
                                  color: colorConstant.black,
                                  width: "90%",
                              }}>2720 Old Lebanon Rd, Nashiville, TN, 37214, US</Text>
                          <Text style={{
                              fontSize: 12,
                              fontFamily: fontConstant.regular,
                              color: colorConstant.black
                          }}>Sep 19  12:25 PM</Text>
                      </View>
                  </View>

              </View>


         
          </ScrollView>

          <TouchableOpacity

              activeOpacity={1}
              style={{
                  flexDirection: "row",
                  alignSelf: "center",
                  bottom: 30,
                  alignItems: "center"
              }}>
              <Text style={{
                  fontSize: 14,
                  fontFamily: fontConstant.regular,
                  color: colorConstant.purple
              }}>Cancel  </Text>
              <Image
                  resizeMode='contain'
                  style={{
                      width: 24,
                      height: 24
                  }}
                  source={imageConstant.info}
              />
          </TouchableOpacity>
    </SafeAreaView>
  )
}

export default ScheduleTripDetails
const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },

    heading:{
        fontSize:36,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        width:"90%",
        alignSelf:"center",
        marginTop:Height*0.10
    },
    map:{
        width:Width,
        height:null,
        aspectRatio:1/0.45,
        marginTop:10,
    },
    location:{
        width:20,
        height:20
    },
    profile:{
        width: 70,
        height: 70
    },
    row:{
        flexDirection:"row",
        width:"90%",
        alignSelf:"center",
        justifyContent:"space-between"
    },
    row1:{
        flexDirection:"row",
        alignItems:"center"
    },
    row2:{
        flexDirection:"row",
        alignSelf:"center",
        alignItems:"center",
        width:"90%",
        marginVertical:10,
    },
    text:{
        fontSize:12,
        fontFamily:fontConstant.regular,
        color:colorConstant.black
    },
    text1:{
        fontSize:12,
        fontFamily:fontConstant.bold,
        color:colorConstant.black
    },
    profile:{
        width:60,
        height:60
    },
    text2:{
        fontSize:12,
        fontFamily:fontConstant.bold,
        color:colorConstant.black,
        width:"90%",
        alignSelf:"center",
        marginTop:30
    }
})