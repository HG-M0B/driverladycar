import {
  Image,
  Keyboard,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import React, { useState, useEffect, useRef } from "react";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import { Width } from "../dimension/dimension";
import CustomButton from "../customComponents/CustomButton";
import PhoneInput from "react-native-phone-number-input";
import {
  GoogleSignin,
  statusCodes,
} from "@react-native-google-signin/google-signin";
import { WEB_CLIENT_ID } from "../utils/HelperConstant";
import axios from "../utils/api";
import { useDispatch } from "react-redux";
import { actions } from "../redux/reducers";
import { storeData } from "../utils/HelperFunction";
import Validations from "../utils/Validations";
import toastShow from "../utils/toastShow";
import { useIsFocused } from "@react-navigation/native";

let initialState = {
  number: "",
  countryCode: "91",
  stateFlag: "IN",
  valid: false,
  showMessage: false,
};
const SignUp = (props) => {
  const [iState, updateState] = useState(initialState);
  const phoneInput = useRef(null);
  const { navigation } = props;
  const dispatch = useDispatch();
  const { number, valid, showMessage, countryCode, stateFlag } = iState;
  const isFocoused=useIsFocused()

  useEffect(() => {
    getStateList();
  }, []);


  

  // ********Function's****************

  const inputHandler = (text) => {
    updateState({ ...iState, number: text });
  };

  const onConfirm = () => {
    if(number.length<8){
      toastShow("Please enter valid number", colorConstant.errorRed)
      return
    }
    Validations.isEmpty(number)
      ? toastShow("Please enter phone number", colorConstant.errorRed)
      : // : Validations.isFormattedNumber(number)
        // ? toastShow("Phone number is not valid",colorConstant.errorRed)
        checkEmailPhone();
  };
  const navigateTo = () => {
    navigation.navigate("OtpVerify", {

    // navigation.navigate("Register", {
      flow: "Number",
      number: number,
      cCode: countryCode,
      stateFlag: stateFlag,
    });
  };
  const googleInit = () => {
    GoogleSignin.configure({
      scopes: [], // what API you want to access on behalf of the user, default is email and profile
      webClientId: WEB_CLIENT_ID,
      offlineAccess: true,
      // hostedDomain: '', // specifies a hosted domain restriction
      // forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
      // accountName: '', // [Android] specifies an account name on the device that should be used
      // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
      // googleServicePlistPath: '', // [iOS] if you renamed your GoogleService-Info file, new name here, e.g. GoogleService-Info-Staging
      // openIdRealm: '', // [iOS] The OpenID2 realm of the home web server. This allows Google to include the user's OpenID Identifier in the OpenID Connect ID token.
      // profileImageSize: 120, // [iOS] The desired height (and width) of the profile image. Defaults to 120px
    });
  };

  const SignUpViaGoogle = async () => {
    googleInit();
    // props.navigation.navigate("Register", {
    //     data: "Google"
    // });
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      checkEmailPhone2(userInfo);

      // await storeData("SocialInfo", userInfo);
      // console.log('====================================');
      // console.log(userInfo,'user Details is here by dkkk');
      // console.log('====================================',userInfo.user,'jkdshfkjdshfksdjhfksdjhfkj');
      // props.navigation.navigate("Register", {
      //     data:userInfo.user,
      //     screenName:'Register',
      //     flow:'Google'
      // });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
        console.log("User profile", error);
      }
    }
  };

  // *********Api's call **************

  const getStateList = async () => {
    try {
      let response = await axios.get("static/state-list");
      if (response && response.status == 200) {
        dispatch(actions.setStateList(response?.data?.stateList));
      } else {
        toastShow(response?.data?.message, colorConstant.errorRed);
      }
    } catch (error) {
      toastShow(error, colorConstant.errorRed);
    }
  };

  //dummy function check if email already exiting

  const signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
    //   setloggedIn(false);
    //   setuserInfo([]);
    } catch (error) {
      console.error(error);
    }
  };

  const checkEmailPhone2 = async (userInfo) => {
    try {
      let reqData = {
        email: userInfo.user.email,
        phoneNumber: "",
      };
      let response = await axios.post(`auth/check-driver`, reqData);
      if (response && response.status == 200) {
        toastShow(
          "Account is already exits with this email",
          colorConstant.errorRed
        );
        signOut()
      } else {
        // navigateTo()
        props.navigation.navigate("Register", {
          data: userInfo.user,
          screenName: "Register",
          flow: "Google",
        });
        signOut()
      }
    } catch (error) {
      console.log("error catch", error);
      toastShow(error, colorConstant.errorRed);
      signOut()

    }
  };
  // region end

  const checkEmailPhone = async () => {
    try {
      let reqData = {
        email: "",
        phoneNumber: number,
      };
      // console.log(reqData, "------ksjdhfkjfh");
      let response = await axios.post(`auth/check-driver`, reqData);
      console.log("====================================");
      console.log(response.data, "------------");
      console.log("====================================");
      if (response && response.status == 200) {
        toastShow(
          "Account is already exits with this number",
          colorConstant.errorRed
        );
      } else {
        // alert('jljslkdj')
        navigateTo();
        // console.log("else", response)
        // toastShow(response.data.message, colorConstant.errorRed)
      }
    } catch (error) {
      console.log("error catch", error);
      toastShow(error, colorConstant.errorRed);
    }
  };

  return (
    <View>
      <Text style={styles.text1}>Let's start the adventure!</Text>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingBottom: 100,
        }}
      >
        <Image
          resizeMode="contain"
          style={{
            width: Width * 0.8,
            height: null,
            aspectRatio: 1 / 0.7,
            alignSelf: "center",
          }}
          source={imageConstant.carr}
        />
        <Text style={styles.text2}>Log In To Sart The Adventure</Text>
        <PhoneInput
          ref={phoneInput}
          defaultValue={number}
          defaultCode="IN"
          layout="first"
          onChangeCountry={(code) => {
            updateState({
              ...iState,
              countryCode: code?.callingCode?.[0],
              stateFlag: code?.cca2,
            });
          }}
          onChangeText={inputHandler}
          containerStyle={{
            width: "80%",
            borderWidth: 1.5,
            borderRadius: 10,
            marginTop: 20,
            alignSelf: "center",
            borderColor: colorConstant.purple,
          }}
          textInputProps={{
            maxLength: 13,
            value: number,
          }}
          textContainerStyle={{
            paddingVertical: 0,
            borderRadius: 10,
          }}
        />

        <CustomButton
          OnButtonPress={onConfirm}
          marginTop={20}
          width={"80%"}
          buttonText={"CONFIRM"}
        />

        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            alignSelf: "center",
            marginVertical: 20,
          }}
        >
          <View
            style={{
              height: 1,
              width: "35%",
              borderRadius: 1,
              borderWidth: 1,
              borderColor: colorConstant.purple,
              borderStyle: "dashed",
            }}
          ></View>
          <Text
            style={{
              fontSize: 16,
              fontFamily: fontConstant.regular,
              color: colorConstant.purple,
            }}
          >
            {" "}
            OR{" "}
          </Text>
          <View
            style={{
              height: 1,
              width: "35%",
              borderRadius: 1,
              borderWidth: 1,
              borderColor: colorConstant.purple,
              borderStyle: "dashed",
            }}
          ></View>
        </View>

        <TouchableOpacity
          onPress={SignUpViaGoogle}
          activeOpacity={0.9}
          style={styles.googleStyle}
        >
          <Image
            source={imageConstant.google}
            style={{
              width: 25,
              height: 25,
            }}
          />
          <Text style={styles.text4}> Continue with Google</Text>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.9} style={styles.facebookStyle}>
          <Image
            source={imageConstant.facebook}
            style={{
              width: 25,
              height: 25,
            }}
          />
          <Text style={styles.text4}> Continue with Facebook</Text>
        </TouchableOpacity>

        {Platform.OS == "ios" && (
          <TouchableOpacity
            activeOpacity={0.9}
            style={[{ ...styles.facebookStyle }, { marginBottom: 30 }]}
          >
            <Image
              source={imageConstant.apple}
              style={{
                width: 25,
                height: 25,
              }}
            />
            <Text style={styles.text4}> Continue with Apple</Text>
          </TouchableOpacity>
        )}
      </ScrollView>
    </View>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
  },
  text1: {
    fontSize: 28,
    fontFamily: fontConstant.bold,
    color: colorConstant.purple,
    alignSelf: "center",
    marginTop: 50,
  },
  text2: {
    fontSize: 20,
    fontFamily: fontConstant.bold,
    color: colorConstant.purple,
    alignSelf: "center",
    marginTop: 20,
  },
  text3: {
    fontSize: 14,
    fontFamily: fontConstant.semi,
    color: colorConstant.black,
  },
  text4: {
    fontSize: 14,
    fontFamily: fontConstant.semi,
    color: colorConstant.white,
  },
  faceStyle: {
    flexDirection: "row",
    width: "80%",
    borderWidth: 2,
    borderColor: colorConstant.purple,
    justifyContent: "center",
    height: 45,
    borderRadius: 10,
    alignItems: "center",
    alignSelf: "center",
    marginTop: 30,
  },
  facebookStyle: {
    flexDirection: "row",
    width: "80%",
    backgroundColor: colorConstant.black,
    justifyContent: "center",
    height: 45,
    borderRadius: 10,
    alignItems: "center",
    alignSelf: "center",
    marginTop: 20,
  },

  googleStyle: {
    flexDirection: "row",
    width: "80%",
    backgroundColor: colorConstant.black,
    justifyContent: "center",
    height: 45,
    borderRadius: 10,
    alignItems: "center",
    alignSelf: "center",
    marginTop: 20,
  },
});
