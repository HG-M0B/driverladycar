import {
  Image,
  Modal,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import React, { useState, useEffect } from "react";
import { moderateScale, scale } from "react-native-size-matters";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import CustomButton from "../customComponents/CustomButton";
import { uploadImageServices } from "../services/uploadImageServices";
import { GetImageUrlServices } from "../services/CommonServices";
import toastShow from "../utils/toastShow";
import LoaderModal from "../customComponents/LoaderModal";
import { useNavigation } from "@react-navigation/native";

const UploadDriverDocument = ({ route }) => {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);
  const [registration, setRegistration] = useState(" ");
  const [insurence, setInsurense] = useState(" ");
  const paramData = route?.params;

  console.log(paramData, "ksjdslkjslkfsl");

  const uploadImageFunction = async () => {
    await GetImageUrlServices(getImageData);
  };

  // console.log(insurence)
  // #region for upload image in server
  const getImageData = async (imageName) => {
    setIsLoading(true);
    let finalFormData = new FormData();
    finalFormData.append("fileName", imageName);
    setIsLoading(true);
    try {
      const response = await uploadImageServices(finalFormData);
      console.log(response.data);
      setIsLoading(false);
      setRegistration(response.data.data);

      toastShow("Image upload successful", colorConstant.green);
    } catch (error) {
      console.log(error, "this is error message");
      setIsLoading(false);
      toastShow("Image upload successful", colorConstant.errorRed);
    }
  };

  const uploadImageFunction2 = async () => {
    await GetImageUrlServices(getImageData2);
  };

  // console.log(insurence)
  // #region for upload image in server
  const getImageData2 = async (imageName) => {
    setIsLoading(true);
    let finalFormData = new FormData();
    finalFormData.append("fileName", imageName);
    setIsLoading(true);
    try {
      const response = await uploadImageServices(finalFormData);
      console.log(response.data);
      setIsLoading(false);
      setInsurense(response.data.data);
      toastShow("Image upload successful", colorConstant.green);
    } catch (error) {
      console.log(error, "this is error message");
      setIsLoading(false);
      toastShow("Image error", colorConstant.errorRed);
    }
  };

  useEffect(() => {
    let mount = true;
    if (mount) {
      if (registration !== " " && insurence !== " " && paramData.data == "Add") {
        navigation.navigate("AdditionalInfo2", {
          flow: "Add",
          vehicleRegistration: registration,
          vehicleInsurence: insurence,
        });
      }
    }
    return () => {
      mount = false;
    };
  }, [insurence, registration]);

  useEffect(() => {
    let mount=true
    if(mount && paramData.data=='edit'){
        console.log(paramData?.vechicleDetail?.vehicleDocument[0].vehicleRegistration)
        setRegistration(paramData?.vechicleDetail?.vehicleDocument[0].vehicleRegistration)
        setInsurense(paramData?.vechicleDetail?.vehicleDocument[0].vehicleInsurence)
    }
    return () => {
      mount=false
    }
  }, [])
  

  return (
    <>
      <View style={styles.container}>
        <View>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={styles.backButton}
          >
            <Image
              source={require("../assets/images/purpleArrow.png")}
              style={styles.img}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>

        <View style={styles.headingText}>
          <Text style={styles.infotext}>Take The picture of</Text>
          <Text style={styles.infotext}>
            Vehicle Documents{" "}
            <Image
              source={imageConstant.info}
              resizeMode="contain"
              style={styles.infoImg}
            />
          </Text>
        </View>

        <View style={[styles.alc, styles.avtarImg]}>
          <Image
            resizeMode="contain"
            source={require("../assets/images/license.png")}
            style={styles.img2}
          />
        </View>

        <View style={{ marginTop: moderateScale(24), alignItems: "center" }}>
          <TouchableOpacity onPress={uploadImageFunction} style={styles.btn}>
            <Text style={styles.btnText}>Vehicle Registration</Text>
            {registration !== " "&&<Image source={imageConstant.right} style={styles.done}/>}
          </TouchableOpacity>

          <TouchableOpacity
            disabled={registration == " " ? true : false}
            onPress={uploadImageFunction2}
            style={[
              styles.btn,
              {
                marginTop: moderateScale(20),
                backgroundColor:
                  registration == " "
                    ? colorConstant.grayBg
                    : colorConstant.purple,
              },
            ]}
          >
            <Text style={styles.btnText}>Vehicle Insurance</Text>
            {insurence !== " "&&<Image source={imageConstant.right} style={styles.done}/>}

          </TouchableOpacity>
        </View>
        {paramData?.data == "edit" && (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("AdditionalInfo2", {
                flow: "edit",
                vehicleData: paramData,
                registrationImg:registration,
                insurenceImg:insurence
              })
            }
            style={[
              styles.btn,
              {
                position: "absolute",
                bottom: moderateScale(40),
                alignSelf: "center",
              },
            ]}
          >
            <Text style={styles.btnText}>Continue</Text>
          </TouchableOpacity>
        )}
      </View>
      <LoaderModal isLoading={isLoading} />
      {console.log({ a: insurence, b: registration })}
    </>
  );
};

export default UploadDriverDocument;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    paddingHorizontal: moderateScale(20),
  },
  img: {
    height: moderateScale(35),
    width: moderateScale(35),
  },
  infotext: {
    fontSize: 29,
    color: colorConstant.purple,
    fontFamily: fontConstant.semi,
  },
  alc: {
    alignItems: "center",
  },
  avtarImg: {
    marginTop: moderateScale(35),
  },
  img2: {
    height:moderateScale(230),
  },
  backButton: { width: scale(50), marginTop: moderateScale(20) },
  headingText: { alignItems: "center", marginTop: moderateScale(20) },
  infoImg: { height: 30, width: 30 },
  btn: {
    backgroundColor: colorConstant.purple,
    width: "85%",
    paddingVertical: moderateScale(13),
    borderRadius: moderateScale(10),
    alignItems: "center",
    justifyContent: "center",
    flexDirection:'row'
  },
  btnText: {
    color: colorConstant.white,
    fontFamily: fontConstant.semi,
    fontSize: moderateScale(15),
  },
  done:{height:moderateScale(20),width:moderateScale(20),marginLeft:scale(10)}
});
