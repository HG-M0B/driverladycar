import { StyleSheet, Text, View,FlatList, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant ,imageConstant} from '../utils/constant'

const ScheduleTrip = (props) => {


   const renderCard=({item,index})=>{
       return(
        <TouchableOpacity
        activeOpacity={0.8}
        onPress={()=>props.navigation.navigate("ScheduleTripDetails")}
        style={{
            flexDirection: "row",
         //    backgroundColor:"pink",
            width: "90%",
            alignSelf: "center", alignItems: "center",
            paddingBottom: 20,
            borderBottomWidth: 1,
            borderBottom: 0.5,
            borderBottomColor:colorConstant.grayBg,
            marginVertical: 10
        }} key={index}>
           <Image 
           resizeMode='contain'
           style={{
               width:40,
               height:40
           }}
           source={imageConstant.profile4}/>
           <View style={{
               marginLeft:10
           }}>
           <Text style={{
               fontSize:14,
               fontFamily:fontConstant.semi,
               color:colorConstant.black
           }}>Trip with Caroline
           </Text>
           <Text style={{
               fontSize:12,
               fontFamily:fontConstant.regular,
               color:colorConstant.black
           }}>Sep 19  12:25 PM  <View style={{
            width:5,
            height:5,
            borderRadius:5/2,
            backgroundColor:colorConstant.grayBg
        }}></View> 15 Mins</Text>
              
           

           </View>
          
       
           <Image 
           resizeMode='contain'
           style={{
               width:10,
               height:12,
               position:"absolute",
               top:15,
               right:0
              
           }}
           source={imageConstant.angle}/>
          

        </TouchableOpacity>
       )
   } 
  return (
    <View style={styles.main}>

          <FlatList
              showsVerticalScrollIndicator={false}
              bounces={false}
              data={[{ id: 1 }, { id: 2 }, { id: 3 }]}
              renderItem={renderCard}
              keyExtractor={(item) => item.id}
              contentContainerStyle={styles.contain}
          />
       
    </View>
  )
}

export default ScheduleTrip

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    contain:{
        ...Platform.select({
            ios:{
                paddingVertical:10,
            },
            android:{
                paddingVertical:40,
            }
        })
    },
    card:{
        width:"90%",
        alignSelf:"center",
        backgroundColor:colorConstant.grayBg,
        borderTopRightRadius:25,
        borderBottomLeftRadius:25,
        marginTop:15,
        padding:15,
        shadowColor:colorConstant.purple,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,

    },
    row:{
        flexDirection:"row",
        marginVertical:5,
        justifyContent:"space-between"
    },
    text1:{
        fontSize:16,
        fontFamily:fontConstant.semi,
        color:colorConstant.purple
    },
    text2:{
        fontSize:12,
        fontFamily:fontConstant.semi,
        color:colorConstant.black
    },
    text3:{
        fontSize:12,
        fontFamily:fontConstant.semi,
        color:colorConstant.black
    },
    text4:{
        fontSize:12,
        fontFamily:fontConstant.semi,
        color:colorConstant.black
    }
})