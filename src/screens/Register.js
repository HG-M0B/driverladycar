import {
  Image,
  Platform,
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
  Keyboard,
} from "react-native";
import React, { useRef, useState, useEffect } from "react";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import CustomHeader from "../customComponents/CustomHeader";
import CustomInput from "../customComponents/CustomInput";
import CustomButton from "../customComponents/CustomButton";
import { KeyboardAwareScrollView } from "@codler/react-native-keyboard-aware-scroll-view";
import CustomPasswordInput from "../customComponents/CustomPasswordInput";
import CustomDropDown from "../customComponents/CustomDropDown";
import PhoneInput from "react-native-phone-number-input";
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import {
  checkPasswordFormat,
  getData,
  storeData,
} from "../utils/HelperFunction";
import axios from "../utils/api";
import toastShow from "../utils/toastShow";
import { useSelector } from "react-redux";
import Validations from "../utils/Validations";
let initialState = {
  name: "",
  email: "",
  flow: "",
  number: "",
  password: "",
  confirm: "",
  birthday: "",
  address: "",
  countryCode: "91",
  selectedState: "",
  selectedStateID: "",
  stateFlag: "IN",
  cityList: null,
  selectedCity: "",
  selectedCityID: "",
  formattedNumber: "",
  passwordMessageShownFlag: true,
  confirmMessageShownFlag: true,
  valid: false,
  showMessage: false,
  date: new Date(),
  show: false,
  mode: "",
  tcFlag: false,
  socialId: "",
  signupType: "NORMAL",
};

const Register = (props) => {
  const [iState, updateState] = useState(initialState);
  const phoneInput = useRef(null);
  const { navigation, route } = props;
  const {
    name,
    email,
    flow,
    number,
    password,
    address,
    confirm,
    birthday,
    valid,
    countryCode,
    showMessage,
    formattedNumber,
    date,
    show,
    mode,
    tcFlag,
    passwordMessageShownFlag,
    confirmMessageShownFlag,
    selectedState,
    selectedStateID,
    cityList,
    selectedCity,
    stateFlag,
    selectedCityID,
    socialId,
    signupType,
  } = iState;

  const [stateList, setStateList] = useState([]);

  const getStateList = async () => {
    try {
      let response = await axios.get("static/state-list");
      if (response && response.status == 200) {
        setStateList(response.data.stateList);
        // dispatch(actions.setStateList(response?.data?.stateList));
      } else {
        toastShow(response?.data?.message, colorConstant.errorRed);
      }
    } catch (error) {
      toastShow(error, colorConstant.errorRed);
    }
  };

  useEffect(() => {
    getStateList();
  }, []);

  // const { stateList } = useSelector((state) => state.reducers);

  console.log("====================================");
  console.log(stateList, "this is state list here so whate we are doing");
  console.log("====================================");

  useEffect(() => {
    updateState({ ...iState, flow: route?.params?.flow });
    // getUserSocialInfo();
    if (route?.params?.flow == "Number") {
      getNumberFlowData();
      // alert('dksjfhsdkjfh')
    }
  }, [route?.params]);

  useEffect(() => {
    if (selectedStateID !== "") {
      getCityList();
    }
  }, [selectedStateID]);

  // const getUserSocialInfo = async () => {
  //   let info = await getData("SocialInfo");
  //   updateState({
  //     ...iState,
  //     name: info?.user?.givenName + " " + info?.user?.familyName,
  //     email: info?.user?.email,
  //   });
  // };

  const getNumberFlowData = async () => {
    let info = await getData("udid");
    let data = JSON.parse(info);

    updateState({
      ...iState,
      countryCode: data?.cCode,
      formattedNumber: data?.phoneNumber,
      stateFlag: data?.sFlag,
      signupType: "NORMAL",
    });
  };
  const onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate;
    updateState({
      ...iState,
      birthday: moment(currentDate).format("YYYY/MM/DD"),
      show: false,
    });
  };

  const showDatepicker = (gotDate) => {
    updateState({ ...iState, show: true });
  };

  const passwordHandler = (text) => {
    let data = checkPasswordFormat(text);
    if (data) {
      updateState({
        ...iState,
        passwordMessageShownFlag: !data,
        password: text,
      });
    } else {
      updateState({
        ...iState,
        passwordMessageShownFlag: !data,
        password: text,
      });
    }
  };

  // const confirmPasswordHandler=(text)=>{
  //   let tempConfirm =  text;
  // if(password.length > 0)
  // {
  //   updateState({...iState,confirmMessageShownFlag:!confirmMessageShownFlag})
  // }
  // else if (password === tempConfirm)
  // {
  //   updateState({...iState,confirmMessageShownFlag:!confirmMessageShownFlag,confirm:tempConfirm})
  // }
  // else
  // {
  //   updateState({...iState,confirmMessageShownFlag:!confirmMessageShownFlag,confirm:tempConfirm})
  // }
  // }

  const inputHandler = (e, inputName) => {
    updateState({ ...iState, [inputName]: e.nativeEvent.text });
  };

  const stateHandler = (item) => {
    updateState({
      ...iState,
      selectedState: item?.name,
      selectedStateID: item?._id,
      selectedCity: "",
      selectedCityID: "",
    });
    
  };
  const cityHandler = (item) => {
    updateState({
      ...iState,
      selectedCity: item?.cityName,
      selectedCityID: item?._id,
    });
  };

  // alert(email)

  const validater = async () => {
    if(formattedNumber.length<8){
      toastShow("Please enter valid number", colorConstant.errorRed)
      return
    }
    Validations.isEmpty(name)
      ? toastShow("Please enter name", colorConstant.errorRed)
      : Validations.isEmpty(birthday)
      ? toastShow("Please select DOB", colorConstant.errorRed)
      // : formattedNumber == ""
      // ? toastShow("Please enter number", colorConstant.errorRed)
      : // : formattedNumber.length !== 13
      // ? toastShow("Number is not correct", colorConstant.errorRed)
      Validations.isEmpty(selectedState)
      ? toastShow("Please select state", colorConstant.errorRed)
      : Validations.isEmpty(selectedCity)
      ? toastShow("Please select city", colorConstant.errorRed)
      : Validations.isEmpty(address)
      ? toastShow("Please enter address", colorConstant.errorRed)
      : Validations.isEmpty(email)
      ? toastShow("Please enter email address", colorConstant.errorRed)
      // : Validations.isEmail(email)
      // ? toastShow("Email format is not valid", colorConstant.errorRed)
      :
      Validations.isEmpty(password)
      // ? toastShow("Please enter address", colorConstant.errorRed)
      // : Validations.isEmpty(password)
      ? toastShow("Please enter password", colorConstant.errorRed)
      : Validations.isEmpty(confirm)
      ? toastShow("Please enter confirm password", colorConstant.errorRed)
      : matchConfirmPassword()
      ? toastShow(
          "Confirm password must be same as password",
          colorConstant.errorRed
        )
      : !tcFlag
      ? toastShow("Please check accept T&C and Privacy", colorConstant.errorRed)
      : checkEmailPhone();
    //
    //  await storeData("signUpData",JSON.stringify(signUpData));
    //  props.navigation.navigate("OtpVerify",{
    //    number:number
    //  })
    //  await checkEmailPhone();
  };

  const matchConfirmPassword = () => {
    return password === confirm ? false : true;
  };

  

  const signWithGoole = async () => {
    let signUpData = {
      // name,
      // birthday,
      // formattedNumber,
      // selectedState,
      // selectedCity,
      // address,
      // email,
      // password,
      // confirm,
      // tcFlag,
      // stateFlag,

      name,
      birthday,
      formattedNumber,
      selectedState,
      selectedCity,
      address,
      email,
      password,
      confirm,
      tcFlag,
      stateFlag,
      selectedStateID,
      selectedCityID,
      socialId,
      signupType,
    };

    // props.navigation.navigate("DocumentPicture", {

    await storeData(
      "signUpData",
      JSON.stringify(signUpData),
      (setDataFunction = () => {
        props.navigation.navigate("OtpVerify", {
          flow: "Google",
          number: formattedNumber,
          cCode: "91",
          stateFlag: "IN",
          // data:signUpData,
          // number :formattedNumber,
          // cCode:"91",
          // stateFlag:stateFlag
        });
      })
    );
    // await storeData("signUpData", JSON.stringify(signUpData));
    // props.navigation.navigate("OtpVerify", {
    //   flow: "Google",
    //   number :formattedNumber,
    //   cCode:"91",
    //   stateFlag:stateFlag
    // });
  };

  // alert(countryCode,'jkshdkjshfkj')

  const numberSignUpData = async () => {
    let numberSignUpData = {
      name,
      birthday,
      formattedNumber,
      selectedState,
      selectedCity,
      address,
      email,
      password,
      confirm,
      tcFlag,
      stateFlag,
      selectedStateID,
      selectedCityID,
      signupType,
    };
    await storeData(
      "signUpData",
      JSON.stringify(numberSignUpData),
      (setDataFunction = () => {
        props.navigation.navigate("DocumentPicture", {
          flow: "Number",
          data: numberSignUpData,
        });
      })
    );
  };
  // ********Api call**********

  const getCityList = async () => {
    try {
      let response = await axios.get(
        `static/city-list?stateId=${selectedStateID}`
      );

      if (response && response.status == 200) {
        updateState({ ...iState, cityList: response.data.askedCity });
      } else {
        toastShow(error.message, colorConstant.errorRed);
      }
    } catch (error) {
      console.log(" get city list error", error);
      toastShow(error, colorConstant.errorRed);
    }
  };

  const checkEmailPhone = async () => {
    try {
      let reqData = {
        email: email,
        phoneNumber: "",
      };


      let response = await axios.post(`auth/check-driver`, reqData);
      if (response && response.status == 200) {
        toastShow(response.data.message, colorConstant.errorRed);
      } else {
        route?.params?.flow !== "Google" ? numberSignUpData() : signWithGoole();
      }
    } catch (error) {
      console.log("error catch", error);
      toastShow(error, colorConstant.errorRed);
    }
  };

  const dummyNavigate = () => {
    props.navigation.navigate("OtpVerify", {
      flow: "Google",
      number: number,
      cCode: "91",
      stateFlag: stateFlag,
    });
  };

  //region user Effect area

  useEffect(() => {
    if (route?.params?.data?.email != undefined) {
      updateState({
        ...iState,
        email: route.params.data.email,
        name: route.params.data.name,
        socialId: route.params.data.id,
        signupType: "GOOGLE",
      });
    }
  }, []);

  // useEffect(() => {
  //   if(route.params.data)
  // }, [])

  // const getDataFlag=()=>{
  //   updateState({ ...iState,email:route.params.data.email,name:route.params.data.name,socialId:route.params.data.id,signupType:"GOOGLE",
  //   })
  // }
  return (
    <View style={styles.main}>
      <Text style={styles.text1}>Welcome to LadyCar!</Text>

      <KeyboardAwareScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
      >
        <CustomInput
          marginTop={20}
          placeholder={"First and Last name"}
          value={name}
          onChange={(e) => inputHandler(e, "name")}
          editable={signupType == "GOOGLE" ? false : true}
        />

        {/* {alert(signupType)} */}

        <Pressable
          onPress={showDatepicker}
          style={{
            width: "80%",
            alignSelf: "center",
            borderColor: colorConstant.purple,
            flexDirection: "row",
            alignItems: "center",
            borderWidth: 1.5,
            marginTop: 25,
            borderRadius: 10,
            paddingHorizontal: 25,
            ...Platform.select({
              ios: {
                height: 50,
              },
            }),
          }}
        >
          <TextInput
            placeholder="Birthday Date"
            placeholderTextColor={colorConstant.placeholderColor}
            style={styles.textinput}
            editable={false}
            value={birthday}
          />
          <Image
            source={imageConstant.drop}
            resizeMode="contain"
            style={{ widht: 11, height: 7, position: "absolute", right: 30 }}
          />
        </Pressable>


        {show && (
          <DateTimePicker
          display={Platform.OS=='ios' ? 'spinner' :'default'}
          // accentColor="red"
          // backgroundColor={'red'}
            placeholder="Birthday Date"
            placeholderText="#000"
            value={date}
            mode={"date"}
            is24Hour={true}
            minimumDate={new Date(moment().subtract(150, "years"))} 
            maximumDate={new Date(moment().subtract(21, "years"))}
            onChange={onChangeDate}
            style={{
              width: "90%",
              alignSelf: "center",
              marginTop: 10,
              color: "#000",
              backgroundColor:'#ffff'
            }}
          />
        )}

        <PhoneInput
          ref={phoneInput}
          defaultValue={formattedNumber}
          defaultCode={stateFlag}
          textInputProps={{
            maxLength: 13,
            value: formattedNumber,
            editable: signupType == "NORMAL" ? false : true,
            
          }}
          layout="first"
          // onChangeFormattedText={(text) => {
          //   // alert(text)
          //   updateState({ ...iState, formattedNumber: text });
          // }}

          onChangeCountry={(code) => {
            updateState({
              ...iState,
              countryCode: code?.callingCode?.[0],
              stateFlag: code?.cca2,
            });
          }}
          onChangeText={(val) =>
            updateState({ ...iState, formattedNumber: val })
          }
          containerStyle={{
            width: "80%",
            height: 48,
            borderWidth: 1.5,
            borderRadius: 10,
            marginTop: 20,
            borderColor: colorConstant.purple,
            alignSelf: "center",
          }}
          textContainerStyle={{
            paddingVertical: 0,
            backgroundColor: colorConstant.white,
            borderTopRightRadius: 10,
            borderBottomRightRadius: 10,
          }}
        />

        <CustomDropDown
          placeholder="Select State"
          dropDownData={stateList}
          labelField="name"
          valueField="_id"
          marginTop={20}
          value={selectedStateID}
          onChange={stateHandler}
        />

        {selectedStateID !== "" && (
          <CustomDropDown
            placeholder="Select City"
            dropDownData={cityList}
            marginTop={20}
            labelField="cityName"
            valueField="_id"
            value={selectedCityID}
            onChange={cityHandler}
          />
        )}
        <CustomInput
          marginTop={20}
          value={address}
          onChange={(e) => inputHandler(e, "address")}
          placeholder={"Address"}
        />

        <CustomInput
          marginTop={20}
          value={email}
          onChange={(e) => inputHandler(e, "email")}
          placeholder={"Email Address"}
          // editable={flow == "Google" ? true : false}
          editable={signupType == "GOOGLE" ? false : true}
        />
        <CustomPasswordInput
          marginTop={20}
          placeholder={"Password"}
          value={password}
          onChangeText={passwordHandler}
        />
        {passwordMessageShownFlag && password.length > 0 && (
          <Text
            style={{
              fontSize: 10,
              fontFamily: fontConstant.regular,
              color: colorConstant.errorRed,
              width: "80%",
              marginTop: 10,
              alignSelf: "center",
            }}
          >
            Required minimum 8 length,1 Small,Capital & Special Character
          </Text>
        )}
        <CustomInput
          marginTop={20}
          onChange={(e) => {
            inputHandler(e, "confirm");
          }}
          placeholder={"Confirm password"}
        />

        {/* {
        confirmMessageShownFlag &&  confirm.length > 0 && (
          <Text style={{
            fontSize:10,
            fontFamily:fontConstant.regular,
            color:colorConstant.errorRed,
            width:"80%",
            marginTop:10,
            alignSelf:"center",
          }}>Confirm password must be same as password</Text>
        )
      } */}

        <View style={styles.termsView}>
          <View
            style={{
              flexDirection: "row",
              width: "75%",
              flexWrap: "wrap",
            }}
          >
            <Text
              style={{
                fontSize: 14,
                fontFamily: fontConstant.regular,
                color: colorConstant.black,
              }}
            >
              I accept the LadyCar{" "}
            </Text>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                alert("Will redirect to T&C");
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: fontConstant.regular,
                  color: colorConstant.purple,
                  textDecorationLine: "underline",
                }}
              >
                Terms of Service{" "}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                alert("Will redirect to Guidelines");
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: fontConstant.regular,
                  color: colorConstant.purple,
                  textDecorationLine: "underline",
                  marginVertical: 3,
                }}
              >
                Community Guidelines
              </Text>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 14,
                fontFamily: fontConstant.regular,
                color: colorConstant.black,
                marginVertical: 3,
              }}
            >
              {" "}
              and{" "}
            </Text>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                alert("Will redirect to Privacy Policy");
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: fontConstant.regular,
                  color: colorConstant.purple,
                  textDecorationLine: "underline",
                  marginVertical: 3,
                }}
              >
                Privacy Policy.
              </Text>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 14,
                fontFamily: fontConstant.regular,
                color: colorConstant.black,
                marginVertical: 3,
              }}
            >
              (Required)
            </Text>
          </View>

          <TouchableOpacity
            activeOpacity={0.9}
            onPress={() => updateState({ ...iState, tcFlag: !tcFlag })}
            style={{
              borderWidth: 2,
              width: 20,
              height: 20,
              borderRadius: 10,
              backgroundColor: tcFlag ? colorConstant.purple : "#FFFFFF",
            }}
          ></TouchableOpacity>
        </View>

        <CustomButton
          buttonText={"Continue"}
          bottom={20}
          marginVertical={20}
          width={"85%"}
          // checkEmailNumber
          OnButtonPress={validater}
          // OnButtonPress={() => props.navigation.navigate("DocumentPicture")}
        />
      </KeyboardAwareScrollView>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
  },
  text1: {
    fontSize: 30,
    fontFamily: fontConstant.bold,
    color: colorConstant.purple,
    textAlign: "center",
    marginTop: 70,
  },
  textinput: {
    paddingVertical: 8,
  },
  termsView: {
    flexDirection: "row",
    alignSelf: "center",
    marginVertical: "6%",
    alignItems: "center",
    justifyContent: "space-between",
  },
  text2: {
    fontSize: 12,
    fontFamily: fontConstant.regular,
    color: colorConstant.black,
    // textAlign:"left"
  },
  text3: {
    fontSize: 12,
    fontFamily: fontConstant.regular,
    color: colorConstant.purple,
    // textAlign:"left",
    textDecorationLine: "underline",
  },
});
