import { Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View ,PermissionsAndroid, BackHandler, Platform, ScrollView} from 'react-native'
import React ,{ useState }from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import CustomMapView from '../customComponents/CustomMapView'
import CustomCard from '../customComponents/CustomCard'
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import { useEffect } from 'react'
import { Height } from '../dimension/dimension'
import { useFocusEffect } from '@react-navigation/native'

const ConfirmPickup = (props) => {
    const [verified,setVerified] =  useState(true);
    useEffect(()=>{

        let subscription = props.navigation.addListener('focus',()=>{
            setVerified(true)
        })
       if(!verified)
       {
        setTimeout(()=>{
            props.navigation.navigate("SafeDrive")
        },5000)
       }
       return()=>subscription;
    },[verified])

    const requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.CAMERA,
              {
                title: 'Camera Permission',
                message: 'App needs camera permission',
              },
            );
            // If CAMERA Permission is granted
            return granted === PermissionsAndroid.RESULTS.GRANTED;
          } catch (err) {
            console.warn(err);
            return false;
          }
        } else return true;
      };
      const requestExternalWritePermission = async () => {
        if (Platform.OS === 'android') {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                title: 'External Storage Write Permission',
                message: 'App needs write permission',
              },
            );
            // If WRITE_EXTERNAL_STORAGE Permission is granted
            return granted === PermissionsAndroid.RESULTS.GRANTED;
          } catch (err) {
            console.warn(err);
            alert('Write permission err', err);
          }
          return false;
        } else return true;
      };
      const handleOpenCamera = async () => {
        let isCameraPermitted = await requestCameraPermission();
        let isStoragePermitted = await requestExternalWritePermission();
        if (isCameraPermitted && isStoragePermitted) {
            console.log("access-->",isCameraPermitted,isStoragePermitted)
            let options={
                mediaType:'photo',
                cameraType:'back',
                saveToPhotos:true,
            }
          await launchCamera(options, response => { 
            if (response.didCancel) { 
            console.log('Cancelled');
            } else if (response.error) { 
            console.log('Error', response.errorMessage);
            } else { 
            console.log(response); 
            setVerified(!verified);
            // setFilePath(response.uri); 
            // setBase64('data:image/png;base64,' + response.base64); 
          } 
          })
        }
      }

      useFocusEffect(
        React.useCallback(() => {
          const backAction = () => {
            console.log("called")
            props.navigation.navigate("OnWay")
            return true;
          };
      
          const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
          );
      
          return () => backHandler.remove();
        }, [])
      );
  return (
      <SafeAreaView style={styles.main}>
          <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
              <View>
                  <CustomMapView 
                  height={Platform.OS == "ios" ? Height*0.65 : Height*0.70}
                  />


                  <View style={styles.row}>
                      <TouchableOpacity
                          onPress={() => props.navigation.toggleDrawer()}
                      >
                          <Image
                              resizeMode='contain'
                              source={imageConstant.drawer}
                              style={styles.img} />
                      </TouchableOpacity>
                      <TouchableOpacity
                          activeOpacity={1}
                          onPress={() => props.navigation.navigate("Profile")} >
                          <Image
                              resizeMode='contain'
                              source={imageConstant.profile1}
                              style={{
                                  width: 35,
                                  height: 35
                              }} />
                      </TouchableOpacity>

                  </View>

                 {
                     verified 
                     ?
                     <CustomCard
                     position={'absolute'}
                     bottom={20}
                     
                     />
                     :
                     <CustomCard
                     position={'absolute'}
                     bottom={20}
                     speech={imageConstant.speech} 
                     onSpeechPress={()=> props.navigation.navigate("Chat")}
                     />
                 }
              </View>

              {
                  verified ?
                      //Not verified yet
                      <>
                          <Text style={styles.text1}>Confirm your identity to pick up the{'\n'}passenger </Text>
                       <View style={{
                           flexDirection:"row",
                           marginTop:30,
                           alignItems:"center",
                           justifyContent:"space-between",
                           width:"80%",
                           alignSelf:"center",
                       }}>

                       <TouchableOpacity
                          onPress={handleOpenCamera}
                            //   onPress={()=>setVerified(!verified)}
                              style={styles.buttonView}>
                            
                                  <Text style={styles.text2}>Face ID</Text>
                                 
                          </TouchableOpacity>

                              <TouchableOpacity
                              activeOpacity={0.8}>
                                  <Image
                                      resizeMode='contain'
                                      source={imageConstant.info}
                                      style={{
                                          width: 24,
                                          height: 24
                                      }}
                                  />

                              </TouchableOpacity>
                       </View>
                        
                         
                      </>
                      :

                      //After verified
                      <>

                          <Text style={styles.text4}>
                              Wait for your passenger to verify her/{'\n'}his identity before getting in the car
                          </Text>

                          <View style={[{...styles.row4},{marginTop:40}]}>
                              <Text style={styles.text5}>Driver’s face validation</Text>
                              <View style={styles.row5}>
                                  <Text style={{
                                      fontSize:12,
                                      fontWeight:"400",
                                      fontFamily:fontConstant.regular,
                                      color:colorConstant.black
                                  }}>Pending</Text>
                                  <Image
                                      source={imageConstant.sand}
                                      style={styles.img4} />
                                 
                                 
                              </View>
                          </View>


                          <View style={styles.row4}>
                              <Text style={styles.text5}>Passenger’s face validation</Text>
                              <View style={styles.row5}>
                                  <Text style={{
                                      fontSize:12,
                                      fontWeight:"400",
                                      fontFamily:fontConstant.regular,
                                      color:colorConstant.black
                                  }}>Done</Text>
                                  <Image
                                      source={imageConstant.ok}
                                      style={styles.img4} />
                                  
                              </View>
                          </View>
                      </>
              }


              <View style={styles.row3}>
                  <TouchableOpacity
                //   onPress={()=>props.navigation.navigate("SafeDrive")}
                      activeOpacity={1}>
                      <Text style={styles.text3}>Cancel</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                      activeOpacity={1}>
                      <Image
                          source={imageConstant.info}
                          style={styles.img3} />
                  </TouchableOpacity>

              </View>
              </ScrollView>
      </SafeAreaView>
    
  )
}

export default ConfirmPickup

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    row:{
        flexDirection:"row",
        width:"93%",
        alignSelf:"center",
        justifyContent:"space-between",
        position:"absolute",
        top:20
    },
    row1:{
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"center",
    },
    img:{
        width:32,
        height:32
    },
    text:{
        fontSize:28,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        textAlign:"center"
    },
    text1:{
        fontSize:18,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        textAlign:"center",
        marginTop:20
    },
    img1:{
        width:18,
        height:18,
        marginLeft:10
    },
    img2:{
        width:13,
        height:13,
    },
    img3:{
        width:22,
        height:22,
        marginLeft:5,
        marginTop:3
    },
    buttonView:{
        flexDirection:"row",
        width:"90%",
        height:40,
        alignSelf:"center",
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:colorConstant.purple,
        borderRadius:10
    },
    text2:{
        fontSize:16,
        fontFamily:fontConstant.semi,
        color:colorConstant.white,
    },
    infoTouch:{
        position:"absolute",
        right:13,
    },
    row3:{
        flexDirection:"row",
        alignSelf:"center",
        justifyContent:"center",
        alignItems:"center",
        ...Platform.select({
            ios:{
                marginTop:30
            },
            android:{
                position:"absolute",
                bottom:20
            }
        })

    },
    text3:{
        fontSize:16,
        fontFamily:fontConstant.regular,
        color:colorConstant.purple,
        textDecorationLine:"underline",
    },
    text4:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        textAlign:"center",
        marginTop:20,
        width:"90%",
        alignSelf:"center"
    },
    text5:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.black,


    },
    img4:{
        width:25,
        height:25,
        marginLeft:10
    },
    row4:{
    flexDirection:"row",
    width:"90%",
    alignSelf:"center",
    alignItems:"center",
    marginTop:10
    },
    row5:{
        flexDirection:"row",
        position:"absolute",
        right:10
    }
})