import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import CustomHeader from '../customComponents/CustomHeader'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { heightPercentageToDP } from '../utils/responsiveFIle'
import CustomButton from '../customComponents/CustomButton'

const DriverLicence = (props) => {
  return (
    <View style={styles.main}>
      <CustomHeader
      img={imageConstant.back}
      navigation={props.navigation}
      />

     <View style={styles.headingView}>
     <Text style={styles.text1}>Take a picture of {'\n'} your driver's license</Text>
     <TouchableOpacity
     >
         <Image
         source={imageConstant.info}
         style={styles.img}/>
     </TouchableOpacity>
     </View>

     <Image 
     resizeMode='contain'
     source={imageConstant.license}
     style={styles.img2}
     />
     <CustomButton
     buttonText={"Driver's Licence"}
     position={'absolute'}
     bottom={Platform.OS == 'ios'  ? 30 : 20}
     />
    </View>
  )
}

export default DriverLicence

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    text1:{
        fontSize:29,
        fontFamily:fontConstant.semi,
        color:colorConstant.purple,
        textAlign:"center"
    },
    headingView:{
        width:"80%",
        flexDirection:"row",
        alignItems:"center",
        alignSelf:"center"
    },
    img:{
        width:20,
        height:20
    },
    img2:{
        width:"80%",
        height:heightPercentageToDP('35%'),
        alignSelf:"center",
        marginTop:heightPercentageToDP('12%')
    }
})