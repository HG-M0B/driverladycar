import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import React from "react";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import CustomMapView from "../customComponents/CustomMapView";
import { useState, useEffect } from "react";
import BottomSheet from "@gorhom/bottom-sheet";
import { useMemo, useRef, useCallback } from "react";
import { Height } from "../dimension/dimension";
import { getCurrentPosition } from "../utils/GoogleHelperFunction";
import Geolocation from "@react-native-community/geolocation";
import AsyncStorage from "@react-native-async-storage/async-storage";

import {
  deiverLoactionServices,
  DriverLooactionServices,
  getUserProfileServices,
} from "../services/DriverServices";
import { useDispatch, useSelector } from "react-redux";
import { actions } from "../redux/reducers";
import toastShow from "../utils/toastShow";
import LoaderModal from "../customComponents/LoaderModal";
import { useIsFocused } from "@react-navigation/native";

let initialState = {
  toggle: false,
  open: true,
  i: 0,
};

// const getCurrenLoaction = async () => {
//   try {
//     const respose = await getCurrentPosition();
//     console.log(respose);
//   } catch (error) {
//     console.log(error);
//   }
// };

const DriverWorkingStatus = (props) => {
  const [isActive, setIsActive] = useState(false);
  const [isLoading,setIsLoading]=useState(false)
  const isFocoused=useIsFocused()
  const sheetRef = useRef(null);
  const dispatch=useDispatch()
  const {driverLocation}  = useSelector(state => state.reducers)


//   console.log('====================================');
//   console.log(driverLocation,'lskajdlakdjalskdjalksdjlskdj');
//   console.log('====================================');
  const handleSheetChanges = useCallback((index) => {
    sheetRef.current?.snapToIndex(index);
    // updateState({ ...iState, open: false, i: index });
  }, []);
//   const { toggle, open, i } = iState;
  const snapPoints = useMemo(() => ["30%"], []);

  useEffect(()=>{
    getLocation();
    //   setTimeout(()=>{
    //       props.navigation.navigate("TripRequest");
    //   },3000)
  },[])

  const getLocation = async () =>{
    // console.log("called")
    getCurrentPosition()
        .then(location =>{
        //   console.log("get location--->",location)
          dispatch(actions.setDriverLocation(location))
        })
        .catch(error =>{
          // alert('sajkhdaskj')
        //   toastShow(error.message,colorConstant.errorRed)
        });
  }

  console.log('====================================');
  console.log(isActive);
  console.log('====================================');
  const driverProfileFunction=async()=>{
    setIsLoading(true)
  try {
    const response=await getUserProfileServices()
  
    console.log(response.data.userData.is_online,'apinnnnnn')
      setIsActive(response.data.userData.is_online)
      setIsLoading(false)
      await AsyncStorage.setItem("token", response.data.userData.jwtToken);
      if(response.data.userData.is_online){
        props.navigation.navigate("TripRequest");
      }
      
  } catch (error) {
    console.log(error)
    setIsLoading(false)
  }
  }

  const onToggle = async () => {
    // setIsActive(!isActive)
    console.log(isActive,'inner')
    setIsLoading(true)
    let objToSend={
        is_online:!isActive,
        lat:driverLocation.latitude,
        long:driverLocation.longitude
    }
    try {
      const response = await DriverLooactionServices(objToSend);
      setIsLoading(false)
      console.log(objToSend)
      await AsyncStorage.setItem("token", response.data.userData.jwtToken);
      driverProfileFunction()
      // props.navigation.navigate("TripRequest");

    } catch (error) {
      // props.navigation.navigate("TripRequest");
      console.log(objToSend);
      setIsLoading(false)
      setIsActive(false)
      toastShow("Something went wrong plase try again",)

      // alert("Something went wrong")
      setIsActive(!isActive)

    }
  };


  useEffect(() => {
    let mount = true
    if(mount){
      driverProfileFunction()
    }
    return () => {
      mount= false
    }
  }, [isFocoused])
  

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: colorConstant.white,
      }}
    >
      <View style={styles.main}>
        <CustomMapView height={Height * 0.71} />

        <View style={styles.headerView}>
          <View></View>
          {/* <TouchableOpacity 
        onPress={()=>props.navigation.toggleDrawer()}
        >
            <Image
            resizeMode='contain'
            source={imageConstant.drawer}
            style={styles.img} />
        </TouchableOpacity> */}

          {/* <Text style={styles.text}>$160.98   I   12</Text> */}
          <Image
            resizeMode="contain"
            source={imageConstant.profile1}
            style={styles.img1}
          />
        </View>

        <BottomSheet
          index={0}
          ref={sheetRef}
          snapPoints={snapPoints}
          onChange={handleSheetChanges}
          enableContentPanningGesture={true}
        >
          <>
            <Text style={styles.text1}>You're offline</Text>
            <Text style={styles.text2}>Go online to start receiving rides</Text>

            <TouchableOpacity
              style={styles.toggleTouch}
              activeOpacity={1}
              onPress={()=>{onToggle()}}
            >
              <Image
                source={isActive ? imageConstant.on : imageConstant.off}
                style={styles.img3}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </>
        </BottomSheet>
      </View>
      <LoaderModal isLoading={isLoading}/>
    </SafeAreaView>
  );
};

export default DriverWorkingStatus;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
  },
  headerView: {
    position: "absolute",
    top: 15,
    width: "95%",
    alignSelf: "center",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  img: {
    width: 40,
    height: 40,
  },
  text: {
    fontSize: 16,
    fontFamily: fontConstant.bold,
    color: colorConstant.purple,
    backgroundColor: colorConstant.white,
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 9,
    },
    shadowOpacity: 0.5,
    shadowRadius: 12.35,

    elevation: 19,
  },
  img1: {
    width: 30,
    height: 30,
  },
  text1: {
    fontSize: 24,
    fontFamily: fontConstant.bold,
    color: colorConstant.black,
    width: "90%",
    alignSelf: "center",
    marginTop: 20,
  },
  text2: {
    fontSize: 14,
    fontFamily: fontConstant.regular,
    color: colorConstant.grayText,
    width: "90%",
    alignSelf: "center",
    marginTop: 15,
  },
  toggleTouch: {
    marginTop: 40,
    alignSelf: "center",
  },
  img2: {
    width: 90,
    height: 60,
  },
  img3: {
    width: 60,
    height: 40,
  },
});
