import { StyleSheet, Text, View,FlatList, Image, TouchableOpacity, Platform } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'

const PendingTrip = (props) => {
   const renderCard=({item,index})=>{
       return(
           <TouchableOpacity 
           onPress={()=>props.navigation.navigate("PendingTripDetails")}
           activeOpacity={0.8}
           style={{
               flexDirection: "row",
            //    backgroundColor:"pink",
               width: "90%",
               alignSelf: "center", alignItems: "center",
               paddingBottom: 20,
               borderBottomWidth: 1,
               borderBottom: 0.5,
               borderBottomColor:colorConstant.grayBg,
               marginVertical: 10
           }} key={index}>
              <Image 
              resizeMode='contain'
              style={{
                  width:40,
                  height:40
              }}
              source={imageConstant.profile4}/>
              <View style={{
                  marginLeft:10
              }}>
              <Text style={{
                  fontSize:14,
                  fontFamily:fontConstant.semi,
                  color:colorConstant.black
              }}>SONIA</Text>
              <Text style={{
                  fontSize:12,
                  fontFamily:fontConstant.regular,
                  color:colorConstant.black
              }}>Sep 19  12:25 PM</Text>
              </View>
              <View style={{
                  flexDirection:"row",
                  alignItems:"center",
                  position:"absolute",
                  top:3,
                  right:0
              }}>
              <Text style={{
                  fontSize:22,
                  fontFamily:fontConstant.bold,
                  color:colorConstant.purple,
                  marginRight:10
              }}>$9</Text>
              <Image 
              resizeMode='contain'
              style={{
                  width:10,
                  height:12,
                 
              }}
              source={imageConstant.angle}/>
              </View>

           </TouchableOpacity>
       )
   } 
  return (
    <View style={styles.main}>

          <FlatList
              showsVerticalScrollIndicator={false}
              bounces={false}
              data={[{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }, { id: 7 }, { id: 8 }]}
              renderItem={renderCard}
              keyExtractor={(item) => item.id}
              contentContainerStyle={styles.contain}
          />
       
    </View>
  )
}

export default PendingTrip

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    contain:{
        ...Platform.select({
            ios:{
                paddingVertical:10,
            },
            android:{
                paddingVertical:40,
            }
        })
       
    }


})