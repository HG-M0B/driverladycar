import {
  View,
  Text,
  StyleSheet,
  Pressable,
  Image,
  TextInput,
  ScrollView,
  SafeAreaView,
  Keyboard,
  Platform,
} from "react-native";
import React from "react";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import { Height, Width } from "../dimension/dimension";
import CustomButton from "../customComponents/CustomButton";
import CustomHeader from "../customComponents/CustomHeader";
import { useEffect } from "react";
import { useState } from "react";
import CustomInput from "../customComponents/CustomInput";
import { KeyboardAwareScrollView } from "@codler/react-native-keyboard-aware-scroll-view";
import { checkUserExist } from "../services/AuthServices";
import toastShow from "../utils/toastShow";

const Forget = (props) => {
  const [flow, setFlow] = useState("");
  const [padding, setPadding] = useState(null);
  const [phoneNumber, setPhoneNumber] = useState("");

  const checkuserexistFunction = async () => {
    let objToSend = {
      phoneNumber: phoneNumber,
      email: "",
    };
    try {
      let response = await checkUserExist(objToSend);
      console.log(response.data,'djdjdjddj')
            props.navigation.navigate("OtpVerify", {
            flow: "forget",
            number: phoneNumber,
            cCode: "91",
            stateFlag: "IN",
          });
    } catch (error) {
      toastShow('User not exits please signup',colorConstant.purple)
      console.log("====================================");
      console.log(error);
      console.log("====================================");
    }
  };

  // *********Keyboard padding**********
  useEffect(() => {
    const showSubscription = Keyboard.addListener("keyboardDidShow", (e) => {
      if (Platform.OS === "ios") {
        console.log("e.endCoordinates.height", e.endCoordinates.height);
        setPadding(30);
      }
    });
    const hideSubscription = Keyboard.addListener("keyboardDidHide", () => {
      if (Platform.OS === "ios") {
        setPadding(10);
      }
    });

    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);

  //**********Flow manage*/
  useEffect(() => {
    if (props.route.params.flow == "login") {
      console.log(props.route.params.flow);
      setFlow("login");
    }
  }, []);

  console.log(padding);
  return (
    <SafeAreaView style={styles.main}>
      <CustomHeader navigation={props.navigation} />
      <KeyboardAwareScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          ...Platform.select({
            ios: {
              paddingBottom: padding,
            },
          }),
        }}
      >
        <Text style={styles.text1}>Forgot Password</Text>
        <Image
          resizeMode="contain"
          style={styles.img1}
          source={imageConstant.forgetPassword}
        />
        <CustomInput
          value={phoneNumber}
          ke
          width={"80%"}
          placeholder={"Phone Number"}
          marginTop={30}
          onChangeText={(val) => setPhoneNumber(val)}
          keyboardType="numeric"
        />
        <CustomButton
          buttonText={"CONFIRM"}
          width={"80%"}
          marginTop={"6%"}
          borderRadius={10}
          OnButtonPress={checkuserexistFunction}
        />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default Forget;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
  },
  pres: {
    alignSelf: "flex-start",
    left: "6%",
    marginTop: "7%",
  },
  text1: {
    fontSize: 29,
    color: colorConstant.purple,
    fontFamily: fontConstant.semi,
    marginTop: "5%",
    alignSelf: "center",
  },
  img1: {
    height: 316,
    width: 160,
    alignSelf: "center",
    marginTop: "7%",
  },
  input: {
    width: Width * 0.8,
    borderWidth: 1,
    height: 38,
    borderRadius: 10,
    borderColor: colorConstant.purple,
    marginTop: "13%",
    paddingLeft: 15,
  },
});
