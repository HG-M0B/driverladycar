import { Image, ImageBackground, Modal, Platform, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { Height } from '../dimension/dimension'
import { useState } from 'react'
import CustomButton from '../customComponents/CustomButton'

const Chat = (props) => {
    
  return (
      <SafeAreaView style={styles.main}>
          <View style={styles.header}>
              <TouchableOpacity onPress={() => props.navigation.goBack()}>
                  <Image
                      resizeMode='contain'
                      source={imageConstant.back}
                      style={{
                          width: 20,
                          height: 20,
                          tintColor: colorConstant.purple
                      }}
                  />
              </TouchableOpacity>

              <View style={[{ ...styles.row }, { marginLeft: 20 }]}>
                  <Image
                      resizeMode='contain'
                      source={imageConstant.profile1}
                      style={{
                          width: 25,
                          height: 25,
                      }}
                  />
                  <Text style={styles.text}>THE COOL BOY</Text>
              </View>


              <View style={[{ ...styles.row }, { position: "absolute", right: 0 }]}>
                  <TouchableOpacity
                      style={{
                          marginRight: 10,
                          padding: 5
                      }}>
                      <Image
                          resizeMode='contain'
                          source={imageConstant.phone}
                          style={{
                              width: 20,
                              height: 20,
                              tintColor: colorConstant.purple
                          }}
                      />
                  </TouchableOpacity>
                  <TouchableOpacity
                      style={{
                          padding: 5
                      }}>
                      <Image
                          resizeMode='contain'
                          source={imageConstant.camera}
                          style={{
                              width: 20,
                              height: 20,
                              tintColor: colorConstant.purple
                          }}
                      />
                  </TouchableOpacity>



              </View>

          </View>
          <View style={styles.lineView}></View>
          <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                  paddingTop: 30,
                  paddingBottom:20,
              }}
          >


              <View style={styles.square}>
                  <Text style={styles.text1}>Hey, cool boy </Text>
              </View>
              <View style={styles.round}>
                  <Text style={styles.text2}>Hey </Text>
              </View>
              <View style={styles.square}>
                  <Text style={styles.text1}>HOw are you ?</Text>
              </View>
              <View style={styles.round}>
                  <Text style={styles.text2}>Cool 😎</Text>
              </View>
              <View style={styles.round}>
                  <Text style={styles.text2}>What are doing ??</Text>
              </View>
              <View style={styles.square}>
                  <Text style={styles.text1}>Nothing much what about you ?? </Text>
              </View>
              <View style={styles.round}>
                  <Text style={styles.text2}>Nothing </Text>
              </View>
              <View style={styles.square}>
                  <Text style={styles.text1}>Cool So 8 pm at your place </Text>
              </View>
              <View style={styles.round}>
                  <Text style={styles.text2}>Perfect waitin for you 😎</Text>
              </View>

           
          </ScrollView>

          <View style={styles.bottomView}>
              <View style={styles.inputView}>
                  <TouchableOpacity style={{
                      backgroundColor: colorConstant.purple,
                      padding: 5,
                      borderRadius: 25,
                      marginLeft: 5

                  }}>
                      <Image
                          resizeMode='contain'
                          source={imageConstant.camera}
                          style={{
                              width: 20,
                              height: 20
                          }} />
                  </TouchableOpacity>

                  <TextInput
                      placeholder='Type here...'
                      style={styles.inputField} />
                  <TouchableOpacity>
                      <Image
                          resizeMode='contain'
                          source={imageConstant.send}
                          style={{
                              width: 20,
                              height: 20,
                              tintColor: colorConstant.purple
                          }}
                      />
                  </TouchableOpacity>
              </View>

              <TouchableOpacity style={{

              }}>
                  <Image
                      resizeMode='contain'
                      source={imageConstant.voice}
                      style={{
                          width: 40,
                          height: 40,
                          tintColor: colorConstant.purple
                      }}
                  />
              </TouchableOpacity>
          </View>
          
      </SafeAreaView>
  )
}

export default Chat

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    header:{
        flexDirection:"row",
        alignItems:"center",
        width:"90%",
        alignSelf:"center",
        marginTop:20,
      
    },
    row:{
        flexDirection:"row",
        alignItems:"center"
    },
    text:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        marginLeft:10
    },
    lineView:{
        marginTop:13,
        borderBottomWidth:1,
        borderBottomColor:colorConstant.purple
    },
    square:{
        borderWidth:2,
        paddingVertical:7,
        paddingHorizontal:10,
        borderRadius:10,
        borderColor:colorConstant.purple,
        maxWidth:"70%",
        alignSelf:"flex-end",
        right:20,
        marginVertical:10
    },
    text1:{
        fontSize:14,
        fontFamily:fontConstant.regular,
        color:colorConstant.purple
    },
    round:{
       backgroundColor:colorConstant.purple,
       maxWidth:"70%",
       borderRadius:30,
       paddingVertical:8,
       paddingHorizontal:15,
       alignSelf:"flex-start",
       left:20,
       marginVertical:10

    },
    text2:{
        fontSize:14,
        fontFamily:fontConstant.regular,
        color:colorConstant.white
    },
    inputView:{
        flexDirection:"row",
        width:"85%",
        alignItems:"center",
        borderWidth:2,
        borderColor:colorConstant.purple,
        borderRadius:20,
    },
    inputField:{
        paddingVertical:Platform.OS == "ios" ? 12 : 2,
        width:"80%"
    },
    bottomView:{
        alignSelf:"center",
        flexDirection:"row",
        alignItems:"center",
        position:"absolute",
        bottom:Platform.OS == 'ios' ?10: 0,
        height:Height*0.10,
        backgroundColor:"#FFFFFF"
    },
   
    
})