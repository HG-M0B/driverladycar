import {
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Modal,
  StatusBar,
} from "react-native";
import React, { useEffect } from "react";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import { Height, Width } from "../dimension/dimension";
import { useState } from "react";
import CustomButton from "../customComponents/CustomButton";
import { KeyboardAwareScrollView } from "@codler/react-native-keyboard-aware-scroll-view";
import {
  EditProfileServices,
  getUserProfileServices,
} from "../services/DriverServices";
import LoaderModal from "../customComponents/LoaderModal";
import { useMemo } from "react";
import { moderateScale, scale } from "react-native-size-matters";
import {
  SafeAreaView,
  useSafeAreaInsets,
} from "react-native-safe-area-context";
import PhoneInput from "react-native-phone-number-input";
import { GetImageUrlServices } from "../services/CommonServices";
import { uploadImageServices } from "../services/uploadImageServices";
import toastShow from "../utils/toastShow";

const EditAccount = (props) => {
  // all state define here
  const insets = useSafeAreaInsets();

  const intialState = {
    otp: "",
    countryCode: "",
    number: null,
    stateFlag: "",
  };
  // region this object form userInfomation
  const [userDetail, setUserDetail] = useState({
    fullName: "",
    phoneNumber: "",
    email: "",
    password: "",
    profileImage: "",
  });
  const [editFlag, setEditFlag] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [changePhoneNumberModal, setChangePhoneNumberModal] = useState(false);
  const [image, setImage] = useState("");
  const [showpassword,setShowpassword]=useState(true)

  // define all function here
  const uploadImageFunction = async () => {
    await GetImageUrlServices(getImageData);
    // console.log(response, "fdkjjfdkljsldkfjsldfjkl");
  };

  // #region for upload image in server
  const getImageData = async (imageName) => {
    setIsLoading(true);
    let finalFormData = new FormData();
    finalFormData.append("fileName", imageName);
    setIsLoading(true);
    try {
      const response = await uploadImageServices(finalFormData);
      console.log('====================================');
      console.log(response.data,'this is problem');
      console.log('====================================');
      setUserDetail({ ...userDetail, profileImage: response.data.data });
      setIsLoading(false);
      setImage(response.data.data);
      console.log(response.data.data, "0987654321", image);
      toastShow("Image upload successful",colorConstant.purple)
    } catch (error) {
      console.log(error,'this is error message')
      setIsLoading(false);
      toastShow("Something went wrong please try again",colorConstant.errorRed)

    }
  };

  const getUserProfileFunction = async () => {
    try {
      setIsLoading(true);
      const response = await getUserProfileServices();
      console.log(response.data, "lllllll ehat we cah do");
       let res =await response.data.userData.profilePic
       setImage(res);
       setUserDetail({
        ...userDetail,
        fullName: response.data.userData.name,
        phoneNumber: response.data.userData.phoneNumber,
        email: response.data.userData.email,
        password: response.data.userData.planePassword,
        profileImage: response.data.userData.profilePic,
      });
      // alert(userDetail.profileImage,image)
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
    }
  };

  // edit profile details services call here

  const editProfileHandler = async () => {
    setIsLoading(true);
    const objToSend = {
      phoneNumber: userDetail.phoneNumber,
      name: userDetail.fullName,
      email: userDetail.email,
      profilePic: userDetail.profileImage,
      planePassword: userDetail.password,
    };
    console.log(userDetail.profileImage, "this is profile image here");
    try {
      const response = await EditProfileServices(objToSend);
      console.log(response.data, "data send succuess", objToSend);
      getUserProfileFunction();
      setIsLoading(false);
      setEditFlag(true)
      toastShow('Profile updated successfully',colorConstant.successGreen)
    } catch (error) {
      setIsLoading(false);
      console.log(error, "this is error");
    }
  };

  // region useEffect area

  useEffect(() => {
    let mount = true;
    if (mount) {
      getUserProfileFunction();
    }
    return () => {
      mount = false;
    };
  }, []);

  return (
    <>
      {/* <StatusBar hidden/> */}
      <SafeAreaView 
      forceInset={{ top: 'always' }}
        edges={[]}
        style={{ flex: 1,  }}
      >
        <View style={styles.main}>
          <KeyboardAwareScrollView
            bounces={false}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.container}
          >
            <ImageBackground
              resizeMode="contain"
              source={imageConstant.header}
              style={styles.headerImage}
            >
              <View
                style={{
                  flexDirection: "row",
                  position: "absolute",
                  alignItems: "center",
                  justifyContent: "space-between",
                  width: "95%",
                  alignSelf: "center",
                  marginTop: Platform.OS == "ios" ? 40 : 10,
                  ...Platform.select({
                    ios: {
                      zIndex: 1,
                    },
                    android: {
                      zIndex: 1,
                    },
                  }),
                }}
              >
                <TouchableOpacity
                  style={{
                    padding: 5,
                  }}
                  onPress={() => props.navigation.goBack()}
                >
                  <Image
                    resizeMode="contain"
                    source={imageConstant.back}
                    style={{
                      width: 20,
                      height: 20,
                      tintColor: colorConstant.white,
                    }}
                  />
                </TouchableOpacity>

                {editFlag && (
                  <TouchableOpacity
                    style={{
                      padding: 5,
                    }}
                    onPress={() => {
                      setEditFlag(!editFlag);
                      // uploadImageFunction()
                    }}
                  >
                    <Image
                      resizeMode="contain"
                      source={imageConstant.edit}
                      tintColor={colorConstant.white}
                      style={{
                        width: 20,
                        height: 20,
                        tintColor: colorConstant.white,
                      }}
                    />
                  </TouchableOpacity>
                )}
              </View>

              <Text
                style={{
                  fontSize: 29,
                  fontFamily: fontConstant.semi,
                  color: colorConstant.white,
                  alignSelf: "center",
                  position: "absolute",
                  marginTop: Platform.OS == "ios" ? 85 : 80,
                }}
              >
                My Info
              </Text>

              <View
                style={{
                  position: "absolute",
                  left: Width / 2 - 50,
                  bottom: -50,
                }}
              >
                <View style={{height:105,width:105,borderRadius:105,overflow:"hidden",borderWidth:2,borderColor:colorConstant.purple}}>
          
                      <Image
                        resizeMode="contain"
                        source={{
                          // uri:userDetail.profileImage,
                         uri:userDetail.profileImage !== ""
                            ? userDetail.profileImage
                            : "https://preview.redd.it/h5gnz1ji36o61.png?width=225&format=png&auto=webp&s=84379f8d3bbe593a2e863c438cd03e84c8a474fa",
                        }}
                        style={{
                          width: 105,
                          height: 105,
                          borderRadius: 105,
                        }}
                      />
                    
                    </View>

                {!editFlag && (
                  <TouchableOpacity
                    style={{
                      position: "absolute",
                      right: 3,
                      bottom: 4,
                      padding: 7,
                      borderRadius: 25,
                      backgroundColor: "#D9D9D9",
                    }}
                    onPress={uploadImageFunction}
                  >
                    <Image
                      resizeMode="contain"
                      source={imageConstant.pencil}
                      style={{
                        width: 17,
                        height: 17,
                      }}
                    />
                  </TouchableOpacity>
                )}
              </View>
            </ImageBackground>

            <Text
              style={[
                { ...styles.title },
                {
                  ...Platform.select({
                    ios: {
                      marginTop: Height * 0.1,
                    },
                    android: {
                      marginTop: Height * 0.13,
                    },
                  }),
                },
              ]}
            >
              First and Last Name
            </Text>
            <View style={styles.inputView}>
              <TextInput
                placeholder="Full Name"
                placeholderTextColor={colorConstant.placeholderColor}
                style={styles.input}
                editable={editFlag ? false : true}
                value={userDetail.fullName}
                onChangeText={(val) =>
                  setUserDetail({ ...userDetail, fullName: val })
                }
              />
              {/* <Image
                resizeMode="contain"
                source={imageConstant.angle}
                style={styles.img1}
              /> */}
            </View>

            <Text style={[{ ...styles.title }, { marginTop: 25 }]}>
              Phone Number
            </Text>
            <TouchableOpacity
              disabled
              onPress={() => setChangePhoneNumberModal(true)}
              style={styles.inputView}
            >
              <Text
                // placeholder="Phone Number"
                style={styles.input}
                // placeholderTextColor={colorConstant.placeholderColor}
                // editable={editFlag ? false : true}
                // value={userDetail.phoneNumber}
                // onChangeText={(val) =>
                //   setUserDetail({ ...userDetail, phoneNumber: val })
                // }
              >
                {userDetail.phoneNumber}
              </Text>
              {/* <Text style={styles.greenText}>Verified</Text> */}
              {/* <Image
                resizeMode="contain"
                source={imageConstant.angle}
                style={styles.img1}
              /> */}
            </TouchableOpacity>

            <Text style={[{ ...styles.title }, { marginTop: 25 }]}>Email</Text>
            <View style={styles.inputView}>
              {/* <TextInput
                disabled={false}
                placeholder="Email"
                placeholderTextColor={colorConstant.placeholderColor}
                style={styles.input}
                editable={editFlag ? false : true}
                value={userDetail.email}
                // onChangeText={(val) =>
                //   setUserDetail({ ...userDetail, fullName: val })
                // }
              /> */}
              <Text style={styles.input}>{userDetail.email}</Text>
              {/* <Text style={styles.greenText}>Verified</Text> */}
              {/* <Image
                resizeMode="contain"
                source={imageConstant.angle}
                style={styles.img1}
              /> */}
            </View>

            <Text style={[{ ...styles.title }, { marginTop: 25 }]}>
              Password
            </Text>
            <View style={[{ ...styles.inputView }, { marginBottom: 50 }]}>
              <TextInput
                placeholder="Password"
                secureTextEntry={showpassword}
                placeholderTextColor={colorConstant.placeholderColor}
                style={styles.input}
                editable={editFlag ? false : true}
                value={userDetail.password}
                onChangeText={(val) =>
                  setUserDetail({ ...userDetail, password: val })
                }
              />
              <TouchableOpacity 
              disabled={editFlag}
              onPress={()=>setShowpassword(!showpassword)}
                style={styles.img1}
              >
              <Image
                resizeMode="contain"
                source={imageConstant.eye}
                style={{height:"100%",width:"100%"}}
              />
              </TouchableOpacity>
            </View>
          {!editFlag && (
            <CustomButton
              OnButtonPress={() => {
                editProfileHandler();
                // setEditFlag(!editFlag);
              }}
              // position={"absolute"}
              bottom={Platform.OS == "ios" ? 30 : 0}
              buttonText={"Update"}
            />
          )}
          </KeyboardAwareScrollView>
          <LoaderModal isLoading={isLoading} />

          <Modal
            animationType="slide"
            statusBarTranslucent
            visible={changePhoneNumberModal}
          >
            <SafeAreaView style={{ flex: 1 }}>
              <View style={{ flex: 1, alignItems: "center" }}>
                <TouchableOpacity
                  style={{
                    padding: 5,
                    width: "100%",
                    paddingHorizontal: moderateScale(20),
                  }}
                  onPress={() => setChangePhoneNumberModal(false)}
                >
                  <Image
                    resizeMode="contain"
                    source={require("../assets/images/purpleArrow.png")}
                    style={{
                      height: moderateScale(30),
                      width: scale(30),
                    }}
                  />
                  {/* <Text>kfgskdjfhkj</Text> */}
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: 29,
                    color: colorConstant.purple,
                    fontFamily: fontConstant.bold,
                    marginTop: moderateScale(50),
                  }}
                >
                  Change Phone Number
                </Text>
                <Image
                  source={require("../assets/images/boarding2.png")}
                  style={{ marginVertical: moderateScale(70) }}
                />
                {/* 
            <TextInput
              value={userDetail.phoneNumber}
              keyboardType="number-pad"
              style={{
                width: "80%",
                borderWidth: 1,
                borderRadius: 10,
                marginTop: moderateScale(40),
                paddingHorizontal: scale(10),
              }}
              onChangeText={(val) =>
                setUserDetail({ ...userDetail, phoneNumber: val })
              }
            /> */}

                <PhoneInput
                  // ref={phoneInput}
                  editable={false}
                  defaultValue={userDetail.phoneNumber}
                  defaultCode="IN"
                  layout="first"
                  // onChangeCountry={(code) => {
                  //   updateState({
                  //     ...iState,
                  //     countryCode: code?.callingCode?.[0],
                  //     stateFlag: code?.cca2,
                  //   });
                  // }}
                  // onChangeText={inputHandler}
                  containerStyle={{
                    width: "80%",
                    borderWidth: 1.5,
                    borderRadius: 10,
                    marginTop: 20,
                    alignSelf: "center",
                    borderColor: colorConstant.purple,
                  }}
                  textInputProps={{
                    maxLength: 13,
                    value: userDetail.phoneNumber,
                  }}
                  textContainerStyle={{
                    paddingVertical: 0,
                    borderRadius: 10,
                  }}
                />

                <TouchableOpacity
                  style={{
                    backgroundColor: colorConstant.purple,
                    width: "80%",
                    borderRadius: 10,
                    paddingVertical: moderateScale(13),
                    alignItems: "center",
                    marginTop: moderateScale(30),
                  }}
                >
                  <Text
                    style={{
                      color: colorConstant.white,
                      fontFamily: fontConstant.semi,
                      fontSize: moderateScale(15),
                    }}
                  >
                    Verifiy Number
                  </Text>
                </TouchableOpacity>
              </View>
            </SafeAreaView>
          </Modal>
        </View>
      </SafeAreaView>
    </>
  );
};

export default EditAccount;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
  },
  container: {
    backgroundColor: colorConstant.white,
  },
  headerImage: {
    width: Width,
    height: null,
    aspectRatio: Platform.OS == "ios" ? 1 / 0.562 : 1 / 0.563,
    zIndex: 1,
  },
  img: {
    width: 20,
    height: 20,
  },
  text: {
    fontSize: 32,
    fontFamily: fontConstant.bold,
    color: colorConstant.purple,
  },
  profile: {
    width: 70,
    height: 70,
  },
  edit: {
    width: 20,
    height: 20,
  },
  editTouch: {
    position: "absolute",
    bottom: 0,
    right: 0,
    padding: 5,
    backgroundColor: colorConstant.white,
    borderRadius: 20,
    // zIndex: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  title: {
    fontSize: 14,
    fontFamily: fontConstant.regular,
    color: colorConstant.grayText,
    width: "90%",
    alignSelf: "center",
  },
  input: {
    width: "90%",
    alignSelf: "center",
    fontSize: 14,
    fontFamily: fontConstant.regular,
    ...Platform.select({
      ios: {
        height: 50,
      },
      android: {
        paddingVertical: 2,
      },
    }),
  },
  img1: {
    width: 20,
    height: 20,
    position: "absolute",
    right: 0,
  },
  inputView: {
    flexDirection: "row",
    alignSelf: "center",
    alignItems: "center",
    width: "90%",
    borderBottomColor: colorConstant.grayButton,
    borderBottomWidth: 1,
  },
  greenText: {
    fontSize: 12,
    fontFamily: fontConstant.regular,
    color: colorConstant.green,
    position: "absolute",
    right: 25,
  },
});
