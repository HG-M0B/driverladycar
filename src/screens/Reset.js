import {
  View,
  Text,
  Image,
  Pressable,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  Platform,
} from "react-native";
import React from "react";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import CustomInput from "../customComponents/CustomInput";
import CustomButton from "../customComponents/CustomButton";
import CustomHeader from "../customComponents/CustomHeader";
import CustomPasswordInput from "../customComponents/CustomPasswordInput";
import { KeyboardAwareScrollView } from "@codler/react-native-keyboard-aware-scroll-view";
import { ForgetPasswordServices } from "../services/AuthServices";
import { useState } from "react";
import { checkPasswordFormat } from "../utils/HelperFunction";
import toastShow from "../utils/toastShow";
const Reset = (props) => {
  const paramData = props?.route?.params;
  console.log(paramData);
  // alert(JSON.stringify(paramData))
  const [pass, setPass] = useState({
    password: " ",
    confirmPassword: " ",
  });
  const [showpass,setShowpass]=useState(true)
  const [validpassword, setValidPassword] = useState(true);

  const forgetPasswordFunction = async () => {
    if (pass.password == " ") {
      toastShow("Enter Password", colorConstant.errorRed);
      return false;
    }
    if (pass.confirmPassword == " ") {
      toastShow("Enter Confirm password", colorConstant.errorRed);
      return;
    }
    if (pass.confirmPassword !== pass.password) {
      toastShow(" Confirm password not match", colorConstant.errorRed);
      return;
    }
    let objToSend = {
      phoneNumber:paramData?.data,
      password: pass.password,
    };

    try {
      const response = await ForgetPasswordServices(objToSend);
      console.log(response.data);
      toastShow('Reset password successfuly',colorConstant.successGreen)
      props.navigation.navigate("Login1");
    } catch (error) {
      console.log(error);
      toastShow("Something went wrong",colorConstant.successGreen)

    }
  };
  const passwordHandler = (text) => {
    let data = checkPasswordFormat(text);
    console.log(data, "dkjsbfdsjkfh");
    if (data) {
      setPass({
        ...pass,
        password: text,
      });
      setValidPassword(data);
    } else {
      setPass({
        ...pass,
        password: text,
      });
      setValidPassword(data);
    }
  };
  return (
    <SafeAreaView style={styles.main}>
      <CustomHeader navigation={props.navigation} />
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        bounces={false}
      >
        <Text style={styles.text1}>Reset Password</Text>
        <Image
          resizeMode="contain"
          style={styles.img1}
          source={imageConstant.resetPassword}
        />
        <CustomPasswordInput
          marginTop={20}
          placeholder={"Password"}
          onChangeText={passwordHandler}
          onPress={()=>setShowpass(!showpass)}
          secureTextEntry={showpass}
          // showHide={()=>setPass({...pass,showpass:!showpass})}
        />
        {/* <Text>{showpass?'ksjhfsd':'jdsfdjksfh'}</Text> */}

        {!validpassword && pass.password.length>0 &&
          <Text
            style={{
              fontSize: 10,
              fontFamily: fontConstant.regular,
              color: colorConstant.errorRed,
              width: "80%",
              marginTop: 10,
              alignSelf: "center",
            }}
          >
            Required minimum 8 length,1 Small,Capital & Special Character
          </Text>
        }

        {/* <Text>{pass.validpass ? "Hello" : "Falskl"}</Text> */}
        <CustomInput
          marginTop={"5%"}
          placeholder="Confirm Password"
          onChangeText={(val) => setPass({ ...pass, confirmPassword: val })}
          // showHide={()=>setPass({...pass,showpass:!showpass})}
        />
        {/* <Text>{pass.showpass?'jhsd':'slkdj'}</Text> */}
        <CustomButton
          buttonText={"Continue"}
          width={"80%"}
          borderRadius={10}
          marginTop={"5%"}
          OnButtonPress={forgetPasswordFunction}
          marginBottom={Platform.OS == "ios" ? 10 : 20}
        />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default Reset;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  pres: {
    alignSelf: "flex-start",
    left: "6%",
    marginTop: "7%",
  },
  text1: {
    fontSize: 29,
    color: colorConstant.purple,
    fontFamily: fontConstant.semibold,
    alignSelf: "center",
    marginTop: "3%",
  },
  img1: {
    height: 316,
    width: 160,
    alignSelf: "center",
    marginTop: "4%",
  },
});
