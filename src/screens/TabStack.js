import { View, Text } from 'react-native'
import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import PendingTrip from './PendingTrip';
import ScheduleTrip from './ScheduleTrip';
import { colorConstant, fontConstant } from '../utils/constant';
import { Width } from '../dimension/dimension';
const Tab = createMaterialTopTabNavigator();
const TabStack=(props)=> {
const {navigation,route}   = props;
 
 
  return (
    <Tab.Navigator

    screenOptions={{
      tabBarInactiveTintColor:colorConstant.grayButton,
      tabBarActiveTintColor:colorConstant.black,
      tabBarPressColor:colorConstant.grayBg,
      tabBarItemStyle: { width:Width/2},
      tabBarStyle:{
        elevation:0,
        borderBottomColor:colorConstant.grayBg,
        borderBottomWidth:2,
        activeTintColor:colorConstant.purple,
      },
      
    
    }}>
      <Tab.Screen name="PendingTrip"
        options={{
          tabBarLabel: (props) => {
            console.log("focused",props)
            return (
              <View style={{
                width: Width / 2,
                flexDirection: "row",
                paddingVertical: 12,
                alignItems: "center",
                justifyContent: "center",
              }}>
                <Text style={{
                  color : props.color,
                  fontSize: 14,
                  fontFamily: props.color == "#000000" ?  fontConstant.bold : fontConstant.regular,
                }}>PENDING</Text>
                <View style={{
                  width: Width * 0.07,
                  height: Width * 0.07,
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: (Width * 0.07) / 2,
                  marginLeft: 5,
                  backgroundColor: colorConstant.purple
                }}>
                  <Text style={{
                    color: colorConstant.white,

                  }}>11</Text>
                </View>

              </View>
            )
          }
        }}
        component={PendingTrip}
         />


      <Tab.Screen name="ScheduleTrip"
        options={{
          tabBarLabel: (props) => {
            console.log("focused----",props)
            return (
              <View style={{
                width: Width / 2,
                flexDirection: "row",
                paddingVertical: 12,
                alignItems: "center",
                justifyContent: "center"
              }}>
                <Text style={{
                   color : props.color,
                  fontSize: 14,
                  fontFamily: props.color == "#000000" ?  fontConstant.bold : fontConstant.regular,
                }}>SCHEDULE</Text>
                <View style={{
                  width: Width * 0.07,
                  height: Width * 0.07,
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: (Width * 0.07) / 2,
                  marginLeft: 5,
                  backgroundColor: colorConstant.purple
                }}>
                  <Text style={{
                    color: colorConstant.white,

                  }}>11</Text>
                </View>
              </View>
            )
          }
        }}
        component={ScheduleTrip} />



      
    </Tab.Navigator>
  )
}

export default TabStack;

  