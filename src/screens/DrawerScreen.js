import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  StyleSheet,
} from "react-native";
import React from "react";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import { Height, Width } from "../dimension/dimension";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch } from "react-redux";
import { actions } from "../redux/reducers";
import { useState } from "react";
import { DriverLooactionServices, getUserProfileServices } from "../services/DriverServices";
import { useEffect } from "react";
import { moderateScale } from "react-native-size-matters";
import { useIsFocused } from "@react-navigation/native";
import toastShow from "../utils/toastShow";

const DrawerScreen = (props) => {

  const [driverImage,setDriverImage]=useState("")
  const isFocoused=useIsFocused()

    const disptach = useDispatch();

  const logoutHandler = async () => {
    try {
      onToggle()
      disptach(actions.setIntroStatus('auth'));
      const keys = await AsyncStorage.getAllKeys();
      await AsyncStorage.multiRemove(keys);

    } catch (error) {
        console.log(error)
    }
  };

  const getUserProfileFunction = async () => {
    try {
      const response = await getUserProfileServices();
      let res =await response.data.userData.profilePic
      console.log(response.data.userData.profilePic, "lllllll ehat we cah do");
       setDriverImage(res);
      // alert(driverImage)
      // alert(userDetail.profileImage,image)
    } catch (error) {
      console.log('',error)
      // alert('dskjhfdkj')
    }
  };


  // alert('djshfgjsdhgfsdhj')

  const onToggle = async () => {

    let objToSend={
        is_online:false,
        lat:"0",
        long:"0"
    }
    try {
      const response = await DriverLooactionServices(objToSend);
      console.log(response.data)
      console.log(objToSend,'kdshfkjsdhfjksfhkjsdhfksjdhf')
      // setIsLoading(false)
      // props.navigation.goBack()
      console.log(response.data)
      // await AsyncStorage.setItem("token", response.data.userData.jwtToken);
      // driverProfileFunction()

    } catch (error) {
      console.log(objToSend);
      // setIsLoading(false)
      toastShow("Something went wrong plase try again",)
    }
  };

  useEffect(() => {
    let mount = true
    if(mount){
      getUserProfileFunction()
    }
    return () => {
      mount=false
    }
  }, [isFocoused])
  

  return (
    <SafeAreaView style={styles.main}>
      <ImageBackground
        resizeMode="contain"
        style={{
          aspectRatio: 1 / 1.229,
          alignItems: "center",
        }}
        source={imageConstant.drawerbg}
      >
        <TouchableOpacity
          style={{
            position: "absolute",
            padding: 5,
            paddingHorizontal: 10,
            top: 20,
            right: 20,
          }}
          onPress={() => props.navigation.navigate("Profile")}
        >
          <Text
            style={{
              fontSize: 16,
              fontFamily: fontConstant.semi,
              color: colorConstant.white,
            }}
          >
            My Profile
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            marginTop: Height * 0.1,
          }}
          activeOpacity={1}
          onPress={() => props.navigation.navigate("Profile")}
        >
          <Image
            // resizeMode=""
            style={{
              width:moderateScale(100),
              height:moderateScale(100),
              borderRadius:moderateScale(100),
              overflow:'hidden'
            }}
            source={{uri:driverImage}}
          />
        </TouchableOpacity>
      </ImageBackground>

      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("EditAccount")}
          style={{
            paddingVertical: 13,
            alignItems: "center",
            borderBottomWidth: 1,
            borderBottomColor: colorConstant.grayBg,
          }}
          activeOpacity={0.8}
        >
          <Text
            style={{
              fontSize: 14,
              fontFamily: fontConstant.regular,
              color: colorConstant.black,
            }}
          >
            My Info
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => props.navigation.navigate("Payment")}
          style={{
            paddingVertical: 13,
            alignItems: "center",
            borderBottomWidth: 1,
            borderBottomColor: colorConstant.grayBg,
          }}
          activeOpacity={0.8}
        >
          <Text
            style={{
              fontSize: 14,
              fontFamily: fontConstant.regular,
              color: colorConstant.black,
            }}
          >
            Payments/Earnings
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => props.navigation.navigate("AddVehicles")}
          style={{
            paddingVertical: 13,
            alignItems: "center",
            borderBottomWidth: 1,
            borderBottomColor: colorConstant.grayBg,
          }}
          activeOpacity={0.8}
        >
          <Text
            style={{
              fontSize: 14,
              fontFamily: fontConstant.regular,
              color: colorConstant.black,
            }}
          >
            Vehicles Details
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => props.navigation.navigate("History")}
          style={{
            paddingVertical: 13,
            alignItems: "center",
            borderBottomWidth: 1,
            borderBottomColor: colorConstant.grayBg,
          }}
          activeOpacity={0.8}
        >
          <Text
            style={{
              fontSize: 14,
              fontFamily: fontConstant.regular,
              color: colorConstant.black,
            }}
          >
            Trip History
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => props.navigation.navigate("CommentSection")}
          style={{
            paddingVertical: 13,
            alignItems: "center",
            borderBottomWidth: 1,
            borderBottomColor: colorConstant.grayBg,
          }}
          activeOpacity={0.8}
        >
          <Text
            style={{
              fontSize: 14,
              fontFamily: fontConstant.regular,
              color: colorConstant.black,
            }}
          >
            Comments
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => props.navigation.navigate("HelpCenter")}
          style={{
            paddingVertical: 13,
            alignItems: "center",
            borderBottomWidth: 1,
            borderBottomColor: colorConstant.grayBg,
          }}
          activeOpacity={0.8}
        >
          <Text
            style={{
              fontSize: 14,
              fontFamily: fontConstant.regular,
              color: colorConstant.black,
            }}
          >
            Help
          </Text>
        </TouchableOpacity>
      </ScrollView>

      <TouchableOpacity style={styles.row} onPress={logoutHandler}>
        <Text style={styles.logout}>Logout</Text>
        <Image
          source={imageConstant.logout}
          style={styles.img}
          resizeMode="contain"
        />
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default DrawerScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  row: {
    flexDirection: "row",
    width: "50%",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "space-between",
    position: "absolute",
    bottom: 40,
  },
  logout: {
    fontSize: 15,
    fontFamily: fontConstant.bold,
    color: colorConstant.black,
  },
  img: {
    width: 21,
    height: 21,
  },
});
