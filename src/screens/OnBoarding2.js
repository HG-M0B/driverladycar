import { Image, Platform, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, View } from 'react-native'
import React,{useEffect,useState,useRef}from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { Height } from '../dimension/dimension'
import CustomButton from '../customComponents/CustomButton'
import { heightPercentageToDP } from '../utils/responsiveFIle'
import PhoneInput from "react-native-phone-number-input";
import LinearGradient from 'react-native-linear-gradient';

let initialState = {
    number:"",
    formattedNumber:0,
    valid:false,
    showMessage:false
}

const OnBoarding2 = (props) => {
    const [iState,updateState] =  useState(initialState);
    const phoneInput = useRef(null);
    const {navigation} =  props;
    const {
    number,
    valid,
    showMessage,
    formattedNumber
} =  iState;
// useEffect(() => {
//   if(formattedNumber.length === 13)
//   {
      
//   }
// }, [formattedNumber]);




  return (
      <SafeAreaView style={styles.main}>
          <ScrollView
          style={{
          }}
              bounces={false}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                  flexGrow:1,
                  alignItems: "center",


              }}>

              <Text style={styles.text1}>Enter your phone number</Text>
              <PhoneInput
                  ref={phoneInput}
                  defaultValue={number}
                  layout="first"
                  onChangeText={(text) => {
                      updateState({ ...iState, value: text })
                  }}

                  onChangeFormattedText={(text) => {
                      updateState({ ...iState, formattedNumber: text })
                  }}

                  containerStyle={{
                      width: "80%",
                      borderWidth: 1.5,
                      borderRadius: 10,
                      marginTop: 20,
                      borderColor: colorConstant.purple

                  }}
                  textInputProps={{
                      maxLength: 13
                  }}
                  textContainerStyle={{
                      paddingVertical: 0,
                      borderRadius: 10
                  }}
              />

              <Image
                  source={imageConstant.boarding2}
                  style={styles.img}
                  resizeMode='contain'
              />

         
              <View style={{
                  width: "90%",
                  position:"absolute",
                  bottom:20,
                  alignItems: "center",
              }}>
                  <Text style={styles.text2}>Continue to recieve an SMS confirmation and accept</Text>
                  <CustomButton
                      buttonText={'Continue'}
                      OnButtonPress={() => navigation.navigate("OtpVerify")}
                  />
              </View>
          </ScrollView>
         
       
      </SafeAreaView>
   
  )
}

export default OnBoarding2

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white,
       
       
    },
    img:{
        marginTop:Height*0.12,
        width:200,
        height:null,
        aspectRatio:1/1,
        // backgroundColor:"green"
    },
    text1:{
        fontSize:24,
        fontFamily:fontConstant.semi,
        color:colorConstant.purple,
        marginTop:80,
    },
    text2:{
        fontSize:12,
        fontFamily:fontConstant.regular,
        color:colorConstant.textGary,
        marginBottom:10

    },
    buttonView:{
        position:"absolute",
        bottom:20
    }

})