import { View, Text, Image ,TouchableOpacity} from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import CustomHeader from '../customComponents/CustomHeader'
import { useState } from 'react'
import CustomButton from '../customComponents/CustomButton'

const Withdraw = (props) => {
const [select,setSelected] = useState("");
    const data = [
        {
            id:1,
            title:"CHASE BANK"
        },
        {
            id:2,
            title:"CHASE BANK"
        },
        {
            id:3,
            title:"CHASE BANK"
        }
    ]
    console.log("select",select)
  return (
    <View style={{
        flex:1,
        backgroundColor:colorConstant.white
    }}>
        <CustomHeader 
        color={colorConstant.purple}
        headerText={'Withdraw Money'}
        navigation={props.navigation}
        />

        <Text style={{
            fontSize:17,
            fontFamily:fontConstant.regular,
            color:colorConstant.black,
            width:"90%",
            alignSelf:"center",
            marginTop:40
        }}>Amount</Text>
        <Text style={{
            fontSize:25,
            fontFamily:fontConstant.bold,
            color:colorConstant.black,
            width:"90%",
            alignSelf:"center"
        }}>$168.89</Text>

          <View style={{
              width: "90%",
              borderWidth: 0.3,
              alignSelf: "center",
              color: colorConstant.black,
              opacity: 0.2,
              marginTop: 25

          }}>
     
          </View>

          <Text style={{
              fontSize: 17,
              fontFamily: fontConstant.regular,
              color: colorConstant.black,
              width: "90%",
              alignSelf: "center",
              marginTop:20
          }}>Select Bank</Text>

          <View style={{
              marginTop:20
          }}>

              {
                  data.map((item, index, data) => {
                      return (
                          <>
                              <View
                                  key={index + 1}
                                  style={{
                                      width: "90%",
                                      alignSelf: "center",
                                      flexDirection: "row",
                                      alignItems: "center",
                                      marginTop: 10
                                  }}>
                                  <Image
                                      resizeMode='contain'
                                      source={imageConstant.bank}
                                      style={{
                                          width: 30,
                                          height: 30
                                      }}

                                  />
                                  <Text style={{
                                      fontSize: 16,
                                      fontFamily: fontConstant.semi,
                                      color: colorConstant.black,
                                      marginLeft: 10
                                  }}>{item.title}</Text>

                                  <TouchableOpacity
                                      onPress={() => { setSelected(index.toString())}}
                                      style={{
                                          width: 20,
                                          height: 20,
                                          borderRadius: 10, borderWidth: 2,
                                          borderColor: colorConstant.purple,
                                          backgroundColor:select == index.toString() ? colorConstant.purple : colorConstant.white,
                                          position: "absolute",
                                          right: 0
                                      }}>

                                  </TouchableOpacity>
                              </View>
                              {
                                  data.length - 1 != index && (
                                      <View style={{
                                          width: "90%",
                                          borderWidth: 0.3,
                                          alignSelf: "center",
                                          color: colorConstant.black,
                                          opacity: 0.2,
                                          marginVertical: 10

                                      }}>
                                      </View>
                                  )
                              }

                          </>
                      )
                  })
              }
          </View>

          <CustomButton
          OnButtonPress={()=>props.navigation.navigate("Success")}
              position={"absolute"}
              bottom={30}
              buttonText={'Withdraw'} />
    </View>
  )
}

export default Withdraw