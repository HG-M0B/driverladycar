import { Image, Keyboard, SafeAreaView, StyleSheet, Text, TextInput, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import CustomHeader from '../customComponents/CustomHeader'
import CustomDropDown from '../customComponents/CustomDropDown'
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view'
import { Height } from '../dimension/dimension'
import CustomButton from '../customComponents/CustomButton'
import { useEffect,useState } from 'react'

const Issue = (props) => {
const [padding,setPadding] = useState()
    useEffect(() => {
        const showSubscription = Keyboard.addListener("keyboardDidShow", (e) => {
            if (Platform.OS === "android") {
                console.log("e.endCoordinates.height",e.endCoordinates.height)
                setPadding(278.9090881347656)
            }
        });
        const hideSubscription = Keyboard.addListener("keyboardDidHide", () => {
            if (Platform.OS === "android") {
                setPadding(10)
            }
        });
      
        return () => {
            showSubscription.remove();
            hideSubscription.remove();
        };
      }, []);
  return (
    <SafeAreaView style={styles.main}>
        <CustomHeader
        headerText={'Choose an Issue'}
        color={colorConstant.purple}
        navigation = {props.navigation}
        />

        <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        bounces={false}
        contentContainerStyle={{
            paddingBottom:padding
        }}>

            <View style={{
                marginTop:10,
                width:"90%",
                alignSelf:"center",
                flexDirection:"row"
            }}>
                <Image
                resizeMode='contain'
                style={{
                    width:40,
                    height:40
                }}
                source={imageConstant.profile4}
                />
                <View style={{
                    marginLeft:10
                }}>
                    <Text style={{
                            fontSize:15,
                            fontFamily:fontConstant.semi,
                            color:colorConstant.black,
                        }}>SONIA</Text>
                    <View style={{
                        flexDirection:"row"
                    }}>
                        <Image
                        resizeMode='contain'
                        style={{
                            width:16,
                            height:16
                        }}
                        source={imageConstant.star}/>
                        <Text style={{
                            fontSize:15,
                            fontFamily:fontConstant.regular,
                            color:colorConstant.black,
                        }}>4.9</Text>
                    </View>
                </View>
            </View>

            <Text style={{
                fontSize:12,
                fontFamily:fontConstant.regular,
                color:colorConstant.black,
                width:"90%",
                alignSelf:"center",
                marginTop:20
            }}>Pickup Time</Text>
            <Text style={{
                fontSize:14,
                fontFamily:fontConstant.bold,
                color:colorConstant.black,
                width:"90%",
                alignSelf:"center",
                marginTop:6
            }}>Sep 19,  12:25 PM</Text>


              <View style={{
                  width: "90%",
                  borderWidth: 0.3,
                  alignSelf: "center",
                  color: colorConstant.black,
                  opacity: 0.2,
                  marginTop: 25

              }}>
              </View>

              <View style={{
                  width: "90%",
                  alignSelf: "center",
                  flexDirection: "row",
                  marginTop: 20
              }}>

                  <View style={{
                      // backgroundColor:"red",
                      justifyContent: "center",
                      alignItems: "center"
                  }}>
                      <Image
                          resizeMode='contain'
                          style={{
                              width: 15,
                              height: 21

                          }}
                          source={imageConstant.locationred} />
                      <Image
                          resizeMode='contain'
                          style={{
                              width: 10,
                              height: 38

                          }}
                          source={imageConstant.line} />
                      <Image
                          resizeMode='contain'
                          style={{
                              marginVertical: 5,
                              width: 15,
                              height: 21

                          }}
                          source={imageConstant.locationgreen} />
                  </View>

                  <View style={{
                      marginLeft: 10,
                      justifyContent: "space-between",
                      height: 100,
                  }}>
                      <View>
                          <Text
                              numberOfLines={1}
                              style={{
                                  fontSize: 14,
                                  fontFamily: fontConstant.bold,
                                  color: colorConstant.black,
                                  width: "90%",
                              }}>Nashville, TN 37214, USA</Text>
                          <Text style={{
                              fontSize: 12,
                              fontFamily: fontConstant.regular,
                              color: colorConstant.black
                          }}>Sep 19  12:25 PM</Text>
                      </View>


                      <View>
                          <Text
                              numberOfLines={1}
                              style={{
                                  fontSize: 14,
                                  fontFamily: fontConstant.bold,
                                  color: colorConstant.black,
                                  width: "90%",
                              }}>2720 Old Lebanon Rd, Nashiville, TN, 37214, US</Text>
                          <Text style={{
                              fontSize: 12,
                              fontFamily: fontConstant.regular,
                              color: colorConstant.black
                          }}>Sep 19  12:25 PM</Text>
                      </View>
                  </View>

              </View>

              <View style={{
                  width: "90%",
                  borderWidth: 0.3,
                  alignSelf: "center",
                  color: colorConstant.black,
                  opacity: 0.2,
                  marginTop: 25

              }}>
              </View>

              <CustomDropDown
              placeholder="Reason"
              onChange={(item) => console.log(item.lable)}
              />
                <TextInput
                  multiline={true}
                  placeholder='Description'
                  placeholderTextColor={colorConstant.grayButton}
                  style={styles.description}
              />
              <CustomButton
                  OnButtonPress={() => { props.navigation.goBack() }}
                  width={'80%'}
                  bottom={30}
                  buttonText={'SUBMIT'} />
        </KeyboardAwareScrollView>
      
    </SafeAreaView>
  )
}

export default Issue

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    description: {
        fontSize:14,
        fontFamily:fontConstant.regular,
        color:colorConstant.black,
        marginTop:30,
        width: "80%",
        alignSelf: "center",
        borderColor:colorConstant.purple,
        borderWidth:1.5,
        height: Height * 0.20,
        backgroundColor: colorConstant.white,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        ...Platform.select({
            ios: {
                paddingTop:13,
                padding:13
            },
            android: {
                textAlignVertical: "top",
                padding:20,
            }
        })
    }
})