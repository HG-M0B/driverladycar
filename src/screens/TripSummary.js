import { Image, StyleSheet, Text, View,ScrollView, SafeAreaView,TouchableOpacity, PermissionsAndroid, TextInput, BackHandler, Platform } from 'react-native'
import React ,{useMemo,useState,useRef,useCallback}from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import CustomMapView from '../customComponents/CustomMapView'
import { Height, Width } from '../dimension/dimension'
import CustomButton from '../customComponents/CustomButton'
import BottomSheet ,{BottomSheetTextInput}from '@gorhom/bottom-sheet';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { useEffect } from 'react'
import { useFocusEffect } from '@react-navigation/native'

let initialState = {
    toggle : false,
    open:true,
    i:0
}
const TripSummary = (props) => {
    const [iState,updateState] =  useState(initialState);
    const sheetRef = useRef(null);
      const {
        toggle ,
        open,
        i
    } =  iState;
    const snapPointsAndroid = useMemo(() => ['55%'], []);
    const snapPointsIos = useMemo(() => ['58%'], []);
    const handleSheetChanges = useCallback((index) => {
        sheetRef.current?.snapToIndex(index);
        updateState({...iState,open:false,i:index})
        
      }, []);

      useEffect(()=>{
          setTimeout(()=>{
            // props.navigation.navigate("TripRequest")
          },4000)
         
      },[])

      useFocusEffect(
        React.useCallback(() => {
          const backAction = () => {
            console.log("called")
            props.navigation.navigate("EndDrive")
            return true;
          };
      
          const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
          );
      
          return () => backHandler.remove();
        }, [])
      );
  return (
      <SafeAreaView style={styles.main}>
       
          <View style={{
              ...Platform.select({
                  ios:{  height : Height * 0.40},
                  android:{
                    height : Height * 0.65
                  }
                
              })
          }}>

              <CustomMapView
                  height={Platform.OS == "ios"  ? Height * 0.40 : Height * 0.65}
              />

              <View style={styles.row}>
                  <TouchableOpacity
                      onPress={() => props.navigation.toggleDrawer()}
                  >
                      <Image
                          resizeMode='contain'
                          source={imageConstant.drawer}
                          style={styles.img} />
                  </TouchableOpacity>
                  <TouchableOpacity
                      activeOpacity={1}
                      onPress={() => props.navigation.navigate("Profile")} >
                      <Image
                          resizeMode='contain'
                          source={imageConstant.profile1}
                          style={{
                              width: 35,
                              height: 35
                          }} />
                  </TouchableOpacity>

              </View>

              <TouchableOpacity
                  onPress={() => alert("Recenter")}
                  activeOpacity={0.8}
                  style={{
                      position: "absolute",
                      right: 0,
                      bottom:20
                  }}>
                  <Image
                      resizeMode='contain'
                      source={imageConstant.location}
                      style={{
                          width: 55,
                          height: 55
                      }}
                  />
              </TouchableOpacity>
          </View>




          <BottomSheet
              index={0}
              ref={sheetRef}
              snapPoints={Platform.OS == 'ios' ? snapPointsIos : snapPointsAndroid}
              enableContentPanningGesture={true}
              keyboardBlurBehavior={"restore"}

          >
              <View style={{
                  backgroundColor: colorConstant.white,
                
              }}>


                  <Text style={styles.text}>Summary of your trip :</Text>
                  <View style={{
                      flexDirection: "row",
                      width: "90%",
                      alignItems: "center",
                      alignSelf: "center",
                      marginTop: 20
                  }}>
                      <Image
                          resizeMode='contain'
                          source={imageConstant.car}
                          style={{
                              width: 50,
                              height: 50,
                              tintColor: colorConstant.black,
                          }} />


                      <View style={{
                          alignItems: "center",
                          marginHorizontal: Width * 0.035
                      }}>
                          <Text
                              style={{
                                  fontSize: 14,
                                  fontFamily: fontConstant.regular,
                                  color: colorConstant.black
                              }}>4 min</Text>
                          <Image
                              resizeMode='contain'
                              source={imageConstant.arrow}
                              style={{
                                  width: Width * 0.15,
                                  height: 10,
                              }} />
                          <Text
                              style={{
                                  fontSize: 8,
                                  fontWeight: "400",
                                  fontFamily: fontConstant.regular,
                                  color: colorConstant.black
                              }}
                          >Your Location</Text>
                      </View>



                      <Image
                          resizeMode='contain'
                          source={imageConstant.locationgreen}
                          style={{
                              width: 30,
                              height: 30
                          }} />

                      <View style={{
                          alignItems: "center",
                          marginHorizontal: Width * 0.035
                      }}>
                          <Text style={{
                              fontSize: 14,
                              fontFamily: fontConstant.regular,
                              color: colorConstant.black
                          }}>7 min</Text>
                          <Image
                              resizeMode='contain'
                              source={imageConstant.arrow}
                              style={{
                                  width: Width * 0.15,
                                  height: 10,
                              }} />
                          <Text
                              style={{
                                  fontSize: 8,
                                  fontWeight: "400",
                                  fontFamily: fontConstant.regular,
                                  color: colorConstant.black
                              }}
                          >Destination</Text>
                      </View>


                      <Image
                          resizeMode='contain'
                          source={imageConstant.locationred}
                          style={{
                              width: 30,
                              height: 30
                          }} />
                      <View>


                      </View>


                      <Text style={{
                          fontSize: 24,
                          fontFamily: fontConstant.bold,
                          color: colorConstant.black,
                          position: "absolute",
                          right: 0
                      }}>$9</Text>

                  </View>

                  <View style={{
                      flexDirection: "row",
                      alignSelf: "center",
                      marginTop: 30,
                      justifyContent: "flex-start",
                      alignItems: "center",
                      width: "90%"
                  }}>
                      <View style={{
                          backgroundColor: "#FFFFFF",
                          marginRight: 10,
                          shadowColor: "#000",
                          shadowOffset: {
                              width: 0,
                              height: 6,
                          },
                          shadowOpacity: 0.39,
                          shadowRadius: 8.30,

                          elevation: 13,
                          borderRadius: 50 / 2
                      }} >
                          <Image
                              resizeMode='contain'
                              source={imageConstant.profile4}
                              style={{
                                  width: 50,
                                  height: 50
                              }} />
                      </View>
                      <View>
                          <Text style={{
                              fontSize: 16,
                              fontFamily: fontConstant.bold,
                              color: colorConstant.black
                          }}>LUCY</Text>
                          <View style={{
                              flexDirection: "row",
                              alignItems: "center"
                          }}>
                              <Image
                                  resizeMode='contain'
                                  source={imageConstant.star}
                                  style={{
                                      width: 15,
                                      height: 15
                                  }} />
                              <Text style={{
                                  fontSize: 16,
                                  fontFamily: fontConstant.regular,
                                  color: colorConstant.black
                              }}>3.8</Text>
                          </View>
                      </View>
                      <TouchableOpacity
                      onPress={()=>props.navigation.navigate("Report")}
                      activeOpacity={0.8}
                          style={{
                              position: "absolute",
                              right: 0
                          }}>
                          <Text style={{
                              fontSize: 16,
                              fontFamily: fontConstant.regular,
                              color: colorConstant.purple
                          }}>Report this passenger</Text>
                      </TouchableOpacity>
                  </View>


                  <View style={{
                      width: "90%",
                      alignSelf: "center",
                      marginTop: 20
                  }}>
                      <Text style={{
                          fontSize: 16,
                          fontFamily: fontConstant.semi,
                          color: colorConstant.purple
                      }}>Rate your passenger :</Text>
                      <Rating
                          style={{
                              alignSelf: "flex-start",
                              marginTop: 5
                          }}
                          jumpValue={1}
                          count={5}
                          imageSize={20}
                      />
                  </View>

                  <View style={{
                      flexDirection: "row",
                      width: "90%",
                      alignSelf: "center",
                      alignItems: "center",
                      marginTop: 20
                  }}>
                      <Image
                          resizeMode='contain'
                          source={imageConstant.profile1}
                          style={{
                              width: 50,
                              height: 50
                          }} />
                      <View style={{
                          width: "90%"
                      }}>

                    <BottomSheetTextInput 
                    // multiline={true}
                    numberOfLines={1}
                    placeholderTextColor={colorConstant.placeholderColor}
                   placeholder='Add comments...'
                    style={{
                        width: "85%",
                        marginLeft:7,
                        borderBottomColor: colorConstant.grayText,
                        borderBottomWidth: 0.7,
                        paddingVertical: 6
                    }} />
                        

                      </View>

                      
                  </View>
                  <CustomButton
                      OnButtonPress={() => props.navigation.navigate("TripRequest")}
                      marginVertical={Platform.OS =='ios' ?2:20}
                      buttonText={'OK'}
                      width={'30%'} />
              </View>
          </BottomSheet>
       
      </SafeAreaView>
    
  )
}

export default TripSummary

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    row:{
        flexDirection:"row",
        width:"93%",
        alignSelf:"center",
        justifyContent:"space-between",
        position:"absolute",
        top:20
    },
    row1:{
        flexDirection:"row",
        marginTop:20,
        width:"90%",
        alignSelf:"center",
        alignItems:"center",
    },
    img:{
        width:35,
        height:35
    },
  
    img1:{
        width:15,
        height:15
    },
    text:{
        fontSize:18,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        marginLeft:20
    },
    text1:{
        fontSize:10,
        fontFamily:fontConstant.semi,
        color:colorConstant.black,
        position:"absolute",
        top:-10
    }
   
   
})