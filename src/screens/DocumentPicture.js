import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  PermissionsAndroid,
  Platform,
  BackHandler,
  Modal,
} from "react-native";
import React from "react";
import CustomHeader from "../customComponents/CustomHeader";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import { heightPercentageToDP } from "../utils/responsiveFIle";
import CustomButton from "../customComponents/CustomButton";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { useFocusEffect } from "@react-navigation/native";
import { useRef } from "react";
import { Height } from "../dimension/dimension";
import { useState } from "react";
import { useEffect } from "react";
import ImageUploadModal from "../customComponents/ImageUploadModal";
import axios from "axios";
import { uploadImageServices } from "../services/uploadImageServices";
import { useSelector } from "react-redux";
import LoaderModal from "../customComponents/LoaderModal";
import toastShow from "../utils/toastShow";
import { moderateScale, scale } from "react-native-size-matters";
import { SafeAreaView } from "react-native-safe-area-context";

const initialState = {
  licenseFront: "",
  licenseBack: "",
  registerFront: "",
  registerBack: "",
  button: "",
};
const DocumentPicture = (props) => {
  // define all state here


  const [iState, updateState] = useState(initialState);
  const [showUploadModal, setShowUploadModal] = useState(false);
  const [modalShowFor, setModalShowFor] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [licenseModal, setLicenseModal] = useState(false)
  const [registerModal, setRegisterModal] = useState(false)
  const [sucessModal,setSucessModal]=useState(false)
  const [errorModal,setErrorModal]=useState(false)

  const { licenseFront, licenseBack, registerFront, registerBack } = iState;

  //image picker code
  const requestCameraPermission = async () => {
    if (Platform.OS === "android") {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: "Camera Permission",
            message: "App needs camera permission",
          }
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };
  const requestExternalWritePermission = async () => {
    if (Platform.OS === "android") {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: "External Storage Write Permission",
            message: "App needs write permission",
          }
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert("Write permission err", err);
      }
      return false;
    } else return true;
  };

  // region for Driver License images
  const getImageUrl = async (type) => {
    // alert(type)
    // console.log(type);
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      // console.log("access-->", isCameraPermitted, isStoragePermitted);
      let options = {
        mediaType: "photo",
        cameraType: "back",
        saveToPhotos: true,
        quality: 5,
        maxWidth: 500,
        maxHeight: 500,
        quality: 0.9,
        //  includeBase64: true,
        noData: true,
      };
      await launchCamera(options, (response) => {
        if (response.didCancel) {
          console.log("Cancelled");
        } else if (response.error) {
          console.log("Error", response.errorMessage);
        } else {
          console.log("Response-->", response?.assets?.[0]?.uri);
          let reqData = {
            uri: response?.assets?.[0]?.uri,
          };
          let formDataReq = new FormData();
          let data = {
            uri: response?.assets?.[0]?.uri,
            type: 'image/jpeg',
            name: response?.assets?.[0]?.fileName,
          };
          formDataReq.append("file", data);
          if (type == "Licence") {
              getImageData(formDataReq?._parts[0][1]);
          } else {
            getImageData2(formDataReq?._parts[0][1]);
          }
        }
      });
    }
  };

  // #region for upload image in server
  const getImageData = async (imageName) => {
    console.log(imageName,'000900090909090909')
    let finalFormData = new FormData();
    finalFormData.append("fileName", imageName);
    setIsLoading(true);
    try {
      const response = await uploadImageServices(finalFormData);
      // if(Platform.OS=='ios'){
      //   toastShow('Image Upload successfuly', colorConstant.successGreen)
      // }
      // alert('skjhgjkdshfksdjhf')
      setSucessModal(true)
      setTimeout(() => {
        setSucessModal(false)
      }, 1000);

      // toastShow("Image upload successfuly ", colorConstant.successGreen);
      // console.log(response.data, "image url in here");
      if (licenseFront == "") {
        setSucessModal(true)
        updateState({ ...iState, licenseFront: response.data.data });
        // toastShow("Image upload successfuly ", colorConstant.successGreen);
        // setSucessModal(false)
        // alert('hello')
        setIsLoading(false);
        // setSucessModal(false)

      } else {
        updateState({ ...iState, licenseBack: response.data.data });
        // toastShow("Image upload successfuly ", colorConstant.errorRed);
        setIsLoading(false);
        setLicenseModal(false)
      }

    } catch (error) {
      // console.log(error,'this is error console what is problem find we can do it');
      // setIsLoading(false);
      // toastShow("Image upload failed please try again", colorConstant.errorRed);
      setErrorModal(true)
      setIsLoading(false);
      setTimeout(() => {
        setErrorModal(false)
      }, 1000);
    }
  };

  // console.log('====================================0w7e78wqe');
  // console.log(initialState);
  // console.log('askldjklasjdlkdlk====================================');

  const getImageData2 = async (imageName) => {
    let finalFormData = new FormData();
    finalFormData.append("fileName", imageName);
    setIsLoading(true);
    try {
      const response = await uploadImageServices(finalFormData);
      if(Platform.OS=='ios'){
        toastShow('Image Upload successfuly', colorConstant.successGreen)
      }
      setSucessModal(true)

      setTimeout(() => {
        setSucessModal(false)
      }, 1000);
      // console.log(response.data);
      if (registerFront == "") {
        updateState({ ...iState, registerFront: response.data.data });
        // toastShow("Image upload successfuly ", colorConstant.successGreen);
        setIsLoading(false);
      } else {
        updateState({ ...iState, registerBack: response.data.data });
        // toastShow("Image upload successfuly ", colorConstant.successGreen);
        setIsLoading(false);
        setRegisterModal(false)
      }
    } catch (error) {
      setErrorModal(true)
      setIsLoading(false);
      setTimeout(() => {
        setErrorModal(false)
      }, 1000);
      toastShow("Image upload failed please try again", colorConstant.errorRed);
      // console.log(error,'problem in get image function find the problemand solve this problem');
    }
  };

  const uploadLicense = (data) => {
    console.log("got license req data->", data);

    if (licenseFront == "") {
      updateState({ ...iState, licenseFront: data.uri });
    } else {
      updateState({ ...iState, licenseBack: data.uri });
    }
  };

  const uploadRegister = (data) => {
    console.log("got register req data->", data);
    if (insurance == "") {
      updateState({ ...iState, insurance: data.uri });
    } else {
      updateState({ ...iState, register: data.uri });
    }
  };

  // region use Effect Area

  useFocusEffect(
    React.useCallback(() => {
      const backAction = () => {
        BackHandler.exitApp();
        return true;
      };

      const backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        backAction
      );

      return () => backHandler.remove();
    }, [])
  );

  useEffect(() => {
    if (registerBack && registerFront && licenseBack && licenseFront) {
      // AdditionalInfo
      props.navigation.navigate("SecurityNumber", {
        documentsData: {
          licenseFront,
          licenseBack,
          registerFront,
          registerBack,
        },
      });
    }
  }, [registerBack, registerBack, licenseBack, licenseBack]);

  // console.log("iState-->", iState, "sakjhdaksjdh");
  return (
    <View style={styles.main}>
            <LoaderModal isLoading={isLoading} />

      {/* <CustomHeader
              img={imageConstant.back}
              navigation={props.navigation}
          />
   */}

      <View style={styles.headingView}>
        <Text style={styles.text1}>
          Take a picture of {"\n"} your ID documents  
        </Text>
        <TouchableOpacity
          style={{
            position: "absolute",
            right: 0,
            bottom: 3,
          }}
        >
          <Image source={imageConstant.info} style={styles.img} />
        </TouchableOpacity>
      </View>

      <Image
        resizeMode="contain"
        source={imageConstant.license}
        style={styles.img2}
      />
      <View
        style={{
          width: "100%",
          position: "absolute",
          bottom: Platform.OS == "ios" ? 30 : 30,
        }}
      >
        <CustomButton
          buttonText={"Driver's Licence"}
          marginTop={25}
          width={"85%"}
          rImage={
            licenseBack !== "" && licenseFront !== ""
              ? imageConstant.right
              : null
          }
          OnButtonPress={() => setLicenseModal(true)}
        // OnButtonPress={() => {setShowUploadModal(true);setModalShowFor("Licence")}}
        // OnButtonPress={() => getImageUrl("Licence")}
        />
        <CustomButton
          buttonText={"Vehicle Registration"}
          marginTop={25}
          width={"85%"}
          // OnButtonPress={() => {setShowUploadModal(true);setModalShowFor("Vehicle Registration")}}

          OnButtonPress={() => setRegisterModal(true)}
          rImage={
            registerFront !== "" && registerBack !== ""
              ? imageConstant.right
              : null
          }
        // OnButtonPress={()=>props.navigation.navigate("SecurityNumber")}
        />
      </View>
      {/* <ImageUploadModal
        modalShowFor={true}
        setModalShowFor={setModalShowFor}
        showUploadModal={showUploadModal}
        setShowUploadModal={setShowUploadModal}
      /> */}



      {
        //#region  image upload modal;

        // region for license fort and back image

        <Modal visible={licenseModal} statusBarTranslucent>
          {/* <SafeAreaView style={styles.mainContainer}> */}
          <View style={styles.container}>
          <Modal transparent visible={sucessModal}>
        <SafeAreaView style={{}}>
        <View style={{ width: scale(230), backgroundColor: colorConstant.successGreen, paddingVertical: moderateScale(10), alignItems: "center", alignSelf: "center", marginTop: moderateScale(20), borderRadius: 10 }}>
          <Text style={{color:colorConstant.white,fontSize:moderateScale(15)}}>Image upload successful</Text>
        </View>
        </SafeAreaView>
      </Modal>


      <Modal transparent visible={errorModal}>
        <SafeAreaView>
        <View style={{ width: scale(250), backgroundColor: colorConstant.errorRed, paddingVertical: moderateScale(10), alignItems: "center", alignSelf: "center", marginTop: moderateScale(20), borderRadius: 10 }}>
          <Text style={{color:colorConstant.white,fontSize:moderateScale(15)}}>Image upload failed please try again</Text>
        </View>
        </SafeAreaView>
      </Modal>
          <LoaderModal isLoading={isLoading} />

            {/* <Text>lkjfdslkj</Text> */}
            {/* { toastShow("Image upload successfuly ", colorConstant.successGreen)} */}

            <View>
              <TouchableOpacity
                onPress={() => setLicenseModal(false)}
                style={styles.backButton}>
                <Image
                  source={require("../assets/images/purpleArrow.png")}
                  style={styles.img}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>

            <View style={styles.headingText}>
              <Text style={styles.infotext}>Take a picture of</Text>
              <Text style={styles.infotext}>
                your ID{" "}
                <Image
                  source={imageConstant.info}
                  resizeMode="contain"
                  style={styles.infoImg}
                />
              </Text>
            </View>

            <View style={[styles.alc, styles.avtarImg]}>
              <Image
                resizeMode="contain"
                source={require("../assets/images/license.png")}
                style={styles.img2}
              />
            </View>

            <View style={{ marginTop: moderateScale(24) }}>
              <CustomButton
                buttonText={`${'Licence'} front`}
                marginTop={25}
                width={"85%"}
                rImage={licenseFront !== "" ? imageConstant.right : null}
                OnButtonPress={() => getImageUrl("Licence")}
              />

              <CustomButton
                disabled={licenseFront == "" ? true : false}
                buttonText={`${'Licence'} Back `}
                marginTop={25}
                width={"85%"}
                rImage={licenseBack !== "" ? imageConstant.right : null}
                OnButtonPress={() => {
                  getImageUrl("Licence");
                  if (licenseBack !== "") {
                    setLicenseModal(false)
                  }
                }
                }
                backgroundColor={licenseFront == "" ? colorConstant.grayBg : colorConstant.purple}
              />
            </View>
          </View>
          {/* </SafeAreaView> */}
        </Modal>
        //#endregion


        //region registration fornt and back image

      }


      {
        //#region registration front back image


        <Modal visible={registerModal} statusBarTranslucent>
          {/* <SafeAreaView style={styles.mainContainer}> */}
          <View style={styles.container}>
          <Modal transparent visible={sucessModal}>
        <SafeAreaView style={{}}>
        <View style={{ width: scale(230), backgroundColor: colorConstant.successGreen, paddingVertical: moderateScale(10), alignItems: "center", alignSelf: "center", marginTop: moderateScale(20), borderRadius: 10 }}>
          <Text style={{color:colorConstant.white,fontSize:moderateScale(15)}}>Image upload successful</Text>
        </View>
        </SafeAreaView>
      </Modal>


      <Modal transparent visible={errorModal}>
        <SafeAreaView>
        <View style={{ width: scale(250), backgroundColor: colorConstant.errorRed, paddingVertical: moderateScale(10), alignItems: "center", alignSelf: "center", marginTop: moderateScale(20), borderRadius: 10 }}>
          <Text style={{color:colorConstant.white,fontSize:moderateScale(15)}}>Image upload failed please try again</Text>
        </View>
        </SafeAreaView>
      </Modal>
          <LoaderModal isLoading={isLoading} />

            <View>
              <TouchableOpacity
                onPress={() => setRegisterModal(false)}
                style={styles.backButton}>
                <Image
                  source={require("../assets/images/purpleArrow.png")}
                  style={styles.img}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>

            <View style={styles.headingText}>
              <Text style={styles.infotext}>Take a picture of</Text>
              <Text style={styles.infotext}>
                your ID{" "}
                <Image
                  source={imageConstant.info}
                  resizeMode="contain"
                  style={styles.infoImg}
                />
              </Text>
            </View>

            <View style={[styles.alc, styles.avtarImg]}>
              <Image
                resizeMode="contain"
                source={require("../assets/images/license.png")}
                style={styles.img2}
              />
            </View>

            <View style={{ marginTop: moderateScale(24) }}>
              <CustomButton
                buttonText={`${'Vehicle Registration'}   `}
                marginTop={25}
                width={"85%"}
                right={moderateScale(30)}
                rImage={registerFront !== "" ? imageConstant.right : null}
                OnButtonPress={() => getImageUrl("Register")}
              />
              {/* <Image source={imageConstant.right} style={{position:'absolute',height:moderateScale(20),width:moderateScale(20)}}/> */}


              <CustomButton
                disabled={registerFront == "" ? true : false}
                buttonText={`${'Vehicle Insurance '} `}
                marginTop={25}
                width={"85%"}
                right={moderateScale(30)}

                rImage={registerBack !== "" ? imageConstant.right : null}
                OnButtonPress={() => { getImageUrl("Register");  }}
                backgroundColor={registerFront == "" ? colorConstant.grayBg : colorConstant.purple}
              />
            </View>
          </View>
          {/* </SafeAreaView> */}
        </Modal>

        //#endregion
      }


      <Modal transparent visible={sucessModal}>
        <SafeAreaView style={{}}>
        <View style={{ width: scale(230), backgroundColor: colorConstant.successGreen, paddingVertical: moderateScale(10), alignItems: "center", alignSelf: "center", marginTop: moderateScale(20), borderRadius: 10 }}>
          <Text style={{color:colorConstant.white,fontSize:moderateScale(15)}}>Image upload successful</Text>
        </View>
        </SafeAreaView>
      </Modal>


      <Modal transparent visible={errorModal}>
        <SafeAreaView>
        <View style={{ width: scale(250), backgroundColor: colorConstant.errorRed, paddingVertical: moderateScale(10), alignItems: "center", alignSelf: "center", marginTop: moderateScale(20), borderRadius: 10 }}>
          <Text style={{color:colorConstant.white,fontSize:moderateScale(15)}}>Image upload failed please try again</Text>
        </View>
        </SafeAreaView>
      </Modal>
    </View>
  );
};

export default DocumentPicture;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
    alignItems: "center",
  },
  text1: {
    fontSize: 24,
    fontFamily: fontConstant.semi,
    color: colorConstant.purple,
    textAlign: "center",
  },
  headingView: {
    width: "80%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: Height * 0.1,
  },
  img: {
    width: 25,
    height: 25,
    marginLeft: 10,
  },
  img2: {
    width: 250,
    height: null,
    aspectRatio: 1 / 1.1,
    alignSelf: "center",
    marginTop: 110,
  },



  mainContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: "#ffff",
    paddingHorizontal: moderateScale(20),
  },
  img: {
    height: moderateScale(35),
    width: moderateScale(35),
  },
  infotext: {
    fontSize: 29,
    color: colorConstant.purple,
    fontFamily: fontConstant.semi,
  },
  alc: {
    alignItems: "center",
  },
  avtarImg: {
    marginTop: moderateScale(35),
  },
  img2: {
    height: moderateScale(280),
  },
  backButton: { width: scale(50), marginTop: moderateScale(20) },
  headingText: { alignItems: "center", marginTop: moderateScale(20) },
  infoImg: { height: 30, width: 30 },
});
