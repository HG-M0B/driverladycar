import {
  BackHandler,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import React from "react";
import { colorConstant, fontConstant } from "../utils/constant";
import { KeyboardAwareScrollView } from "@codler/react-native-keyboard-aware-scroll-view";
import CustomInput from "../customComponents/CustomInput";
import CustomDropDown from "../customComponents/CustomDropDown";
import CustomButton from "../customComponents/CustomButton";
import { Height } from "../dimension/dimension";
import { useEffect } from "react";
import { useState } from "react";
import { AddNewVechiclesServices } from "../services/DriverServices";
import { useSelector } from "react-redux";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "../utils/api";
import toastShow from "../utils/toastShow";
import LoaderModal from "../customComponents/LoaderModal";
import { useFocusEffect } from "@react-navigation/native";
import Validations from "../utils/Validations";

const AdditionalInfo2 = (props) => {
  let numberOfDoorsData = [
    {
      label: "4",
      value: "4",
    },
    {
      label: "5",
      value: "5",
    },
    {
      label: "6",
      value: "6",
    },
    {
      label: "7",
      value: "7",
    },
    {
      label: "8",
      value: "8",
    },
  ];

  let numberOfSeatData = [
    {
      label: "1",
      value: "1",
    },
    {
      label: "2",
      value: "2",
    },
    {
      label: "3",
      value: "3",
    },
    {
      label: "4",
      value: "4",
    },
    {
      label: "5",
      value: "5",
    },
    {
      label: "6",
      value: "6",
    },
    {
      label: "7",
      value: "7",
    },
    {
      label: "8",
      value: "8",
    },
  ];
  const paramData = props?.route?.params;

  // console.log(paramData,'jkshdfsjkf')

  // define all state here
  const [flow, setFlow] = useState("");
  const [vehiclesDetails, setVehiclesDetails] = useState({
    vehicleId: "",
    stateId: "",
    cityId: "",
    vehicleTypeId: "",
    plate: "",
    year: "",
    make: "",
    model: "",
    color: "",
    doorsNumber: "",
    seatsBelt: "",
    vehicleRegistration: "",
    vehicleInsurence: "",
    selectedState: "",
    selectedCity: "",
  });
  const [vehicleTypeData, setVechiclesData] = useState([]);
  const [stateList, setStateList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [cityList, setCityList] = useState([]);

  // all function define here

  const Validater = () => {
    // alert('sjdfhkj')
    Validations.isEmpty(vehiclesDetails.plate)
      ? toastShow("Please enter plate number", colorConstant.errorRed)
      : Validations.isEmpty(vehiclesDetails.stateId)
      ? toastShow("Please select state", colorConstant.errorRed)
      : Validations.isEmpty(vehiclesDetails.cityId)
      ? toastShow("Please select city", colorConstant.errorRed)
      : Validations.isEmpty(vehiclesDetails.make)
      ? toastShow("Please enter make", colorConstant.errorRed)
      : Validations.isEmpty(vehiclesDetails.year)
      ? toastShow("Please enter year", colorConstant.errorRed)
      : Validations.isEmpty(vehiclesDetails.model)
      ? toastShow("Please enter model", colorConstant.errorRed)
      : Validations.isEmpty(vehiclesDetails.vehicleTypeId)
      ? toastShow("Please select vechicle ", colorConstant.errorRed)
      : Validations.isEmpty(vehiclesDetails.color)
      ? toastShow("Please enter color", colorConstant.errorRed)
      // : Validations.isEmpty(vehiclesDetails.cityId)
      // ? toastShow("Please select city", colorConstant.errorRed)
      : Validations.isEmpty(vehiclesDetails.doorsNumber)
      ? toastShow("Please select door", colorConstant.errorRed)
      : Validations.isEmpty(vehiclesDetails.seatsBelt)
      ? toastShow("Please select seat belt", colorConstant.errorRed)
      : addNewVechiclesHandler();
  };

  const clearData = () => {
    setVehiclesDetails({
      vehicleId: "",
      stateId: "",
      cityId: "",
      vehicleTypeId: "",
      plate: "",
      year: "",
      make: "",
      model: "",
      color: "",
      doorsNumber: "",
      seatsBelt: "",
      vehicleRegistration: "",
      vehicleInsurence: "",
      selectedState: "",
      selectedCity: "",
    });
  };

  // add vechicle api function
  const addNewVechiclesHandler = async () => {
    let url = "";
    //decide url based on flow ex: edit and new Vehicle
    paramData.flow == "edit"
      ? (url = "edit-vechile")
      : (url = "add-new-vehicle");

    const objToSend = {
      vehicleId: vehiclesDetails.vehicleId,
      stateId: vehiclesDetails.stateId,
      cityId: vehiclesDetails.cityId,
      vehicleTypeId: vehiclesDetails.vehicleTypeId,
      plate: vehiclesDetails.plate,
      year: vehiclesDetails.year,
      make: vehiclesDetails.make,
      model: vehiclesDetails.model,
      color: vehiclesDetails.color,
      doorsNumber: vehiclesDetails.doorsNumber,
      seatsBelt: vehiclesDetails.seatsBelt,
      vehicleRegistration: vehiclesDetails.vehicleRegistration,
      vehicleInsurence: vehiclesDetails.vehicleInsurence,
    };
    setIsLoading(true);
    try {
      const response = await AddNewVechiclesServices(objToSend, url);
      toastShow(
        `Vehicle ${paramData.flow == "edit" ? "edit" : "add"} successfuly`,
        colorConstant.successGreen
      );
      clearData();
      props.navigation.navigate("AddVehicles");
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      clearData();
      console.log(error, objToSend);
      toastShow("Something went wrong", colorConstant.errorRed);
    }
  };

  // city list form api
  const getCityList = async () => {
    try {
      let response = await axios.get(
        `static/city-list?stateId=${vehiclesDetails.stateId}`
      );
      if (response && response.status == 200) {
        // console.log(cityList,'jskahdjkshdakjsdhk')
        setCityList(response.data.askedCity);
        // updateState({ ...iState, cityList: response.data.askedCity });
      } else {
        toastShow(error.message, colorConstant.errorRed);
      }
    } catch (error) {
      console.log(" get city list error", error);
      toastShow(error, colorConstant.errorRed);
    }
  };

  //get state List form api
  const getStateList = async () => {
    try {
      let response = await axios.get("static/state-list");
      if (response && response.status == 200) {
        setStateList(response.data.stateList);
      } else {
        toastShow(response?.data?.message, colorConstant.errorRed);
      }
    } catch (error) {
      toastShow(error, colorConstant.errorRed);
    }
  };

  // get vehicleTypeData form api
  const getVihiclesDetailsDataFunction = async () => {
    try {
      let response = await axios.get(`auth/vehicle-type`);
      if (response && response.status == 200) {
        setVechiclesData(response.data.result);
      }
    } catch (error) {
      console.log("err", error.message);
    }
  };
  //use Effect Area

  useEffect(() => {
    let mount = true;
    if (mount) {
      getVihiclesDetailsDataFunction();
    }
    return () => {
      mount = false;
    };
  }, []);

  // this useEffect for when user edit the vechicle
  useEffect(() => {
    let mount = true;
    if (mount && paramData.flow == "edit") {
      setVehiclesDetails({
        ...vehiclesDetails,
        plate: paramData?.vehicleData?.vechicleDetail?.plate,
        stateId: paramData?.vehicleData?.vechicleDetail?.stateId,
        cityId: paramData?.vehicleData?.vechicleDetail?.cityId,
        vehicleTypeId: paramData?.vehicleData?.vechicleDetail?.vehicleTypeId,
        vehicleId: paramData?.vehicleData?.vechicleDetail?._id,
        year: paramData?.vehicleData?.vechicleDetail?.year,
        make: paramData?.vehicleData?.vechicleDetail?.make,
        model: paramData?.vehicleData?.vechicleDetail?.model,
        color: paramData?.vehicleData?.vechicleDetail?.color,
        doorsNumber: paramData?.vehicleData?.vechicleDetail?.doorsNumber,
        seatsBelt: paramData?.vehicleData?.vechicleDetail?.seatsBelt,
        vehicleRegistration: paramData?.registrationImg,
        vehicleInsurence: paramData?.insurenceImg,
      });
    }
    return () => {
      mount = false;
    };
  }, []);

  useEffect(() => {
    if (vehiclesDetails.stateId !== "") {
      getCityList();
    }
  }, [vehiclesDetails.stateId]);

  useEffect(() => {
    let mount = true;

    if (mount && paramData.flow == "Add") {
      setVehiclesDetails({
        ...vehiclesDetails,
        vehicleRegistration: paramData.vehicleRegistration,
        vehicleInsurence: paramData.vehicleInsurence,
      });
    }

    return () => {
      mount = false;
    };
  }, []);

  useEffect(() => {
    if (props.route.params?.data === "edit") {
      setFlow("edit");
    }
    getStateList();
  }, []);
  const onChangeCall = (item) => {
    console.log(item);
  };

  useFocusEffect(
    React.useCallback(() => {
      const backAction = () => {
        props.navigation.navigate("AddVehicles");
        return true;
      };

      const backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        backAction
      );

      return () => backHandler.remove();
    }, [])
  );
  return (
    <SafeAreaView style={styles.main}>
      <KeyboardAwareScrollView
        bounces={false}
        contentContainerStyle={{
          paddingTop: 15,
          ...Platform.select({
            ios: {},
          }),
        }}
        showsVerticalScrollIndicator={false}
      >
        <Text style={styles.text1}>
          Additional Information About {"\n"} Your Personal Vehicle
        </Text>
        <CustomInput
          value={vehiclesDetails.plate}
          borderWidth={0}
          placeholderTextColor={colorConstant.darkGray}
          placeholder={"License Plate"}
          marginTop={25}
          onChangeText={(val) =>
            setVehiclesDetails({ ...vehiclesDetails, plate: val })
          }
        />
        {/* <CustomDropDown
          placeholderTextColor={colorConstant.darkGray}
          placeholder={"State"}
          marginTop={20}
          onChange={(item) => onChangeCall(item.label)}
        /> */}

        {/* <CustomDropDown
          placeholder="Select State"
          dropDownData={stateList}
          labelField="name"
          valueField="_id"
          marginTop={20}
          value={vehiclesDetails.stateId}
          onChange={stateHandler}
        /> */}

        <CustomDropDown
          placeholder="Select State"
          dropDownData={stateList}
          labelField="name"
          valueField="_id"
          marginTop={20}
          value={vehiclesDetails.stateId}
          // onChange={}
          onChange={(item) =>
            setVehiclesDetails({
              ...vehiclesDetails,
              stateId: item._id,
              selectedState: item?.name,
              cityId:""
            })
          }
        />

        {vehiclesDetails.stateId !== "" && (
          <CustomDropDown
            placeholder="Select City"
            dropDownData={cityList}
            marginTop={20}
            labelField="cityName"
            valueField="_id"
            value={vehiclesDetails.cityId}
            onChange={(item) =>
              // console.log(item,'cityasjkhdk')
              setVehiclesDetails({ ...vehiclesDetails, cityId: item._id })
            }
          />
        )}

        {/* <CustomDropDown
          marginTop={20}
          placeholderTextColor={colorConstant.darkGray}
          placeholder={"Make"}
          onChange={(item) => onChangeCall(item.label)}
        /> */}

        <CustomInput
          value={vehiclesDetails.make}
          borderWidth={0}
          placeholder={"Make"}
          marginTop={20}
          onChangeText={(val) =>
            setVehiclesDetails({ ...vehiclesDetails, make: val })
          }
        />

        <CustomInput
          value={vehiclesDetails.year}
          borderWidth={0}
          placeholder={"Year"}
          marginTop={20}
          onChangeText={(val) =>
            setVehiclesDetails({ ...vehiclesDetails, year: val })
          }
          keyboardType={"numeric"}
        />

        <CustomInput
          value={vehiclesDetails.model}
          borderWidth={0}
          placeholder={"Model"}
          marginTop={20}
          onChangeText={(val) =>
            setVehiclesDetails({ ...vehiclesDetails, model: val })
          }
        />

        {/* <CustomDropDown
          marginTop={20}
          placeholderTextColor={colorConstant.darkGray}
          placeholder={"Model"}
          onChange={(item) => onChangeCall(item.label)}
        /> */}
        <CustomDropDown
          dropDownData={vehicleTypeData}
          labelField="vehicleType"
          valueField="_id"
          marginTop={20}
          placeholder={"Vehicle Type"}
          value={vehiclesDetails.vehicleTypeId}
          // onChange={onChangeCall}
          // marginTop={20}
          placeholderTextColor={colorConstant.darkGray}
          // placeholder={"Vehiclesss Type"}
          // dropDownData={vehicleTypeData}

          onChange={(item) =>
            // console.log(item)
            setVehiclesDetails({ ...vehiclesDetails, vehicleTypeId: item._id })
          }
        />
        <CustomInput
          value={vehiclesDetails.color}
          borderWidth={0}
          placeholder={"Color"}
          marginTop={20}
          onChangeText={(val) =>
            setVehiclesDetails({ ...vehiclesDetails, color: val })
          }
        />

        <CustomDropDown
          dropDownData={numberOfDoorsData}
          value={vehiclesDetails.doorsNumber}
          marginTop={20}
          placeholderTextColor={colorConstant.darkGray}
          placeholder={"Number of door's"}
          onChange={(item) =>
            setVehiclesDetails({ ...vehiclesDetails, doorsNumber: item.value })
          }

          // onChange={(item) => onChangeCall(item.label)}
        />
        <CustomDropDown
          dropDownData={numberOfSeatData}
          value={vehiclesDetails.seatsBelt}
          marginVertical={20}
          placeholderTextColor={colorConstant.darkGray}
          placeholder={"Number of seat belts"}
          onChange={(item) =>
            setVehiclesDetails({ ...vehiclesDetails, seatsBelt: item.value })
          }
          // onChange={(item) => onChangeCall(item.label)}
        />
        <CustomButton
          width={"80%"}
          // position={'absolute'}
          // bottom={20}
          marginBottom={20}
          buttonText={paramData.flow === "edit"? "Update" : "Add"}
          // OnButtonPress={() => props.navigation.goBack()}
          OnButtonPress={() => Validater()}
        />
      </KeyboardAwareScrollView>
      <LoaderModal isLoading={isLoading} />
    </SafeAreaView>
  );
};

export default AdditionalInfo2;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
  },
  text1: {
    fontSize: 24,
    fontFamily: fontConstant.bold,
    color: colorConstant.purple,
    textAlign: "center",
    marginTop: Platform.OS == "ios" ? 0 : 40,
  },
});
