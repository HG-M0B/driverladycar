import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Platform,
} from "react-native";
import React from "react";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import CustomHeader from "../customComponents/CustomHeader";
import { Height } from "../dimension/dimension";
import CustomButton from "../customComponents/CustomButton";
import { useEffect } from "react";
import { getAllAsyncData, getData } from "../utils/HelperFunction";
import axios from "../utils/api";
import { useState } from "react";
import LoaderModal from "../customComponents/LoaderModal";

const FaceRecog = (props) => {
  const [signUpData, setSignUpData] = useState(null);
  const [udidData, setUdidData] = useState(null);
  const [isLoading,setIsLoading]=useState(false)
  const [isSave,setIsSave]=useState(false)
  useEffect(() => {
    let subs = props.navigation.addListener("focus", () => {
      getDataForSignUp();
    });
    return () => subs;
  }, []);

  // ******Collecting data******
  const { docsData, addInfoData,ssnNumber } = props?.route?.params;
  let tempSignUpData;
  let tempUdid;

  const getDataForSignUp = async () => {
    setUdidData(JSON.parse(await getData("udid")));
    setSignUpData(JSON.parse(await getData("signUpData")));
  };

  const signUpApiCall = async () => {
    setIsLoading(true)
    // create body data for sign up api
    let objToSend = {
      dlFront: docsData.licenseFront,
      dlBack: docsData.licenseBack,
      vehicleRegistration: docsData.registerFront,
      vehicleInsurence: docsData.registerBack,
      stateId: signUpData.selectedStateID,
      cityId: signUpData.selectedCityID,
      phoneNumber: signUpData.formattedNumber,
      name: signUpData.name,
      email: signUpData.email,
      password: signUpData.password,
      plate: addInfoData.plateNumber,
      year: addInfoData.year,
      make: addInfoData.make,
      model: addInfoData.model,
      color: addInfoData.color,
      doorsNumber: addInfoData.numberDoor,
      seatsBelt: addInfoData.numberSeat,
      dob: signUpData.birthday,
      signupType:signUpData.signupType,
      vehicleTypeId: addInfoData.vehicleTypeID,
      socialId:signUpData.socialId,
      address:signUpData.address,
      ssnNumber:ssnNumber
    };

    // console.log(objToSend, "body data kjsahdaksjdh");
    console.log('====================================');
    console.log(objToSend,'0000sjkdhasjkdhaskjdhaskjdhkasjd');
    console.log('====================================');


    
    try {
      let response = await axios.post("auth/driver-signUp", objToSend, {
        headers: {
          "Content-Type": "application/json",
        },
      });
      setIsLoading(false)
      setIsSave(true)
      console.log(response.data, "Login sucess done");
      if (response && response.status == 200) {
        props.navigation.navigate("Wishes");
      } else {
        console.log("response else--->", JSON.stringify(response));
      }

    } catch (error) {
      setIsLoading(false)
      console.log("err", error);
      console.log(objToSend,'APi failed to send data');

    }
  };
  return (
    <View style={styles.main}>
      <CustomHeader navigation={props.navigation} />
      <Text style={styles.text1}>
        LadyCar requires {"\n"}facial recognition{" "}
      </Text>
      <TouchableOpacity>
        <Image
          resizeMode="contain"
          source={imageConstant.info}
          style={{
            width: 25,
            height: 25,
            alignSelf: "center",
            marginTop: 15,
          }}
        />
      </TouchableOpacity>
      <Image
        resizeMode="contain"
        source={imageConstant.face}
        style={{
          width: "91%",
          height: Height * 0.31,
          alignSelf: "center",
          marginTop: 35,
        }}
      />
      <CustomButton
        disabled={isSave}
        buttonText={"Face ID"}
        position={"absolute"}
        bottom={Platform.OS == "ios" ? 30 : 30}
        OnButtonPress={signUpApiCall}
        // OnButtonPress={()=>props.navigation.navigate("Wishes")}
      />
      
      <LoaderModal isLoading={isLoading}/>
    </View>
  );
};

export default FaceRecog;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
  },
  text1: {
    fontSize: 26,
    fontFamily: fontConstant.semi,
    color: colorConstant.purple,
    marginTop: 30,
    textAlign: "center",
  },
});
