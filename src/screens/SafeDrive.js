import { Image, StyleSheet, Text, View,ScrollView, SafeAreaView,TouchableOpacity, PermissionsAndroid, BackHandler, Linking, Platform } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import CustomMapView from '../customComponents/CustomMapView'
import { Height, Width } from '../dimension/dimension'
import CustomButton from '../customComponents/CustomButton'
import { useEffect } from 'react'
import CustomCard from '../customComponents/CustomCard'
import { useFocusEffect } from '@react-navigation/native'


const SafeDrive = (props) => {

    useEffect(()=>{
        setTimeout(()=>{
            props.navigation.navigate("EndDrive")
        },3000)
    },[])

    useFocusEffect(
        React.useCallback(() => {
          const backAction = () => {
            console.log("called")
            props.navigation.navigate("ConfirmPickup")
            return true;
          };
      
          const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
          );
      
          return () => backHandler.remove();
        }, [])
      );
  return (
      <SafeAreaView style={styles.main}>
 <View style={styles.row}>
              <TouchableOpacity
                  onPress={() => props.navigation.toggleDrawer()}
              >
                  <Image
                      resizeMode='contain'
                      source={imageConstant.drawer}
                      style={styles.img} />
              </TouchableOpacity>
              <Text style={styles.text}>Have a safe {'\n'}drive!</Text>
              <TouchableOpacity
                  activeOpacity={1}
                  onPress={() => props.navigation.navigate("Profile")} >
                  <Image
                      resizeMode='contain'
                      source={imageConstant.profile1}
                      style={{
                          width: 35,
                          height: 35
                      }} />
</TouchableOpacity>

          </View>
          <ScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}
          >
         
          <View style={{
              ...Platform.select({
                  ios:{
                    height:Height * 0.60
                  }
              })
          }}>
              <CustomMapView
                  height={Height * 0.60}
                  marginTop={10}
              />
              <TouchableOpacity
              activeOpacity={0.8}
              onPress={()=>{
                  Linking.openURL("https://www.google.com/maps/dir/?api=1")
              }}
            //   onPress={()=>{props.navigation.navigate("EndDrive")}}
              style={{
                  backgroundColor:colorConstant.purple,
                  position: "absolute",
                  borderRadius:10,
                  height:Platform.OS =='ios' ? 45 : 40,
                  bottom:Platform.OS =='ios' ? 0 :15 ,
                  width: "70%",
                  flexDirection:"row",
                  alignItems:"center",
                  justifyContent:"center",
                  alignSelf:"center"
              }}>
                  <Image
                      resizeMode='contain'
                      source={imageConstant.direction}
                      style={{
                          width: 15,
                          height: 15,
                          marginRight:15
                      }}
                  />
                  <Text style={{
                      fontSize:16,
                      fontFamily:fontConstant.semi,
                      color:colorConstant.white
                  }}>Direction</Text>
              </TouchableOpacity>
          </View>

          <CustomCard
          marginTop={30}
            //   position={'absolute'}
            //   bottom={20}
              speech={imageConstant.speech}
              onSpeechPress={() => props.navigation.navigate("Chat")}
          />
          <View style={{
              flexDirection:"row",
              alignSelf:"center",
              alignItems:"center",
              marginTop:10
          }}>
              <TouchableOpacity
              activeOpacity={0.8}>
                  <Image
                  resizeMode='contain'
                  source={imageConstant.sos}
                  style={{
                      width:Width*0.35,
                      height:Width*0.35
                  }}
                  />
              </TouchableOpacity>
              <TouchableOpacity
              activeOpacity={0.8}>
                  <Image
                  resizeMode='contain'
                  source={imageConstant.info}
                  style={{
                      width:25,height:25
                  }}
                  />
              </TouchableOpacity>
          </View>
          </ScrollView>
     


      </SafeAreaView>
    
  )
}

export default SafeDrive

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    row:{
        flexDirection:"row",
        width:"93%",
        alignSelf:"center",
        // alignItems:"center",
        justifyContent:"space-between",
        marginTop:Platform.OS == 'ios' ? 10: 20
    },
    img:{
        width:32,
        height:32
    },
    text:{
        fontSize:20,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        textAlign:"center"
    },
    img1:{
        width:15,
        height:15
    }
   
   
})