import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { Height } from '../dimension/dimension'
import { useEffect } from 'react'

const Success = (props) => {

    useEffect(()=>{
        setTimeout(()=>{
            props.navigation.navigate("Payment")
        },2000)
    },[])
  return (
    <View style={{
        flex:1,
        backgroundColor:colorConstant.white,


    }}>
      <View style={{
          marginTop:Height*0.25
      }}>
          <Image
          source={imageConstant.trans}
          style={{
              width:120,
              height:105,
              alignSelf:"center"
          }}
          />
          <Text style={{
              fontSize:15,
              fontFamily:fontConstant.bold,
              color:colorConstant.black,
              textAlign:"center",
              marginTop:20
            

          }}>Transaction Successful</Text>
            <Text style={{
              fontSize:15,
              fontFamily:fontConstant.regular,
              color:colorConstant.black,
              opacity:0.6,
              textAlign:"center",
              marginTop:12

          }}>Your money will be credited soon in your account</Text>
      </View>
    </View>
  )
}

export default Success

const styles = StyleSheet.create({})