import { ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View,Keyboard, Platform} from 'react-native'
import React,{ useState ,useEffect} from 'react'
import { colorConstant, fontConstant } from '../utils/constant'
import CustomHeader from '../customComponents/CustomHeader'
import { Height } from '../dimension/dimension'
import CustomButton from '../customComponents/CustomButton'


const Report = (props) => {
const [option,setOption] = useState(null);
const [padding,setPadding] = useState(0);


// ***************Keyboard manage code**************
    useEffect(() => {
        const showSubscription = Keyboard.addListener("keyboardDidShow", (e) => {
            // if (Platform.OS === "android") {
                setPadding(e.endCoordinates.height)
            // }
          
        });
        const hideSubscription = Keyboard.addListener("keyboardDidHide", () => {
            // if (Platform.OS === "android") {
                setPadding(10)
            // }
        });

        return () => {
            showSubscription.remove();
            hideSubscription.remove();
        };
    }, []);
 


    const data=[
        {
            id:1,
            text:"My driver and I couldn't connect"
        },
        {
            id:2,
            text:"My pickup location was incorrect "
        },
        {
            id:3,
            text:"My driver cancelled "
        },
        {
            id:4,
            text:"The ETA was too long "
        },
        {
            id:5,
            text:"The ETA was too short"
        },
        {
            id:6,
            text:"My driver asked me to cancel"
        },
        {
            id:7,
            text:"Other reason"
        },
    ]
  return (
      <View style={styles.main}>
          <ScrollView
          bounces={false}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                  ...Platform.select({
                      android: {
                          paddingBottom: padding,
                          height: Height,
                      },
                      ios: {
                        paddingBottom: padding,
                      }
                  })
              }}
          >
              <CustomHeader
                  navigation={props.navigation} />
              <Text style={{
                  fontSize: 22,
                  fontFamily: fontConstant.semi,
                  color: colorConstant.purple,
                  alignSelf: "center",
                  textAlign: "center",
                  ...Platform.select({
                    android: {
                        marginTop:15
                    },
                    ios: {}
                })
              }}>Select Reason for Report</Text>

              <View style={{
                  android: {
                      marginTop:25
                },
                ios: {}
              }}>
                  {
                      data.map((item, index) => {
                          return (
                              <View
                                  key={index}
                                  style={{
                                      flexDirection: "row",
                                      width: "80%",
                                      alignSelf: "center",
                                      alignItems: "center",
                                      marginVertical: 10
                                  }}>
                                  <TouchableOpacity
                                      onPress={() => setOption(index)}
                                      style={{
                                          width: 20,
                                          height: 20,
                                          borderRadius: 10,
                                          borderWidth: 2,
                                          backgroundColor: option == index ? colorConstant.purple : colorConstant.white,
                                          borderColor: colorConstant.purple

                                      }}>

                                  </TouchableOpacity>
                                  <Text style={{
                                      fontSize: 14,
                                      fontFamily: fontConstant.semi,
                                      color: colorConstant.black,
                                      marginLeft: 20
                                  }}>{item.text}</Text>
                              </View>
                          )
                      })
                  }
              </View>

              <TextInput
                  placeholder={`Write your reason`}
                  placeholderTextColor={colorConstant.grayText}
                  style={styles.input}
                  multiline={true}
              />
              <CustomButton
                  OnButtonPress={() => props.navigation.goBack()}
                  width={'75%'}
                  buttonText={'Report'}
                  marginTop={Platform.OS == "ios" ? 40 : 30}
              />
          </ScrollView>
      </View>
  )
}

export default Report

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    input: {
        padding:25,
        width: "75%",
        backgroundColor: colorConstant.white,
        alignSelf: "center",
        borderWidth: 1.5,
        marginTop: 30,
        height: Height * 0.25,
        borderColor: colorConstant.purple,
        borderRadius: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.48,
        shadowRadius: 11.95,
        elevation: 18,
        ...Platform.select({
            ios: {
                paddingVertical:20,
            },
            android: {
                textAlignVertical: "top"
            }

        })
    }
})