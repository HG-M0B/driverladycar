import { View, Text,StyleSheet, Image,BackHandler, ScrollView, Platform } from 'react-native'
import React from 'react'
import { fontConstant, imageConstant } from '../../utils/constant'
import CustomHeader from '../../custom/CustomHeader'
import CustomButton from '../../custom/CustomButton'
import { useEffect } from 'react'
import { useState } from 'react'
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import { useFocusEffect } from '@react-navigation/native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { baseUrl } from '../../utils/Urls'
const Passport = (props) => {
  

const openCamera=async()=>{
    // alert('hello')
    let options={
        mediaType:'photo',
        cameraType:'back',
        saveToPhotos:true	
    }
    // launchImageLibrary(options,(response)=>{
    //     console.log('response', response)
    // })
    // launchCamera(options,(res)=>{
    //     console.log(res)
    // })
    launchCamera(options,async(response)=>{
      if (response.didCancel) {
        // toast('User cancelled ', colorConstant.darkRed)
        console.log('User cancelled image picker');
    } else if (response.error) {
        // toast('Something went wrong', colorConstant.darkRed)
        console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);

    } else {
      console.log("response",response);
      props.navigation.navigate("FaceId")
        console.log('response from gallery------------------->', JSON.stringify(response));
        let formDataReq = new FormData();
        let data = {
            uri:response?.assets?.[0]?.uri,
            type:response?.assets?.[0]?.type,
            name:response?.assets?.[0]?.fileName
        }
        formDataReq.append("file",data);
        getImageData2(formDataReq?._parts[0][1])
        // formDataReq.append("uri",response?.assets?.[0]?.uri);
        // formDataReq.append("type",response?.assets?.[0]?.type);
        // formDataReq.append("fileName",response?.assets?.[0]?.fileName);
        // await AsyncStorage.setItem("idBack",JSON.stringify(formDataReq))
        // console.log("formDataReq",JSON.stringify(formDataReq?._part?.[0]));
    }

    })
    
   
}
const openCameras=async()=>{
  let options={
    mediaType:'photo',
    cameraType:'back',
    saveToPhotos:true	
}
// launchImageLibrary(options,(response)=>{
//     console.log('response', response)
// })
launchCamera(options,async(response)=>{
  // console.log(res)
  if (response.didCancel) {
    // toast('User cancelled ', colorConstant.darkRed)
    console.log('User cancelled image picker');
} else if (response.error) {
    // toast('Something went wrong', colorConstant.darkRed)
    console.log('ImagePicker Error: ', response.error);
} else if (response.customButton) {
    console.log('User tapped custom button: ', response.customButton);

} else {
  console.log("response",response);
  // props.navigation.navigate("FaceId")
    console.log('response from gallery------------------->', JSON.stringify(response));
    let formDataReq = new FormData();
    let data = {
        uri:response?.assets?.[0]?.uri,
        type:response?.assets?.[0]?.type,
        name:response?.assets?.[0]?.fileName
    }
    formDataReq.append("file",data);
    // let formDataReq = new FormData();
    // formDataReq.append("uri",response?.assets?.[0]?.uri);
    // formDataReq.append("type",response?.assets?.[0]?.type);
    // formDataReq.append("fileName",response?.assets?.[0]);
    getImageData(formDataReq?._parts[0][1])
    // await AsyncStorage.setItem("idFront",JSON.stringify(formDataReq))
  }
})
}

const getImageData=async(imageName)=>{
  // let data={
  //   fileName:imageName
  // }
  let finalFormData=new FormData()
  finalFormData.append("fileName",imageName)
  console.log("image wala",finalFormData)
 await fetch(baseUrl+"auth/image-upload",
 {
  method:"POST",
  headers:{
    'Content-Type': 'multipart/form-data',
    'Accept':'application/json'
  },
  body:finalFormData
 }
 )
 .then(async(res)=>{
  let jsonData=await res.json()
  return jsonData;
 })
 .then(async(res)=>{
  console.log("resssss",res)
  await AsyncStorage.setItem("idFront",JSON.stringify(res.data))
 })
}

const getImageData2=async(imageName)=>{
  // let data={
  //   fileName:imageName
  // }
  let finalFormData=new FormData()
  finalFormData.append("fileName",imageName)
  console.log("image wala",finalFormData)
 await fetch(baseUrl+"auth/image-upload",
 {
  method:"POST",
  headers:{
    'Content-Type': 'multipart/form-data',
    'Accept':'application/json'
  },
  body:finalFormData
 }
 )
 .then(async(res)=>{
  let jsonData=await res.json()
  return jsonData;
 })
 .then(async(res)=>{
  console.log("resssss",res)
  await AsyncStorage.setItem("idBack",JSON.stringify(res.data))
 })
}
    return (
    <View style={styles.main}>
        <CustomHeader
         onPressBack={()=>props.navigation.goBack()} 
        //  marginBottom={5}
        />
        <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{backgroundColor:'#FFFFFF',alignItems:'center',marginTop:'8%'}}
        >
         <Text style={styles.welcomText}>Take a picture of
your ID <Image source={imageConstant.info} style={{height:16,width:16}}/></Text>
        <Image
        source={imageConstant.camera}
        style={styles.img}
        resizeMode='contain'
        
        />
        <CustomButton
        buttonName={'Passport front'}
        marginTop={'20%'}
        imageRightFlag={true}
        imageName={imageConstant.check}
        OnButtonPress={()=>openCameras()}
        borderRadius={10}
        height={42}
        txtmarginTop={'1%'}
        />
          <CustomButton
        buttonName={'Passport back'}
        marginTop={'5%'}
        imageRightFlag={true}
        OnButtonPress={()=>openCamera()}
        imageName={imageConstant.check}
        borderRadius={10}
        marginBottom={20}
        height={42}
        txtmarginTop={'1%'}
        />
           </ScrollView>
    </View>
  )
}

export default Passport
const styles=StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:'#FFFFFF'
    },
  img:{
width:240,
height:287,
marginTop:'10%'
  },
  welcomText:{
    color:'#6C00C0',
    fontFamily:fontConstant.semibold,
    fontSize:29,
    width:'75%',
    lineHeight:37,
    textAlign:'center',
    marginTop:'3%',
   
},
})