import {
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  FlatList,
  Pressable,
  SafeAreaView,
} from "react-native";
import React, { useEffect } from "react";
import { imageConstant, colorConstant, fontConstant } from "../utils/constant";
import { Height, Width } from "../dimension/dimension";
import { getUserProfileServices } from "../services/DriverServices";
import { useState } from "react";
import LoaderModal from "../customComponents/LoaderModal";
import { useIsFocused } from "@react-navigation/native";
import { moderateScale } from "react-native-size-matters";

const Profile = (props) => {
  let data2 = [
    {
      id: 1,
      img: imageConstant.img,
    },
    {
      id: 2,
      img: imageConstant.taxi3,
    },
    {
      id: 3,
      img: imageConstant.img,
    },
    {
      id: 4,
      img: imageConstant.taxi2,
    },
    {
      id: 5,
      img: imageConstant.taxi2,
    },
    {
      id: 6,
      img: imageConstant.img,
    },
    {
      id: 7,
      img: imageConstant.taxi3,
    },
    {
      id: 8,
      img: imageConstant.img,
    },
    {
      id: 9,
      img: imageConstant.taxi2,
    },
    {
      id: 10,
      img: imageConstant.img,
    },
    {
      id: 11,
      img: imageConstant.taxi3,
    },
    {
      id: 12,
      img: imageConstant.img,
    },
  ];
  const renderDetails = ({ item, index }) => {
    return (
      <View key={index}>
        <Image resizeMode="contain" style={styles.img5} source={item?.img} />
      </View>
    );
  };

  const [driverProfile, setDriverProfile] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const isFocused = useIsFocused();
  const _renderItem = () => {
    return (
      <View
        style={{
          alignSelf: "center",
          margin: 6,
        }}
      >
        <Image
          resizeMode="contain"
          source={imageConstant.jpeg}
          style={{
            width: 110,
            height: 110,
          }}
        />
      </View>
    );
  };

  const getUserProfileFunction = async () => {
    setIsLoading(true);
    try {
      const response = await getUserProfileServices();
      setDriverProfile(response.data.userData);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log(error);
    }
  };

  useEffect(() => {
    let mount = true;
    if (mount) {
      getUserProfileFunction();
    }
    return () => {
      mount = false;
    };
  }, [isFocused]);

  return (
    <SafeAreaView style={styles.main}>
      <LoaderModal isLoading={isLoading} />
      <View style={styles.row}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Image
            resizeMode="contain"
            source={imageConstant.back}
            style={styles.img}
          />
        </TouchableOpacity>

        {/* <View style={styles.searchView}> */}
        {/* <Image
                      resizeMode='contain'
                      source={imageConstant.search}
                      style={styles.img1} /> */}
        <TextInput
          value="My Profile"
          editable={false}
          placeholder="Search Profiles"
          style={[
            styles.input,
            { fontFamily: fontConstant.bold, color: colorConstant.purple },
          ]}
        />
        {/* </View> */}
        <View></View>
      </View>

      <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
        <View style={styles.view3}>
          <Image
            resizeMode="cover"
            style={styles.img3}
            source={{ uri: driverProfile.profilePic }}
          />
          <View style={{ right: 10 }}>
            <Text style={styles.text4}>{driverProfile.name}</Text>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Image
                style={{
                  width: 20,
                  height: 20,
                }}
                resizeMode="contain"
                source={imageConstant.star}
              />
              <Text style={styles.text5}> 4</Text>
            </View>
          </View>
          <View style={styles.view4}>
            <Image
              resizeMode="contain"
              style={{ height: 25, width: 25 }}
              source={imageConstant.calender}
            />
            <Pressable
            //   onPress={() => props.navigation.navigate("Comments")}
            >
              <Image
                resizeMode="contain"
                style={{ height: 30, width: 30 }}
                source={imageConstant.speech}
              />
            </Pressable>
          </View>
        </View>

        <Text style={styles.text1}>{driverProfile.about_you}</Text>

        <View style={styles.row1}>
          {driverProfile.is_smoking && (
            <Image
              resizeMode="contain"
              source={imageConstant.smoke}
              style={{
                width: 30,
                height: 30,
                marginLeft: 23,
              }}
            />
          )}
          {driverProfile.is_music && (
            <Image
              source={imageConstant.music}
              style={{
                width: 30,
                height: 30,
                marginLeft: 23,
              }}
            />
          )}
          {driverProfile.is_chat && (
            <Image
              source={imageConstant.speech}
              style={{
                width: 30,
                height: 30,
                marginLeft: 23,
              }}
            />
          )}
          {driverProfile.is_pet && (
            <Image
              source={imageConstant.dog}
              style={{
                width: 30,
                height: 30,
                marginLeft: 23,
              }}
            />
          )}
        </View>

        <TouchableOpacity
          onPress={() =>
            props.navigation.navigate("AboutDriver", {
              flow: "mainStack",
              driverProfile: driverProfile,
            })
          }
          style={styles.pres1}
        >
          <Image
            style={styles.img4}
            resizeMode="contain"
            source={imageConstant.edit}
          />
          <Text style={styles.text2}> Edit</Text>
        </TouchableOpacity>

        <Pressable style={styles.pres1}>
          <Image
            style={styles.img4}
            resizeMode="contain"
            source={imageConstant.upload}
          />
          <Text style={styles.text2}> Upload</Text>
        </Pressable>
        <FlatList
          bounces={false}
          renderItem={renderDetails}
          data={data2}
          // horizontal
          numColumns={4}
          contentContainerStyle={styles.flatview}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default Profile;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
  },
  row: {
    marginTop: 20,
    flexDirection: "row",
    alignItems: "center",
    width: "90%",
    alignSelf: "center",
    justifyContent: "space-between",
  },
  img: {
    width: 20,
    height: 20,
    tintColor: colorConstant.purple,
  },
  img1: {
    width: 20,
    height: 20,
  },
  searchView: {
    flexDirection: "row",
    width: "85%",
    alignItems: "center",
    backgroundColor: "#EDF1F7",
    paddingVertical: 3,
    paddingHorizontal: 10,
    borderRadius: 10,
    height: 45,
    // shadowColor: "#000",
    // shadowOffset: {
    //     width: 0,
    //     height: 3,
    // },
    // shadowOpacity: 0.27,
    // shadowRadius: 4.65,

    // elevation: 6,
  },
  input: {
    width: "80%",
    // paddingLeft:5,
    ...Platform.select({
      android: {
        paddingVertical: 0,
      },
    }),
    fontSize: moderateScale(20),
    textAlign: "center",
  },
  profile: {
    width: 100,
    height: 100,
    alignSelf: "center",
    marginTop: 40,
  },
  text: {
    fontSize: 24,
    fontFamily: fontConstant.semi,
    color: colorConstant.black,
    textAlign: "center",
    marginTop: 15,
  },
  img2: {
    width: "60%",
    height: 30,
    alignSelf: "center",
    marginTop: 20,
  },
  text1: {
    fontSize: 14,
    fontFamily: fontConstant.regular,
    color: colorConstant.black,
    width: "90%",
    alignSelf: "center",
    textAlign: "left",
    marginTop: 20,
    lineHeight: 17,
  },
  row1: {
    flexDirection: "row",
    alignSelf: "flex-start",
    // width:"90%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  view3: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    marginTop: "8%",
  },
  img3: {
    height: 60,
    width: 60,
    borderRadius: 60,
    borderWidth:10
  },
  text4: {
    fontSize: 15,
    color: "#000000",
    fontWeight: "600",
  },
  text5: {
    fontSize: 14,
    color: "#000000",
    textAlign: "center",
  },
  text6: {
    fontSize: 12,
    color: "#000000",
  },
  text7: {
    fontSize: 14,
    color: "#6C00BF",
    fontWeight: "600",
  },
  view4: {
    width: "25%",
    flexDirection: "row",
    justifyContent: "space-around",
  },
  pres1: {
    height: 38,
    width: Width * 0.75,
    borderRadius: 9,
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    alignSelf: "center",
    marginTop: "5%",
  },
  img4: {
    height: 24,
    width: 24,
  },
  text2: {
    color: "#000000",
    fontSize: 15,
    fontWeight: "700",
  },
  img5: {
    margin: 3,
    height: 90,
    width: 90,
    marginVertical: 7,
  },
  flatview: {
    width: Width * 0.9,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "12%",
  },
});
