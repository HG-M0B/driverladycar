import { Image, StyleSheet, Text, View,ScrollView, SafeAreaView,TouchableOpacity, PermissionsAndroid, BackHandler, Platform } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import CustomMapView from '../customComponents/CustomMapView'
import { Height } from '../dimension/dimension'
import CustomButton from '../customComponents/CustomButton'
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import { useFocusEffect } from '@react-navigation/native'


const OnWay = (props) => {





    //Opening Camer Here********************
    const requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.CAMERA,
              {
                title: 'Camera Permission',
                message: 'App needs camera permission',
              },
            );
            // If CAMERA Permission is granted
            return granted === PermissionsAndroid.RESULTS.GRANTED;
          } catch (err) {
            console.warn(err);
            return false;
          }
        } else return true;
      };
      const requestExternalWritePermission = async () => {
        if (Platform.OS === 'android') {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                title: 'External Storage Write Permission',
                message: 'App needs write permission',
              },
            );
            // If WRITE_EXTERNAL_STORAGE Permission is granted
            return granted === PermissionsAndroid.RESULTS.GRANTED;
          } catch (err) {
            console.warn(err);
            alert('Write permission err', err);
          }
          return false;
        } else return true;
      };
      const handleOpenCamera = async () => {
        let isCameraPermitted = await requestCameraPermission();
        let isStoragePermitted = await requestExternalWritePermission();
        if (isCameraPermitted && isStoragePermitted) {
            console.log("access-->",isCameraPermitted,isStoragePermitted)
            let options={
                mediaType:'photo',
                cameraType:'back',
                saveToPhotos:true,
                quality: 5, 
                maxWidth: 500,
                 maxHeight: 500, 
                 includeBase64: true, 
                 noData: true,
            }
          await launchCamera(options, response => { 
            if (response.didCancel) { 
            console.log('Cancelled');
            } else if (response.error) { 
            console.log('Error', response.errorMessage);
            } else { 
            console.log(response); 
            // setFilePath(response.uri); 
            // setBase64('data:image/png;base64,' + response.base64); 
          } 
          })
        }
      }

      useFocusEffect(
        React.useCallback(() => {
          const backAction = () => {
            console.log("called")
            props.navigation.navigate("TripRequest")
            return true;
          };
      
          const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
          );
      
          return () => backHandler.remove();
        }, [])
      );
    
  return (
      <SafeAreaView style={styles.main}>

          <View style={styles.row}>
            <TouchableOpacity
              onPress={() => props.navigation.toggleDrawer()}
            >
              <Image
                resizeMode='contain'
                source={imageConstant.drawer}
                style={styles.img} />
            </TouchableOpacity>
            <Text style={styles.text}>You’re on your{'\n'}way</Text>

            <TouchableOpacity
              activeOpacity={1}
              onPress={() => props.navigation.navigate("Profile")} >
              <Image
                resizeMode='contain'
                source={imageConstant.profile1}
                style={{
                  width: 35,
                  height: 35,
                }} />
            </TouchableOpacity>

          </View>
      <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
        <CustomMapView
          height={Platform.OS =="ios" ? Height * 0.65:Height * 0.70}
          marginTop={10}
        />
        <CustomButton
          buttonText={"I am here"}
          width={'80%'}
          marginTop={20}
          OnButtonPress={() => props.navigation.navigate("ConfirmPickup")
          }
        />
        <View style={styles.row1}>
          <TouchableOpacity
            activeOpacity={1}>
            <Text style={styles.text1}>Cancel</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={1}>
            <Image
              source={imageConstant.info}
              style={styles.img1} />
          </TouchableOpacity>

        </View>
      </ScrollView>
      </SafeAreaView>
    
  )
}

export default OnWay

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    row:{
        flexDirection:"row",
        width:"93%",
        alignSelf:"center",
        justifyContent:"space-between",
        ...Platform.select({
          ios:{
            marginTop:10
          },
          android:{
            marginTop:20
          }
        })
       
    },
    img:{
        width:32,
        height:32
    },
    text:{
        fontSize:22,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        textAlign:"center"
    },
    img1:{
        width:25,
        height:25
    },
    row1:{
        flexDirection:"row",
        alignSelf:"center",
        justifyContent:"center",
        alignItems:"center",
        marginTop:40


    },
    text1:{
        fontSize:16,
        fontFamily:fontConstant.regular,
        color:colorConstant.purple,
        textDecorationLine:"underline",
        margin:7
    },
    container:{
        flex:1
    }
})