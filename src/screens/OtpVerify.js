import {
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import React, { useRef, useState } from "react";
import { KeyboardAwareScrollView } from "@codler/react-native-keyboard-aware-scroll-view";
import { colorConstant, fontConstant } from "../utils/constant";
import { Height } from "../dimension/dimension";
import auth from "@react-native-firebase/auth";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import CustomButton from "../customComponents/CustomButton";
import { useEffect } from "react";
import { storeData } from "../utils/HelperFunction";
import toastShow from "../utils/toastShow";
import Loader from "../utils/Loader";
import { useCallback } from "react";

const intialState = {
  otp: "",
  loader: false,
  countryCode: null,
  number: null,
  stateFlag: "",
};
const CELL_COUNT = 6;
const OtpVerify = (props) => {
  const [iState, updateState] = useState(intialState);
  const [code, setCode] = useState("");
  const [confirm, setConfirm] = useState(null);
  const [flow, setFlow] = useState("");
  const [seconds, setSeconds] = useState(30);
  const [isTimerRunning, setIsTimerRunning] = useState(true);
  // const [seconds, setSeconds] = useState(30);

  useEffect(() => {
    let interval = null;
    if (isTimerRunning) {
      interval = setInterval(() => {
        setSeconds((seconds) => {
          if (seconds === 0) {
            clearInterval(interval);
            setIsTimerRunning(false);
            return 30;
          }
          return seconds -1
        });
      }, 1000);
    }
    return () => clearInterval(interval);
  }, [isTimerRunning]);



  console.log(props.route.params, "----------kdfjsldfjldfjlk");

  console.log("====================================kajshdakjsdhjkasd");
  console.log(props.route.params, "sdhfkdhfkjsdhfkjsd");
  console.log("====================================askjdhkajsdghjasgd218te8t");

  const { otp, loader, number, countryCode, stateFlag } = iState;

  useEffect(() => {
    let subscription = props.navigation.addListener("focus", () => {
      if (props?.route?.params) {
        // alert(JSON.stringify("+"+props?.route?.params?.cCode+props?.route?.params?.number))
        // getOtp(
        //   "+" + props?.route?.params?.cCode + props?.route?.params?.number
        // );
        //   setFlow(props?.route.params.flow);
        //   getOtp("+",props?.route?.params?.cCode+props?.route?.params?.number);
          updateState({
            ...iState,
            number: props?.route?.params?.number,
            countryCode: props?.route?.params?.cCode,
            stateFlag: props?.route?.params?.stateFlag,
          });
      }
    });
    return () => {
      subscription;
    };
  }, []);

  // useEffect(() => {
  //   console.log("useCallback called");
  //   if (number !== null && countryCode !== null) {
  //     getOtp();
  //   }
  // }, [number,countryCode]);

  const onOtpVerified = async (user) => {
    console.log("got the user--->", user);
    let verifiedNumber = {
      udid: user?.user?.uid,
      phoneNumber: number,
      cCode: countryCode,
      sFlag: stateFlag,
    };
    console.log("Saving this the user--->", verifiedNumber);

    if (props.route?.params?.flow == "forget") {
      props.navigation.navigate("Reset", {
        data: number,
        flow: "Number",
      });
    }

    else{
    await storeData(
      "udid",
      JSON.stringify(verifiedNumber),
      //   DocumentPicture
      (setDataFunction = () => {
        // updateState({...iState,loader:true});
        if (
          props.route != undefined &&
          props.route?.params?.flow === "Google"
        ) {
          props.navigation.navigate("DocumentPicture", {
            data: verifiedNumber,
            flow: "Google",
          });
        } else {
          props.navigation.navigate("Register", {
            data: verifiedNumber,
            flow: "Number",
          });
        }
      })
    );
    }
  };


  const DummyOTP = async (user) => {
    console.log("got the user--->", user);
    let verifiedNumber = {
      // udid: user?.user?.uid,
      phoneNumber: number,
      cCode: countryCode,
      sFlag: stateFlag,
    };
    console.log("Saving this the user--->", verifiedNumber);

    if (props.route?.params?.flow == "forget") {
      props.navigation.navigate("Reset", {
        data: number,
        flow: "Number",
      });
    }

    else{
    await storeData(
      "udid",
      JSON.stringify(verifiedNumber),
      //   DocumentPicture
      (setDataFunction = () => {
        // updateState({...iState,loader:true});
        if (
          props.route != undefined &&
          props.route?.params?.flow === "Google"
        ) {
          props.navigation.navigate("DocumentPicture", {
            data: verifiedNumber,
            flow: "Google",
          });
        } else {
          props.navigation.navigate("Register", {
            data: verifiedNumber,
            flow: "Number",
          });
        }
      })
    );
    }
  };





  
  const getOtp = async (phoneNumber) => {
    console.log("Forget Flow---->", phoneNumber);
    try {
      const confirmation = await auth().verifyPhoneNumber(phoneNumber);
      setConfirm(confirmation?.verificationId);
    } catch (error) {
      alert(error);
    }
  };

  const verifyCode = async () => {
    try {
      // updateState({...iState,loader:true})
      const credential = auth.PhoneAuthProvider.credential(confirm, code);
      console.log("credential", credential);
      let userData = await auth().signInWithCredential(credential);
      onOtpVerified(userData);
    } catch (error) {
      updateState({ ...iState, loader: false });
      console.log("error-->", error);
      if (error.code == "auth/invalid-verification-code") {
        toastShow("Please enter valid OTP", "red");
      } else {
        console.log("Account linking error");
      }
    }
  };

  const resendOtp = () => {
    setCode("");
    console.log(code,'skldjslkdjklsjdslkjd')
    setSeconds(30)
    setIsTimerRunning(true);

    // alert('dkkjsj')
    // getOtp("+" + countryCode + number);
    setConfirm(null);
  };

  const onContinue = () => {
    if (code === "") {
      toastShow("Please enter the verification code", colorConstant.errorRed);
    }
    if (code.length < 6) {
      toastShow("Please enter the verification code", colorConstant.errorRed);
    } else {
      DummyOTP();
    }

    if (flow == "login") {
        props.navigation.navigate('Reset')
    }
    else {
        props.navigation.navigate('Register')
    }
  };

  // useEffect(() => {
  //   if(seconds>0){
  //   let interval = setInterval(() => {
  //       setSeconds(seconds => seconds - 1);

  //     }, 1000);
  //   }
  //   return () => clearInterval(interval);
  // }, []);

  // console.log(
  //   "flow-->",
  //   flow,
  //   "number-->",
  //   number,
  //   "id==>",
  //   confirm,
  //   "code==>",
  //   code
  // );
  console.log("====================================");
  console.log("confirmconfirmconfirm", confirm);
  console.log("====================================");
  return (
    <SafeAreaView style={styles.main}>
      {/* <Loader data={loader}/> */}
      <KeyboardAwareScrollView
        bounce={false}
        style={{
          flex: 1,
        }}
      >
        <Text style={styles.text1}>Verification Code</Text>
        <Text style={styles.text2}>
          A code has just been sent to your Phone Number
        </Text>

        <OTPInputView
          style={{
            width: "80%",
            height: Height * 0.09,
            alignSelf: "center",
            marginTop: 10,
          }}
          pinCount={6}
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
          onCodeFilled={(code) => {
            setCode(code);
          }}
        />

        <View style={styles.resendView}>
          <Text style={styles.text3}>Dont receive the Otp ? <Text style={{color:colorConstant.purple}}> {seconds} sec </Text></Text>
         {!isTimerRunning && <TouchableOpacity onPress={resendOtp}>
            {
              <Text
                style={[
                  styles.text3,
                  {
                    color: colorConstant.purple,
                    fontSize: 16,
                    fontFamily: fontConstant.bold,
                  },
                ]}
              >
                {"  "}Resend OTP
              </Text>
            }
          </TouchableOpacity>
}

          {/* {isTimerRunning ? (
            <Text>{seconds} seconds left</Text>
          ) : (
            <TouchableOpacity onPress={() => setIsTimerRunning(true)}>
              <Text>Resend OTP</Text>
            </TouchableOpacity>
          )} */}
        </View>
      </KeyboardAwareScrollView>
      <CustomButton
        buttonText={"Continue"}
        bottom={Platform.OS == "ios" ? 20 : 20}
        OnButtonPress={DummyOTP}
        backgroundColor={
          confirm == !null ? colorConstant.grayBg : colorConstant.purple
        }
        color={confirm == !null ? colorConstant.black : colorConstant.white}
      />
    </SafeAreaView>
  );
};

export default OtpVerify;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colorConstant.white,
    // ...Platform.select({
    //     ios:{
    //         marginTop:30
    //     }
    // })
  },
  text1: {
    fontSize: 24,
    fontFamily: fontConstant.semi,
    color: colorConstant.purple,
    alignSelf: "center",
    ...Platform.select({
      ios: {
        marginTop: 50,
      },
      android: {
        marginTop: 80,
      },
    }),
  },
  text2: {
    fontSize: 12,
    fontFamily: fontConstant.regular,
    color: colorConstant.textGary,
    alignSelf: "center",
    marginTop: 50,
  },

  underlineStyleBase: {
    width: 50,
    height: 50,
    fontSize: 22,
    borderWidth: 1,
    color: colorConstant.black,
    borderColor: colorConstant.placeholderColor,
    borderRadius: 10,
    fontFamily: fontConstant.semi,
    // backgroundColor:'pink'
  },
  underlineStyleHighLighted: {
    borderColor: colorConstant.purple,
    color: colorConstant.purple,
    borderWidth: 2,
  },
  resendView: {
    flexDirection: "row",
    with: "90%",
    alignItems: "center",
    alignSelf: "center",
    marginTop: 20,
  },
  text3: {
    fontSize: 14,
    fontFamily: fontConstant.semi,
    color: colorConstant.textGary,
  },
});
