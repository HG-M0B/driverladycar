import { Image, Platform, SafeAreaView, StyleSheet, Text, TextInput, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import CustomHeader from '../customComponents/CustomHeader'
import CustomDropDown from '../customComponents/CustomDropDown'
import CustomButton from '../customComponents/CustomButton'
import { Height, Width } from '../dimension/dimension'
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'
import { onResponderTerminationRequest } from 'deprecated-react-native-prop-types/DeprecatedViewPropTypes'

const HelpCenter = (props) => {
  return (
    <SafeAreaView style={styles.main}>
        <CustomHeader
        navigation={props.navigation}
        />
        <Text style={styles.text1}>Help Center</Text>
          <KeyboardAwareScrollView
              bounce={false}
              showsVerticalScrollIndicator={false}
          >
              <CustomDropDown
                  marginTop={40}
                  placeholderFont={14}
                  fontFamily={fontConstant.semi}
                  placeholder={"Select"}
                  placeholderColor={colorConstant.grayButton}
                  onChange={(item) => console.log(item.lable)}
              />

              <CustomDropDown
                  placeholderFont={14}
                  fontFamily={fontConstant.semi}
                  placeholder={"Trip"}
                  placeholderColor={colorConstant.grayButton}
                  onChange={(item) => console.log(item.lable)}
              />

              <TextInput
                  multiline={true}
                  placeholder='Description'
                  placeholderTextColor={colorConstant.grayButton}
                  style={styles.description}
              />
              <View style={{
                  flexDirection: "row",
                  alignSelf: "center",
                  marginTop: 50,
                  width: "80%",
                  justifyContent: "space-between",
                  //  backgroundColor:"green"
              }}>
                  <CustomButton
                      // width={'70%'}
                      buttonText={'Continue'}
                  />
                  <Image
                      source={imageConstant.question}
                      resizeMode='contain'
                      style={{
                          width: 25,
                          height: 25,
                          marginLeft: 10,
                          alignSelf: "center",
                      }} />
              </View>
          </KeyboardAwareScrollView>
    </SafeAreaView>
  )
}

export default HelpCenter

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    },
    text1:{
        fontSize:22,
        fontFamily:fontConstant.bold,
        color:colorConstant.purple,
        textAlign:"center"

    },
    description: {
       
        fontSize:14,
        fontFamily:fontConstant.semi,
        color:colorConstant.black,
        marginTop:30,
        width: "80%",
        backgroundColor: "green",
        alignSelf: "center",
        borderColor:colorConstant.purple,
        borderWidth:1.5,
        height: Height * 0.3,
        backgroundColor: colorConstant.white,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        ...Platform.select({
            ios: {
                paddingTop:15,
                paddingLeft:15
            },
            android: {
                padding:20,
                textAlignVertical: "top"
            }
        })
    },
    input:{
       
    }
})