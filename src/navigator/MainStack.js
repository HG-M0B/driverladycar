
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions
} 
from 'react-native';
import React, { Component,useEffect } from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer';


import CustomInput from '../customComponents/CustomInput';


import  Start from '../screens/Start';
import OnBoarding1 from '../screens/OnBoarding1';
import OnBoarding2 from '../screens/OnBoarding2';
import OtpVerify from '../screens/OtpVerify';
import Register from '../screens/Register';
import DriverLicence from '../screens/DriverLicence';
import DocumentPicture from '../screens/DocumentPicture';
import SecurityNumber from '../screens/SecurityNumber';
import AdditionalInfo from '../screens/AdditionalInfo';
import FaceRecog from '../screens/FaceRecog';
import Wishes from '../screens/Wishes';
import Login from '../screens/Login';
import AboutDriver from '../screens/AboutDriver';
import CustomMapView from '../customComponents/CustomMapView';
import CustomAutoComplete from '../customComponents/CustomAutoComplete';
import DriverWorkingStatus from '../screens/DriverWorkingStatus';
import TripRequest from '../screens/TripRequest';
import OnWay from '../screens/OnWay';
import ConfirmPickup from '../screens/ConfirmPickup'
import CustomCard from '../customComponents/CustomCard'
import EditAccount from '../screens/EditAccount';
import Payment from '../screens/Payment';
import AddBankInfo from '../screens/AddBankInfo';
import AddVehicles from '../screens/AddVehicles';
import History from '../screens/History';
import TripDetails from '../screens/TripDetails';
import DriverComments from '../screens/DriverComments';
import OwnComments from '../screens/OwnComments';
import HelpCenter from '../screens/HelpCenter';
import DrawerScreen from '../screens/DrawerScreen';
import Profile from '../screens/Profile';
import SafeDrive from '../screens/SafeDrive';
import EndDrive from '../screens/EndDrive';
import TripSummary from '../screens/TripSummary';
import { Height, Width } from '../dimension/dimension'
import ScheduleTrip from '../screens/ScheduleTrip';
import PendingTrip from '../screens/PendingTrip';
import { colorConstant, fontConstant } from '../utils/constant';
import Chat from '../screens/Chat';
import Report from '../screens/Report';
import AdditionalInfo2 from '../screens/AdditionalInfo2';
import Issue from '../screens/Issue';
import Withdraw from '../screens/Withdraw';
import Success from '../screens/Success';
import PendingTripDetails from '../screens/PendingTripDetails';
import ScheduleTripDetails from '../screens/ScheduleTripDetails';
import TabStack from '../screens/TabStack';
import TripsList from '../screens/TripsList';
import CommentSection from '../screens/CommentSection';
import UploadDriverDocument from '../screens/UploadDriverDocument';



const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();



const DrawerStack = () => (
  <>
    <Drawer.Navigator
      initialRouteName="DriverWorkingStatus"
      screenOptions={{
      drawerPosition:'left',
      drawerType: Platform.OS == 'ios' ? "front" :"front",
        headerShown: false,
        drawerStyle: {
          width:Platform.OS == 'ios' ?  "45%": '50%',
          height:Height*0.9,
          borderBottomRightRadius:Width*0.15,
        },
      }}
      drawerContent={props => <DrawerScreen {...props} />}>
      <Drawer.Screen name="DriverWorkingStatus" component={DriverWorkingStatus} />
      <Drawer.Screen name="TripRequest" component={TripRequest} />
      <Drawer.Screen name="OnWay" component={OnWay} />
      <Drawer.Screen name="ConfirmPickup" component={ConfirmPickup} />
      <Drawer.Screen name="SafeDrive" component={SafeDrive} />
      <Drawer.Screen name="EndDrive" component={EndDrive} />
      <Drawer.Screen name="TripSummary" component={TripSummary} />
    </Drawer.Navigator>
  </>
)

const MainStack = () => {

  return (
    <Stack.Navigator initialRouteName='DrawerStack' screenOptions={{ headerShown: false,animation :"slide_from_right"}}>
       <Stack.Screen name="TripsList" component={TripsList} />
       <Stack.Screen name="CommentSection" component={CommentSection} />
       {/* <Stack.Screen name="TabStack" component={TabStack} /> */}
      <Stack.Screen name="DrawerStack" component={DrawerStack} />
      <Stack.Screen name="EditAccount" component={EditAccount} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="Payment" component={Payment} />
      <Stack.Screen name="AddBankInfo" component={AddBankInfo} />
      <Stack.Screen name="AddVehicles" component={AddVehicles} />
      <Stack.Screen name="History" component={History} />
      <Stack.Screen name="TripDetails" component={TripDetails} />

      <Stack.Screen name="DriverComments" component={DriverComments} />
      <Stack.Screen name="OwnComments" component={OwnComments} />
      <Stack.Screen name="HelpCenter" component={HelpCenter} />
      <Stack.Screen name="Chat" component={Chat} />
      <Stack.Screen name="Report" component={Report} />
      <Stack.Screen name="Issue" component={Issue} />
      <Stack.Screen name="Withdraw" component={Withdraw} />
      <Stack.Screen name="Success" component={Success} />
      <Stack.Screen name="PendingTripDetails" component={PendingTripDetails} />
      <Stack.Screen name="ScheduleTripDetails" component={ScheduleTripDetails} />
      <Stack.Screen name="AdditionalInfo2" component={AdditionalInfo2} />
      <Stack.Screen name="UploadDriverDocument" component={UploadDriverDocument} />
      <Stack.Screen name="AboutDriver" component={AboutDriver} />


     
    </Stack.Navigator>

  )
}

export default MainStack
