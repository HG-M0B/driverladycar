import { StyleSheet, Text, View } from 'react-native'
import React from 'react'


import IntroStack from './IntroStack';
import AuthStack from './AuthStack';
import MainStack from './MainStack';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {useSelector} from 'react-redux'
const Stack = createNativeStackNavigator();

const RootNavigator =   ({ navigation }) => {

const {reducers} =  useSelector((state)=>state);
const {introStatus} = reducers;


    return (
        <Stack.Navigator 
        screenOptions={{
            headerShown: false
        }}>
            <>
            {
                introStatus == null && <Stack.Screen name= "IntroStack" component = {IntroStack}/>
            }
            {
                introStatus == "auth" && <Stack.Screen name= "AuthStack" component = {AuthStack}/>
            }
            {
                introStatus == "main" && <Stack.Screen name= "MainStack" component = {MainStack}/>
            }
            </>
        </Stack.Navigator>
    )
}

export default RootNavigator
