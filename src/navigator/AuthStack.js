
import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    SafeAreaView,
    Button,
    TouchableOpacity,
    Alert,
    Platform,
    Dimensions
  } 
  from 'react-native';
  import React, { Component,useEffect } from 'react'
  import { createNativeStackNavigator } from '@react-navigation/native-stack';
  import OnBoarding2 from '../screens/OnBoarding2';
  import OtpVerify from '../screens/OtpVerify';
  import Register from '../screens/Register';
  import DriverLicence from '../screens/DriverLicence';
  import DocumentPicture from '../screens/DocumentPicture';
  import SecurityNumber from '../screens/SecurityNumber';
  import AdditionalInfo from '../screens/AdditionalInfo';
  import FaceRecog from '../screens/FaceRecog';
  import Wishes from '../screens/Wishes';
  import Login from '../screens/Login';
  import Login1 from '../screens/Login1';
  import AboutDriver from '../screens/AboutDriver';
import First from '../screens/First';
import Forget from '../screens/Forget';
import Reset from '../screens/Reset';
import SignUp from '../screens/SignUp';

  
  
  
  const Stack = createNativeStackNavigator();
  

  
  const AuthStack = () => {
  
    return (
      <Stack.Navigator initialRouteName='First' screenOptions={{ headerShown: false , animation :"slide_from_right"}} >
        <Stack.Screen name="First" component={First} />
        <Stack.Screen name="OnBoarding2" component={OnBoarding2} />
        <Stack.Screen name="OtpVerify" component={OtpVerify} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="DriverLicence" component={DriverLicence} />
        <Stack.Screen name="DocumentPicture" component={DocumentPicture} />
        <Stack.Screen name="SecurityNumber" component={SecurityNumber} />
        <Stack.Screen name="AdditionalInfo" component={AdditionalInfo} />
        <Stack.Screen name="FaceRecog" component={FaceRecog} />
        <Stack.Screen name="Wishes" component={Wishes} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Login1" component={Login1} />
        <Stack.Screen name="SignUp" component={SignUp} />
        <Stack.Screen name="Forget" component={Forget} />
        <Stack.Screen name="Reset" component={Reset} />
        <Stack.Screen name="AboutDriver" component={AboutDriver} />
      </Stack.Navigator>
  
    )
  }
  
  export default AuthStack
  