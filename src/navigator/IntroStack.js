
import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    SafeAreaView,
    Button,
    TouchableOpacity,
    Alert,
    Platform,
    Dimensions
  } 
  from 'react-native';
  import React, { Component,useEffect } from 'react'
  import { createNativeStackNavigator } from '@react-navigation/native-stack';
  
  
  import  Start from '../screens/Start';
  

  const Stack = createNativeStackNavigator();
  const IntroStack = () => {
  
    return (
      <Stack.Navigator initialRouteName='Start' screenOptions={{ headerShown: false,animation :"slide_from_right"}}>
        <Stack.Screen name="Start" component={Start} />
      </Stack.Navigator>
  
    )
  }
  
  export default IntroStack
  