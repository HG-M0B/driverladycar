import axios from "axios";


const baseURL= 'http://13.59.90.211:7030/api/'

export const LoginWithMobileServices=async(data)=>{
    
    return axios.post(`${baseURL}auth/driver-login`, data, {
      headers: {
        'Content-Type': 'application/json',
        'Accept':'application/json'
      }
  })
}


export const LoginWithGoogleServices=async(data)=>{
    return axios.post(`${baseURL}auth/driver-social-login`, data, {
      headers: {
        'Content-Type': 'application/json',
        'Accept':'application/json'
      }
  })
}


export const checkUserExist=async(data)=>{
    return axios.post(`${baseURL}auth/check-driver`, data, {
      headers: {
        'Content-Type': 'application/json',
        'Accept':'application/json'
      }
  })
}


export const ForgetPasswordServices=async(data)=>{
  console.log(data)
  return axios.post(`${baseURL}auth/driver-forgot-password`, data, {
    headers: {
      'Content-Type': 'application/json',
      'Accept':'application/json'
    }
})
}



