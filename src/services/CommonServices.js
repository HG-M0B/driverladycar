import { PermissionsAndroid } from "react-native";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";

 const requestCameraPermission = async () => {
    if (Platform.OS === "android") {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: "Camera Permission",
            message: "App needs camera permission",
          }
        );
        // If CAMERA Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === "android") {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: "External Storage Write Permission",
            message: "App needs write permission",
          }
        );
        // If WRITE_EXTERNAL_STORAGE Permission is granted
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        alert("Write permission err", err);
      }
      return false;
    } else return true;
  };

 export const GetImageUrlServices = async (type) => {
    console.log(type)
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      console.log("access-->", isCameraPermitted, isStoragePermitted);
      let options = {
        mediaType: "photo",
        cameraType: "back",
        saveToPhotos: true,
        quality: 0.9,
        maxWidth: 500,
        maxHeight: 500,
        //  includeBase64: true,
        noData: true,
      };
      await launchCamera(options, (response) => {
        if (response.didCancel) {
          console.log("Cancelled");
        } else if (response.error) {
          console.log("Error", response.errorMessage);
        } else {
          console.log("Response-->", response?.assets?.[0]?.uri);
          let reqData = {
            uri: response?.assets?.[0]?.uri,
          };
          let formDataReq = new FormData();
          let data = {
            uri: response?.assets?.[0]?.uri,
            type: response?.assets?.[0]?.type,
            name: response?.assets?.[0]?.fileName,
          };
          formDataReq.append("file", data);
            type(formDataReq?._parts[0][1]);
        }
      });
    }
  };



  export const GetCityListServices=async()=>{
    const token = await AsyncStorage.getItem("token");
    return axios.get(`${baseURL}user/driver-view-profile`, {
        headers: {
          'Content-Type': 'application/json',
          "Authorization":token
      }
    })
  }



