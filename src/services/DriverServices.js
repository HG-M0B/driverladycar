import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";

const baseURL = "http://13.59.90.211:7030/api/";

export const DriverLooactionServices = async (data) => {
  const token = await AsyncStorage.getItem("token");
  return axios.post(`${baseURL}user/set-status`, data, {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
};

export const getUserProfileServices = async () => {
  const token = await AsyncStorage.getItem("token");
  return axios.get(`${baseURL}user/driver-view-profile`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
};

export const EditProfileServices = async (data) => {
  const token = await AsyncStorage.getItem("token");
  return axios.post(`${baseURL}user/driver-edit-profile`, data, {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
};

export const AddNewVechiclesServices = async (data,type) => {
  const token = await AsyncStorage.getItem("token");
  let url=`${baseURL}user/${type}`
  // console.log(data, "body data");
  console.log(url)
  return axios.post(url, data, {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
};

export const GetVehiclesListServices = async () => {
  const token = await AsyncStorage.getItem("token");
  return axios.get(`${baseURL}user/vehicle-list`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
};

export const SetDefaultVechicleServices = async (data) => {
  const token = await AsyncStorage.getItem("token");
  console.log(data, "body data");
  return axios.post(`${baseURL}user/set-vehicle`, data, {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
};

export const DeleteVechicleServices = async (data) => {
  const token = await AsyncStorage.getItem("token");
  console.log(data, "body data");
  return axios.post(`${baseURL}user/delete-vechile`, data, {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
};





export const PassengerRequestList = async () => {
  const token = await AsyncStorage.getItem("token");
  return axios.post(`${baseURL}user/request-list`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
};


export const EditVechicleServices = async (data) => {
  const token = await AsyncStorage.getItem("token");
  console.log(data, "body data");
  return axios.post(`${baseURL}user/edit-vechile`, data, {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
};
